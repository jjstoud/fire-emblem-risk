﻿using MySql.Data.MySqlClient;
using System;
using System.Data.SQLite;

namespace Data_Access
{
    public class MiscProvider
    {
        private readonly MySqlConnection _onlineDbConnection;
        private readonly SQLiteConnection _localDbConnection;

        private readonly bool _online;

        public MiscProvider(string dbConnection, bool online)
        {
            _online = online;

            if (online)
            {
                _onlineDbConnection = new MySqlConnection(dbConnection);
            }

            else
            {
                _localDbConnection = new SQLiteConnection(dbConnection);
            }
        }

        public bool CheckArmyName(string armyName)
        {
            bool authenticated;

            var sql = "SELECT Army_Name FROM Armies WHERE Army_Name='" + armyName + "'";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    var reader = command.ExecuteReader();

                    authenticated = reader.Read();

                    _onlineDbConnection.Close();
                }

                return authenticated;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                var reader = command.ExecuteReader();

                authenticated = reader.Read();

                _localDbConnection.Close();
            }

            return authenticated;
        }

        public bool AuthenticateUser(string armyName, string armyPassword)
        {
            bool authenticated;

            var sql = "SELECT Army_Name, Army_Leader FROM Armies WHERE Army_Name='" + armyName + "' AND Army_Password='" + armyPassword + "'";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    var reader = command.ExecuteReader();

                    authenticated = reader.Read();

                    _onlineDbConnection.Close();
                }

                return authenticated;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                var reader = command.ExecuteReader();

                authenticated = reader.Read();

                _localDbConnection.Close();
            }

            return authenticated;
        }
    }
}
