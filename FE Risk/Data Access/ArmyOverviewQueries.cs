﻿using System;
using System.Data.SQLite;
using Core_Classes;
using MySql.Data.MySqlClient;

namespace Data_Access
{
    public class ArmyOverviewQueries
    {
        private readonly MySqlConnection _onlineDbConnection;
        private readonly SQLiteConnection _localDbConnection;

        private readonly string _armyName;
        private readonly bool _online;

        public ArmyOverviewQueries(string dbConnection, bool online, string armyName)
        {
            _online = online;

            if (online)
            {
                _onlineDbConnection = new MySqlConnection(dbConnection);
            }

            else
            {
                _localDbConnection = new SQLiteConnection(dbConnection);
            }

            _armyName = armyName;
        }

        public Army GetArmy()
        {
            var sql = "SELECT Army_Name, Army_Leader, Army_Gold, Army_Land, Army_Status FROM Armies WHERE Army_Name = '" + _armyName + "'";
            var army = new Army();

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        army.Name = (string) reader["Army_Name"];
                        army.Leader = (string) reader["Army_Leader"];
                        army.Gold = Convert.ToInt32((uint) reader["Army_Gold"]);
                        army.Land = (byte) reader["Army_Land"];
                        army.Status = (string) reader["Army_Status"];
                    }

                    _onlineDbConnection.Close();
                }

                return army;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    army.Name = (string)reader["Army_Name"];
                    army.Leader = (string)reader["Army_Leader"];
                    army.Gold = Convert.ToInt32((long)reader["Army_Gold"]);
                    army.Land = Convert.ToInt32((long)reader["Army_Land"]);
                    army.Status = (string)reader["Army_Status"];
                }

                _localDbConnection.Close();
            }

            return army;
        }

        public int GetNumArmyGenerals()
        {
            int uniqueCount;

            var sql = "SELECT COUNT(ID) FROM Army_Generals WHERE Army_Name = '" + _armyName + "'";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    uniqueCount = Convert.ToInt32(command.ExecuteScalar());

                    _onlineDbConnection.Close();
                }

                return uniqueCount;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                uniqueCount = Convert.ToInt32(command.ExecuteScalar());

                _localDbConnection.Close();
            }

            return uniqueCount;
        }

        public int GetUniqueArmyUnits()
        {
            int uniqueCount;

            var sql = "SELECT COUNT(ID) FROM Army_Units WHERE Army_Name = '" + _armyName + "'";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    uniqueCount = Convert.ToInt32(command.ExecuteScalar());

                    _onlineDbConnection.Close();
                }

                return uniqueCount;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                uniqueCount = Convert.ToInt32(command.ExecuteScalar());

                _localDbConnection.Close();
            }

            return uniqueCount;
        }

        public string[] GetGeneralNames(string[] generalNames)
        {
            var sql = "SELECT Name FROM Army_Generals WHERE Army_Name = '" + _armyName + "'";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    var reader = command.ExecuteReader();

                    var count = 0;

                    while (reader.Read())
                    {
                        generalNames[count] = (string)reader["Name"];
                        count++;
                    }

                    _onlineDbConnection.Close();
                }

                return generalNames;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                var reader = command.ExecuteReader();

                var count = 0;

                while (reader.Read())
                {
                    generalNames[count] = (string)reader["Name"];
                    count++;
                }

                _localDbConnection.Close();
            }

            return generalNames;
        }

        public string[] GetUnitTitles(string[] unitTitles)
        {
            var sql = "SELECT Title, Count FROM Army_Units WHERE Army_Name = '" + _armyName + "'";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    var reader = command.ExecuteReader();

                    var count = 0;

                    while (reader.Read())
                    {
                        unitTitles[count] = (string)reader["Title"];
                        count++;
                    }

                    _onlineDbConnection.Close();
                }

                return unitTitles;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                var reader = command.ExecuteReader();

                var count = 0;

                while (reader.Read())
                {
                    unitTitles[count] = (string)reader["Title"];
                    count++;
                }

                _localDbConnection.Close();
            }

            return unitTitles;
        }

        public string[] GetUnitDisplayTitles(string[] unitTitles)
        {
            var sql = "SELECT Title, Count FROM Army_Units WHERE Army_Name = '" + _armyName + "'";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    var reader = command.ExecuteReader();

                    var count = 0;

                    while (reader.Read())
                    {
                        unitTitles[count] = (string)reader["Title"] + @"-" + (byte)reader["Count"];
                        count++;
                    }

                    _onlineDbConnection.Close();
                }

                return unitTitles;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                var reader = command.ExecuteReader();

                var count = 0;

                while (reader.Read())
                {
                    unitTitles[count] = (string)reader["Title"] + @"-" + (int)reader["Count"];
                    count++;
                }

                _localDbConnection.Close();
            }

            return unitTitles;
        }
    }
}
