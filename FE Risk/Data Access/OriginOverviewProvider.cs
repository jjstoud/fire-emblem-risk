﻿using System;
using System.Data;
using System.Data.SQLite;
using MySql.Data.MySqlClient;

namespace Data_Access
{
    public class OriginOverviewProvider
    {
        private readonly MySqlConnection _onlineDbConnection;
        private readonly SQLiteConnection _localDbConnection;

        private readonly bool _online;

        public OriginOverviewProvider(string dbConnection, bool online)
        {
            _online = online;

            if (online)
            {
                _onlineDbConnection = new MySqlConnection(dbConnection);
            }

            else
            {
                _localDbConnection = new SQLiteConnection(dbConnection);
            }
        }

        public int GetUniqueOriginCount()
        {
            int uniqueCount;

            const string sql = "SELECT COUNT(DISTINCT Unit_Origin) FROM Classes";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    uniqueCount = Convert.ToInt32(command.ExecuteScalar());

                    _onlineDbConnection.Close();
                }

                return uniqueCount;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                uniqueCount = Convert.ToInt32(command.ExecuteScalar());

                _localDbConnection.Close();
            }

            return uniqueCount;
        }

        public string[] GetUniqueOriginNames(string[] unitOrigins)
        {
            const string sql = "SELECT DISTINCT Unit_Origin FROM Classes";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    var reader = command.ExecuteReader();

                    var count = 0;

                    while (reader.Read())
                    {
                        unitOrigins[count] = (string)reader["Unit_Origin"];
                        count++;
                    }

                    _onlineDbConnection.Close();
                }

                return unitOrigins;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                var reader = command.ExecuteReader();

                var count = 0;

                while (reader.Read())
                {
                    unitOrigins[count] = (string)reader["Unit_Origin"];
                    count++;
                }

                _localDbConnection.Close();
            }

            return unitOrigins;
        }

        public DataTable GetOriginDataTable(string origin)
        {
            var dt = new DataTable();

            var sql = "SELECT Unit_Title, Unit_Tier, Weapon_Type_1, Weapon_Type_2, Weapon_Type_3, Weapon_Type_4, Weapon_Type_5, Ability_1, Ability_2 FROM Classes WHERE Unit_Origin='" + origin + "'";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    var reader = command.ExecuteReader();

                    dt.Columns.Add(new DataColumn("Unit_Title", typeof(string)));
                    dt.Columns.Add(new DataColumn("Unit_Tier", typeof(byte)));
                    dt.Columns.Add(new DataColumn("Weapon_Type_1", typeof(string)));
                    dt.Columns.Add(new DataColumn("Weapon_Type_2", typeof(string)));
                    dt.Columns.Add(new DataColumn("Weapon_Type_3", typeof(string)));
                    dt.Columns.Add(new DataColumn("Weapon_Type_4", typeof(string)));
                    dt.Columns.Add(new DataColumn("Weapon_Type_5", typeof(string)));
                    dt.Columns.Add(new DataColumn("Ability_1", typeof(string)));
                    dt.Columns.Add(new DataColumn("Ability_2", typeof(string)));

                    while (reader.Read())
                    {
                        dt.Rows.Add((string)reader["Unit_Title"], (byte)reader["Unit_Tier"], (string)reader["Weapon_Type_1"], (string)reader["Weapon_Type_2"], (string)reader["Weapon_Type_3"], (string)reader["Weapon_Type_4"], (string)reader["Weapon_Type_5"], (string)reader["Ability_1"], (string)reader["Ability_2"]);
                    }

                    _onlineDbConnection.Close();
                }

                return dt;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                var reader = command.ExecuteReader();

                dt.Columns.Add(new DataColumn("Unit_Title", typeof(string)));
                dt.Columns.Add(new DataColumn("Unit_Tier", typeof(int)));
                dt.Columns.Add(new DataColumn("Weapon_Type_1", typeof(string)));
                dt.Columns.Add(new DataColumn("Weapon_Type_2", typeof(string)));
                dt.Columns.Add(new DataColumn("Weapon_Type_3", typeof(string)));
                dt.Columns.Add(new DataColumn("Weapon_Type_4", typeof(string)));
                dt.Columns.Add(new DataColumn("Weapon_Type_5", typeof(string)));
                dt.Columns.Add(new DataColumn("Ability_1", typeof(string)));
                dt.Columns.Add(new DataColumn("Ability_2", typeof(string)));

                while (reader.Read())
                {
                    dt.Rows.Add((string)reader["Unit_Title"], (int)reader["Unit_Tier"], (string)reader["Weapon_Type_1"], (string)reader["Weapon_Type_2"], (string)reader["Weapon_Type_3"], (string)reader["Weapon_Type_4"], (string)reader["Weapon_Type_5"], (string)reader["Ability_1"], (string)reader["Ability_2"]);
                }

                _localDbConnection.Close();
            }

            return dt;
        }
    }
}
