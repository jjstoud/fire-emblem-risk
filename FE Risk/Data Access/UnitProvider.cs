﻿using System;
using System.Collections;
using System.Data.SQLite;
using Core_Classes;
using MySql.Data.MySqlClient;

namespace Data_Access
{
    public class UnitProvider
    {
        private readonly MySqlConnection _onlineDbConnection;
        private readonly SQLiteConnection _localDbConnection;

        private StatsProvider _statsProvider;
        private WeaponProvider _weaponProvider;

        private readonly string _dbConnection;
        private readonly bool _online;

        public UnitProvider(string dbConnection, bool online)
        {
            _dbConnection = dbConnection;
            _online = online;

            if (online)
            {
                _onlineDbConnection = new MySqlConnection(dbConnection);
            }

            else
            {
                _localDbConnection = new SQLiteConnection(dbConnection);
            }
        }

        public Unit GetUnit(string title)
        {
            _statsProvider = new StatsProvider(_dbConnection, _online, "Unit");

            Unit unit = null;

            var promotionsAl = new ArrayList();
            var weaponTypesAl = new ArrayList();
            var abilitiesAl = new ArrayList();

            var sql = "SELECT Unit_ID, Unit_Origin, Unit_Tier, Unit_Promotion_1, Unit_Promotion_2, Weapon_Type_1, Weapon_Type_2, Weapon_Type_3, Weapon_Type_4, Weapon_Type_5, Ability_1, Ability_2 FROM Classes WHERE Unit_Title='" + title + "'";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        promotionsAl.Add((string)reader["Unit_Promotion_1"]);

                        //Check if unit has a promotion to begin with before trying to add a second promotion
                        if (promotionsAl[0].ToString() != "N/A")
                        {
                            promotionsAl.Add((string)reader["Unit_Promotion_2"]);
                        }

                        weaponTypesAl.Add((string)reader["Weapon_Type_1"]);
                        weaponTypesAl.Add((string)reader["Weapon_Type_2"]);

                        //Check if the unit had a second weapon type before adding a third
                        if (weaponTypesAl[1].ToString() != "N/A")
                        {
                            weaponTypesAl.Add((string)reader["Weapon_Type_3"]);

                            //Check if the unit had a third weapon type before adding a fourth
                            if (weaponTypesAl[2].ToString() != "N/A")
                            {
                                weaponTypesAl.Add((string)reader["Weapon_Type_4"]);

                                //Check if the unit had a fourth weapon type before adding a fifth
                                if (weaponTypesAl[3].ToString() != "N/A")
                                {
                                    weaponTypesAl.Add((string)reader["Weapon_Type_5"]);

                                    //Deletes last last insert if the unit didn't have a fifth weapon type
                                    if (weaponTypesAl[4].ToString() == "N/A")
                                    {
                                        weaponTypesAl.Remove("N/A");
                                    }
                                }

                                //Deletes last last insert if the unit didn't have a fourth weapon type
                                else
                                {
                                    weaponTypesAl.Remove("N/A");
                                }
                            }

                            //Deletes last last insert if the unit didn't have a third weapon type
                            else
                            {
                                weaponTypesAl.Remove("N/A");
                            }
                        }

                        //Deletes last last insert if the unit didn't have a second weapon type
                        else
                        {
                            weaponTypesAl.Remove("N/A");
                        }

                        abilitiesAl.Add((string)reader["Ability_1"]);

                        //Check if unit has an ability to begin with before trying to add a second ability
                        if (abilitiesAl[0].ToString() != "N/A")
                        {
                            abilitiesAl.Add((string)reader["Ability_2"]);

                            if (abilitiesAl[1].ToString() == "N/A")
                            {
                                abilitiesAl.Remove("N/A");
                            }
                        }

                        unit = new Unit((string)reader["Unit_ID"], (string)reader["Unit_Origin"], title, (byte)reader["Unit_Tier"], promotionsAl, weaponTypesAl, abilitiesAl, _statsProvider.GetStats(title));
                    }

                    _onlineDbConnection.Close();
                }

                return unit;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    promotionsAl.Add((string)reader["Unit_Promotion_1"]);

                    //Check if unit has a promotion to begin with before trying to add a second promotion
                    if (promotionsAl[0].ToString() != "N/A")
                    {
                        promotionsAl.Add((string)reader["Unit_Promotion_2"]);
                    }

                    weaponTypesAl.Add((string)reader["Weapon_Type_1"]);
                    weaponTypesAl.Add((string)reader["Weapon_Type_2"]);

                    //Check if the unit had a second weapon type before adding a third
                    if (weaponTypesAl[1].ToString() != "N/A")
                    {
                        weaponTypesAl.Add((string)reader["Weapon_Type_3"]);

                        //Check if the unit had a third weapon type before adding a fourth
                        if (weaponTypesAl[2].ToString() != "N/A")
                        {
                            weaponTypesAl.Add((string)reader["Weapon_Type_4"]);

                            //Check if the unit had a fourth weapon type before adding a fifth
                            if (weaponTypesAl[3].ToString() != "N/A")
                            {
                                weaponTypesAl.Add((string)reader["Weapon_Type_5"]);

                                //Deletes last last insert if the unit didn't have a fifth weapon type
                                if (weaponTypesAl[4].ToString() == "N/A")
                                {
                                    weaponTypesAl.Remove("N/A");
                                }
                            }

                            //Deletes last last insert if the unit didn't have a fourth weapon type
                            else
                            {
                                weaponTypesAl.Remove("N/A");
                            }
                        }

                        //Deletes last last insert if the unit didn't have a third weapon type
                        else
                        {
                            weaponTypesAl.Remove("N/A");
                        }
                    }

                    //Deletes last last insert if the unit didn't have a second weapon type
                    else
                    {
                        weaponTypesAl.Remove("N/A");
                    }

                    abilitiesAl.Add((string)reader["Ability_1"]);

                    //Check if unit has an ability to begin with before trying to add a second ability
                    if (abilitiesAl[0].ToString() != "N/A")
                    {
                        abilitiesAl.Add((string)reader["Ability_2"]);

                        if (abilitiesAl[1].ToString() == "N/A")
                        {
                            abilitiesAl.Remove("N/A");
                        }
                    }

                    unit = new Unit((string)reader["Unit_ID"], (string)reader["Unit_Origin"], title, (int)reader["Unit_Tier"], promotionsAl, weaponTypesAl, abilitiesAl, _statsProvider.GetStats(title));
                }

                _localDbConnection.Close();
            }

            return unit;
        }

        public Unit GetArmyUnit(string armyName, string title)
        {
            _statsProvider = new StatsProvider(_dbConnection, _online, "Unit");
            _weaponProvider = new WeaponProvider(_dbConnection, _online);

            Unit unit = null;

            var promotionsAl = new ArrayList();
            var weaponTypesAl = new ArrayList();
            var weaponsAl = new ArrayList();
            var abilitiesAl = new ArrayList();

            var sql = "SELECT ID, Origin, Title, Level, Tier, Promotion_1, Promotion_2, Weapon_Type_1, Weapon_Type_2, Weapon_Type_3, Weapon_Type_4, Weapon_Type_5, Weapon_1, Weapon_2, Weapon_3, Ability_1, Ability_2 FROM Army_Units WHERE Army_Name='" + armyName + "' AND Title='" + title + "'";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        promotionsAl.Add((string)reader["Promotion_1"]);

                        //Check if unit has a promotion to begin with before trying to add a second promotion
                        if (promotionsAl[0].ToString() != "N/A")
                        {
                            promotionsAl.Add((string)reader["Promotion_2"]);
                        }

                        weaponTypesAl.Add((string)reader["Weapon_Type_1"]);
                        weaponTypesAl.Add((string)reader["Weapon_Type_2"]);

                        //Check if the unit had a second weapon type before adding a third
                        if (weaponTypesAl[1].ToString() != "N/A")
                        {
                            weaponTypesAl.Add((string)reader["Weapon_Type_3"]);

                            //Check if the unit had a third weapon type before adding a fourth
                            if (weaponTypesAl[2].ToString() != "N/A")
                            {
                                weaponTypesAl.Add((string)reader["Weapon_Type_4"]);

                                //Check if the unit had a fourth weapon type before adding a fifth
                                if (weaponTypesAl[3].ToString() != "N/A")
                                {
                                    weaponTypesAl.Add((string)reader["Weapon_Type_5"]);

                                    //Deletes last last insert if the unit didn't have a fifth weapon type
                                    if (weaponTypesAl[4].ToString() == "N/A")
                                    {
                                        weaponTypesAl.Remove("N/A");
                                    }
                                }

                                //Deletes last last insert if the unit didn't have a fourth weapon type
                                else
                                {
                                    weaponTypesAl.Remove("N/A");
                                }
                            }

                            //Deletes last last insert if the unit didn't have a third weapon type
                            else
                            {
                                weaponTypesAl.Remove("N/A");
                            }
                        }

                        //Deletes last last insert if the unit didn't have a second weapon type
                        else
                        {
                            weaponTypesAl.Remove("N/A");
                        }

                        weaponsAl.Add(_weaponProvider.GetWeapon((string)reader["Weapon_1"]));

                        //Check if the unit had a second weapon before adding a third
                        if ((string)reader["Weapon_2"] != "N/A")
                        {
                            weaponsAl.Add(_weaponProvider.GetWeapon((string)reader["Weapon_2"]));

                            //Check if the unit had a third weapon
                            if ((string)reader["Weapon_3"] != "N/A")
                            {
                                weaponsAl.Add(_weaponProvider.GetWeapon((string)reader["Weapon_3"]));
                            }
                        }

                        abilitiesAl.Add((string)reader["Ability_1"]);

                        //Check if unit has an ability to begin with before trying to add a second ability
                        if (abilitiesAl[0].ToString() != "N/A")
                        {
                            abilitiesAl.Add((string)reader["Ability_2"]);

                            if (abilitiesAl[1].ToString() == "N/A")
                            {
                                abilitiesAl.Remove("N/A");
                            }
                        }

                        unit = new Unit((string)reader["ID"], (string)reader["Origin"], (string)reader["Title"], (byte)reader["Level"], (byte)reader["Tier"], promotionsAl, weaponTypesAl, weaponsAl, abilitiesAl, _statsProvider.GetUnitStats((string)reader["ID"]));
                    }

                    _onlineDbConnection.Close();
                }

                return unit;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    promotionsAl.Add((string)reader["Promotion_1"]);

                    //Check if unit has a promotion to begin with before trying to add a second promotion
                    if (promotionsAl[0].ToString() != "N/A")
                    {
                        promotionsAl.Add((string)reader["Promotion_2"]);
                    }

                    weaponTypesAl.Add((string)reader["Weapon_Type_1"]);
                    weaponTypesAl.Add((string)reader["Weapon_Type_2"]);

                    //Check if the unit had a second weapon type before adding a third
                    if (weaponTypesAl[1].ToString() != "N/A")
                    {
                        weaponTypesAl.Add((string)reader["Weapon_Type_3"]);

                        //Check if the unit had a third weapon type before adding a fourth
                        if (weaponTypesAl[2].ToString() != "N/A")
                        {
                            weaponTypesAl.Add((string)reader["Weapon_Type_4"]);

                            //Check if the unit had a fourth weapon type before adding a fifth
                            if (weaponTypesAl[3].ToString() != "N/A")
                            {
                                weaponTypesAl.Add((string)reader["Weapon_Type_5"]);

                                //Deletes last last insert if the unit didn't have a fifth weapon type
                                if (weaponTypesAl[4].ToString() == "N/A")
                                {
                                    weaponTypesAl.Remove("N/A");
                                }
                            }

                            //Deletes last last insert if the unit didn't have a fourth weapon type
                            else
                            {
                                weaponTypesAl.Remove("N/A");
                            }
                        }

                        //Deletes last last insert if the unit didn't have a third weapon type
                        else
                        {
                            weaponTypesAl.Remove("N/A");
                        }
                    }

                    //Deletes last last insert if the unit didn't have a second weapon type
                    else
                    {
                        weaponTypesAl.Remove("N/A");
                    }

                    weaponsAl.Add(_weaponProvider.GetWeapon((string)reader["Weapon_1"]));

                    //Check if the unit had a second weapon before adding a third
                    if ((string)reader["Weapon_2"] != "N/A")
                    {
                        weaponsAl.Add(_weaponProvider.GetWeapon((string)reader["Weapon_2"]));

                        //Check if the unit had a third weapon
                        if ((string)reader["Weapon_3"] != "N/A")
                        {
                            weaponsAl.Add(_weaponProvider.GetWeapon((string)reader["Weapon_3"]));
                        }
                    }

                    abilitiesAl.Add((string)reader["Ability_1"]);

                    //Check if unit has an ability to begin with before trying to add a second ability
                    if (abilitiesAl[0].ToString() != "N/A")
                    {
                        abilitiesAl.Add((string)reader["Ability_2"]);

                        if (abilitiesAl[1].ToString() == "N/A")
                        {
                            abilitiesAl.Remove("N/A");
                        }
                    }

                    unit = new Unit((string)reader["ID"], (string)reader["Origin"], (string)reader["Title"], (int)reader["Level"], (int)reader["Tier"], promotionsAl, weaponTypesAl, weaponsAl, abilitiesAl, _statsProvider.GetUnitStats((string)reader["ID"]));
                }

                _localDbConnection.Close();
            }

            return unit;
        }
    }
}
