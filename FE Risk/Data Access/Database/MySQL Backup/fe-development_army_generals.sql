-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 25.13.131.6    Database: fe-development
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `army_generals`
--

DROP TABLE IF EXISTS `army_generals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `army_generals` (
  `ID` varchar(11) NOT NULL,
  `Army_Name` varchar(45) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Origin` varchar(45) NOT NULL,
  `Title` varchar(45) NOT NULL,
  `Level` tinyint(2) NOT NULL,
  `Tier` tinyint(1) NOT NULL,
  `Promotion_1` varchar(45) NOT NULL,
  `Promotion_2` varchar(45) NOT NULL,
  `Weapon_Type_1` varchar(45) NOT NULL,
  `Weapon_Type_2` varchar(45) NOT NULL,
  `Weapon_Type_3` varchar(45) NOT NULL,
  `Weapon_Type_4` varchar(45) NOT NULL,
  `Weapon_Type_5` varchar(45) NOT NULL,
  `Weapon_1` varchar(45) NOT NULL,
  `Weapon_2` varchar(45) NOT NULL,
  `Weapon_3` varchar(45) NOT NULL,
  `Ability_1` varchar(45) NOT NULL,
  `Ability_2` varchar(45) NOT NULL,
  `Health` tinyint(2) unsigned NOT NULL,
  `Strength` tinyint(2) unsigned NOT NULL,
  `Magic` tinyint(2) unsigned NOT NULL,
  `Skill` tinyint(2) unsigned NOT NULL,
  `Speed` tinyint(2) unsigned NOT NULL,
  `Luck` tinyint(2) unsigned NOT NULL,
  `Defense` tinyint(2) unsigned NOT NULL,
  `Resistance` tinyint(2) unsigned NOT NULL,
  `Move` tinyint(1) unsigned NOT NULL,
  `Health_Growth` tinyint(3) unsigned NOT NULL,
  `Strength_Growth` tinyint(3) unsigned NOT NULL,
  `Magic_Growth` tinyint(3) unsigned NOT NULL,
  `Skill_Growth` tinyint(3) unsigned NOT NULL,
  `Speed_Growth` tinyint(3) unsigned NOT NULL,
  `Luck_Growth` tinyint(3) unsigned NOT NULL,
  `Defense_Growth` tinyint(3) unsigned NOT NULL,
  `Resistance_Growth` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `Army_Name_idx` (`Army_Name`),
  KEY `Title_idx` (`Title`),
  CONSTRAINT `Army_Name_Generals` FOREIGN KEY (`Army_Name`) REFERENCES `armies` (`Army_Name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Title_Generals` FOREIGN KEY (`Title`) REFERENCES `classes` (`Unit_Title`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table that holds every general generated.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `army_generals`
--
-- ORDER BY:  `ID`

LOCK TABLES `army_generals` WRITE;
/*!40000 ALTER TABLE `army_generals` DISABLE KEYS */;
INSERT INTO `army_generals` VALUES ('GBRPEGLJ291','Gliding Professionals','Bethany','Rider','Pegasus Knight',1,1,'Griffon Rider','Falcoknight','Lance','N/A','N/A','N/A','N/A','Bronze Lance','N/A','N/A','N/A','N/A',20,10,6,7,9,8,7,10,7,40,50,20,45,70,40,35,65),('GDICIDIJ914','Divine Killers','Diane','Initiate','Cleric',1,1,'Vindicator','Paladin','Axe','Light Magic','Staff','N/A','N/A','Bronze Axe','Light','Heal','N/A','N/A',18,9,9,5,5,8,7,8,5,70,60,80,35,30,10,45,45),('GDPMAARJ879','Arcane Warriors','Damean','Pupil','Mage',1,1,'Archmage','Wizard','Arcane Magic','N/A','N/A','N/A','N/A','Fire','N/A','N/A','N/A','N/A',16,6,10,7,7,8,6,8,5,50,10,80,65,50,40,15,55),('GETANMAJ423','Marching Bulls','Enid','Trainee','Archer',1,1,'Sniper','Huntsman','Bow','N/A','N/A','N/A','N/A','Bronze Bow','N/A','N/A','+5 Crit Chance','N/A',19,10,5,10,9,6,6,6,5,60,60,10,85,60,40,25,25),('GGIIRSHJ263','Shadow Mounts','Gransin','Initiate','Impaler',1,1,'Dread Knight','Heretic','Lance','Dark Magic','N/A','N/A','N/A','Bronze Lance','Flux','N/A','N/A','N/A',16,6,7,8,9,5,7,5,5,40,80,60,65,60,10,25,25),('GJTPAGRJ389','Grima\'s Hope','Jax','Trainee','Pirate',1,1,'Reaver','Beserker','Axe','N/A','N/A','N/A','N/A','Bronze Axe','N/A','N/A','+5 Crit Chance','Moves freely through water',19,9,6,6,7,7,6,5,5,70,90,10,45,40,30,55,25),('GKRKAMYJ371','Mystic Knights','Kathryn','Recruit','Knight',1,1,'General','Commander','Lance','N/A','N/A','N/A','N/A','Bronze Lance','N/A','N/A','N/A','N/A',17,8,5,8,8,8,8,7,4,70,70,10,55,30,40,75,25),('GRRWADRJ947','Draconic Mages','Raymundo','Rider','Wyvern Rider',1,1,'Wyvern Knight','Wyvern Lord','Axe','N/A','N/A','N/A','N/A','Bronze Axe','N/A','N/A','N/A','N/A',19,9,5,7,8,9,8,5,7,70,70,10,55,40,40,65,15),('GVIILDAJ446','Dark Riders','Vlad','Initiate','Impaler',1,1,'Dread Knight','Heretic','Lance','Dark Magic','N/A','N/A','N/A','Bronze Lance','Flux','N/A','N/A','N/A',17,9,8,9,7,5,6,8,5,50,70,70,75,30,20,25,25);
/*!40000 ALTER TABLE `army_generals` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-28  9:45:51
