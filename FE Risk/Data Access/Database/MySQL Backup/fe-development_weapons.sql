-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 25.13.131.6    Database: fe-development
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `weapons`
--

DROP TABLE IF EXISTS `weapons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `weapons` (
  `Weapon_ID` varchar(5) NOT NULL DEFAULT 'N/A',
  `Weapon_Name` varchar(45) NOT NULL DEFAULT 'N/A',
  `Weapon_Type` varchar(45) NOT NULL DEFAULT 'N/A',
  `Weapon_Subtype` varchar(45) NOT NULL DEFAULT 'N/A',
  `Might` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `Hit_Rate` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Critical` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `Min_Range` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `Max_Range` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `Weight` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `Cost` smallint(5) unsigned NOT NULL DEFAULT '0',
  `Effect` tinytext,
  PRIMARY KEY (`Weapon_ID`),
  UNIQUE KEY `Weapon_ID_UNIQUE` (`Weapon_ID`),
  UNIQUE KEY `Weapon_Name_UNIQUE` (`Weapon_Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The table that holds all the weapon information.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `weapons`
--
-- ORDER BY:  `Weapon_ID`

LOCK TABLES `weapons` WRITE;
/*!40000 ALTER TABLE `weapons` DISABLE KEYS */;
INSERT INTO `weapons` VALUES ('BTKLR','Beast Killer','Knife','N/A',9,65,20,1,1,8,1200,'Effective vs. Beasts.'),('BVAXE','Brave Axe','Axe','N/A',11,80,0,1,1,13,2640,'Allows user to strike twice in one attack.'),('BVBOW','Brave Bow','Bow','N/A',9,85,0,2,2,10,7200,'Allows user to strike twice in one attack.'),('BVLCE','Brave Lance','Lance','N/A',10,85,0,1,1,11,2900,'Allows user to strike twice in one attack.'),('BVSWD','Brave Sword','Sword','N/A',9,90,0,1,1,9,2800,'Allows user to strike twice in one attack.'),('BZAXE','Bronze Axe','Axe','N/A',5,85,0,1,1,7,250,'Cannot critical.'),('BZBOW','Bronze Bow','Bow','N/A',3,80,0,2,2,4,400,'Cannot critical.'),('BZDGR','Bronze Dagger','Knife','N/A',2,85,0,1,1,1,270,'Cannot critical.'),('BZKFE','Bronze Knife','Knife','N/A',1,70,0,1,2,1,180,'Cannot critical.'),('BZLCE','Bronze Lance','Lance','N/A',4,90,0,1,1,6,450,'Cannot critical.'),('BZSWD','Bronze Sword','Sword','N/A',3,95,0,1,1,5,350,'Cannot critical.'),('HAMMR','Hammer','Axe','N/A',13,60,0,1,1,17,800,'Effective vs. Armored units.'),('HDAXE','Hand Axe','Axe','N/A',9,75,0,1,2,12,625,'N/A'),('HEASF','Heal','Staff','N/A',1,100,5,1,1,2,800,'Restores HP to an adjacent ally equal to (Magic +10). While equipped, user restores 5 HP each turn. Provides 11 EXP to the user.'),('HRSLR','Horseslayer','Lance','N/A',12,65,0,1,1,15,1300,'Effective vs. Horseback units.'),('IRAXE','Iron Axe','Axe','N/A',8,80,0,1,1,11,400,'N/A'),('IRBDE','Iron Blade','Sword','N/A',10,70,0,1,1,13,800,'N/A'),('IRBOW','Iron Bow','Bow','N/A',6,85,0,2,2,8,750,'N/A'),('IRDGR','Iron Dagger','Knife','N/A',5,80,5,1,1,3,360,'N/A'),('IRGLE','Iron Greatlance','Lance','N/A',11,75,0,1,1,14,640,'N/A'),('IRKFE','Iron Knife','Knife','N/A',2,65,0,1,2,2,500,'N/A'),('IRLBW','Iron Longbow','Bow','N/A',8,65,0,2,3,15,2000,'N/A'),('IRLCE','Iron Lance','Lance','N/A',7,85,0,1,1,9,600,'N/A'),('IRPAX','Iron Poleaxe','Axe','N/A',12,65,0,1,1,16,400,'N/A'),('IRSWD','Iron Sword','Sword','N/A',6,90,0,1,1,7,500,'N/A'),('JVLIN','Javelin','Lance','N/A',7,65,0,1,2,11,600,'N/A'),('KARD','Kard','Knife','N/A',4,70,10,1,1,6,2400,'N/A'),('KLAXE','Killer Axe','Axe','N/A',10,75,30,1,1,12,2880,'N/A'),('KLBOW','Killer Bow','Bow','N/A',8,80,30,2,2,9,2000,'N/A'),('KLEDE','Killing Edge','Sword','N/A',8,85,30,1,1,8,3600,'N/A'),('KLLCE','Killer Lance','Lance','N/A',9,80,30,1,1,10,4320,'N/A'),('PSHKZ','Peshkatz','Knife','N/A',13,90,0,1,2,9,12600,'N/A'),('SHAXE','Short Axe','Axe','N/A',12,60,0,1,2,13,1500,'N/A'),('SHSPR','Short Spear','Lance','N/A',10,55,0,1,2,12,3000,'N/A'),('SLAXE','Silver Axe','Axe','N/A',14,70,0,1,1,14,1440,'N/A'),('SLBDE','Silver Blade','Sword','N/A',16,60,0,1,1,16,3600,'N/A'),('SLBOW','Silver Bow','Bow','N/A',15,75,0,2,2,9,2250,'N/A'),('SLCER','Silencer','Bow','N/A',16,100,5,2,2,6,8000,'N/A'),('SLDGR','Silver Dagger','Knife','N/A',12,85,5,1,1,7,1800,'N/A'),('SLGLE','Silver Greatlance','Lance','N/A',17,50,0,1,1,17,2880,'N/A'),('SLKFE','Silver Knife','Knife','N/A',7,60,0,1,2,4,1800,'N/A'),('SLLBW','Silver Longbow','Bow','N/A',17,55,0,2,3,17,12000,'N/A'),('SLLCE','Silver Lance','Lance','N/A',13,80,0,1,1,12,2160,'N/A'),('SLPAX','Silver Poleaxe','Axe','N/A',18,60,0,1,1,19,1800,'N/A'),('SLSWD','Silver Sword','Sword','N/A',12,80,0,1,1,10,1800,'N/A'),('SMLCE','Slim Lance','Lance','N/A',3,95,5,1,1,4,490,'N/A'),('SMSWD','Slim Sword','Sword','N/A',2,100,5,1,1,3,560,'N/A'),('SOSWD','Storm Sword','Sword','N/A',12,50,0,1,2,11,4000,'N/A'),('SPEAR','Spear','Lance','N/A',13,60,5,1,2,16,9000,'N/A'),('STAXE','Steel Axe','Axe','N/A',11,75,0,1,1,15,640,'N/A'),('STBDE','Steel Blade','Sword','N/A',13,65,0,1,1,17,1400,'N/A'),('STBOW','Steel Bow','Bow','N/A',10,80,0,2,2,10,1050,'N/A'),('STDGR','Steel Dagger','Knife','N/A',8,85,5,1,1,5,420,'N/A'),('STGLE','Steel Greatlance','Lance','N/A',14,70,0,1,1,18,1120,'N/A'),('STKFE','Steel Knife','Knife','N/A',4,60,0,1,2,3,800,'N/A'),('STLBW','Steel Longbow','Bow','N/A',12,60,0,2,3,18,4000,'N/A'),('STLCE','Steel Lance','Lance','N/A',10,80,0,1,1,13,960,'N/A'),('STLTO','Stiletto','Knife','N/A',8,80,20,1,1,8,3100,'N/A'),('STPAX','Steel Poleaxe','Axe','N/A',15,60,0,1,1,20,700,'N/A'),('STSWD','Steel Sword','Sword','N/A',9,85,0,1,1,11,800,'N/A'),('TMAFE','Arcfire','Arcane Magic','Fire Magic',9,80,5,1,2,7,2500,'N/A'),('TMATR','Arcthunder','Arcane Magic','Thunder Magic',7,90,15,1,2,9,2550,'N/A'),('TMAWD','Arcwind','Arcane Magic','Wind Magic',8,85,10,1,2,6,2450,'N/A'),('TMBLE','Bolganone','Arcane Magic','Fire Magic',11,75,0,1,2,9,3000,'N/A'),('TMBTG','Bolting','Arcane Magic','Thunder Magic',6,60,5,3,10,19,800,'N/A'),('TMBZD','Blizzard','Arcane Magic','Wind Magic',7,75,0,3,10,17,700,'N/A'),('TMCRU','Carreau','Dark Magic','N/A',10,70,5,1,2,11,3000,'N/A'),('TMEFE','Elfire','Arcane Magic','Fire Magic',7,85,0,1,2,5,2100,'N/A'),('TMELT','Ellight','Light Magic','N/A',5,95,0,1,2,3,2240,'N/A'),('TMETR','Elthunder','Arcane Magic','Thunder Magic',5,75,10,1,2,6,1470,'N/A'),('TMEWD','Elwind','Arcane Magic','Wind Magic',6,90,0,1,2,4,2030,'N/A'),('TMFLX','Flux','Dark Magic','N/A',7,80,0,1,2,8,900,'N/A'),('TMFNR','Fenrir','Dark Magic','N/A',9,60,0,3,10,20,1500,'N/A'),('TMFRE','Fire','Arcane Magic','Fire Magic',5,90,0,1,2,3,400,'N/A'),('TMHWK','Tomahawk','Axe','N/A',15,65,5,1,2,17,6000,'N/A'),('TMLGT','Light','Light Magic','N/A',3,100,0,1,2,1,960,'N/A'),('TMMTR','Meteor','Arcane Magic','Fire Magic',8,70,0,3,10,18,750,'N/A'),('TMNFU','Nosferatu','Light Magic','N/A',6,85,0,1,2,7,4000,'Restores HP to user equal to damage dealt.'),('TMPGE','Purge','Light Magic','N/A',5,80,0,3,10,16,1000,'N/A'),('TMSHE','Shine','Light Magic','N/A',7,90,10,1,2,5,2600,'N/A'),('TMTDR','Thunder','Arcane Magic','Thunder Magic',3,80,5,1,2,4,880,'N/A'),('TMTHN','Thoron','Arcane Magic','Thunder Magic',9,65,5,1,2,11,3200,'N/A'),('TMTNO','Tornado','Arcane Magic','Wind Magic',10,80,0,1,2,7,2800,'N/A'),('TMVLA','Valaura','Light Magic','N/A',8,95,0,1,2,12,6800,'Inflicts Poison upon contact.'),('TMVRE','Verrine','Dark Magic','N/A',12,65,0,1,2,13,5000,'N/A'),('TMWND','Wind','Arcane Magic','Wind Magic',4,95,0,1,2,2,720,'N/A'),('TMWRM','Worm','Dark Magic','N/A',8,75,0,1,2,9,2400,'N/A'),('TPBDE','Tempest Blade','Sword','N/A',15,55,5,1,2,15,10000,'N/A'),('VNAXE','Venin Axe','Axe','N/A',8,80,0,1,1,11,250,'Inflicts Poison upon contact.'),('VNBOW','Venin Bow','Bow','N/A',6,85,0,2,2,8,400,'Inflicts Poison upon contact.'),('VNEDE','Venin Edge','Sword','N/A',6,90,0,1,1,7,300,'Inflicts Poison upon contact.'),('VNLCE','Venin Lance','Lance','N/A',7,85,0,1,1,9,350,'Inflicts Poison upon contact.'),('WDEDE','Wind Edge','Sword','N/A',6,60,0,1,2,10,700,'N/A'),('WMSLR','Wyrmslayer','Sword','N/A',11,70,0,1,1,14,3600,'Effective vs. Dragons.'),('WODAO','Wo Dao','Sword','N/A',7,90,20,1,1,7,3000,'N/A');
/*!40000 ALTER TABLE `weapons` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-28  9:45:53
