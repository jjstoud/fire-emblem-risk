-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 25.13.131.6    Database: fe-development
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `class_stats_full`
--

DROP TABLE IF EXISTS `class_stats_full`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_stats_full` (
  `Unit_ID` varchar(4) NOT NULL DEFAULT 'N/A',
  `Unit_Title` varchar(45) NOT NULL DEFAULT 'N/A',
  `Health` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `Strength` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `Magic` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `Skill` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `Speed` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `Luck` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `Defense` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `Resistance` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `Move` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Health_Growth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Strength_Growth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Magic_Growth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Skill_Growth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Speed_Growth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Luck_Growth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Defense_Growth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Resistance_Growth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Unit_ID`),
  UNIQUE KEY `Unit_ID_UNIQUE` (`Unit_ID`),
  UNIQUE KEY `Unit_Title_UNIQUE` (`Unit_Title`),
  CONSTRAINT `Unit_ID` FOREIGN KEY (`Unit_ID`) REFERENCES `classes` (`Unit_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Unit_Title` FOREIGN KEY (`Unit_Title`) REFERENCES `classes` (`Unit_Title`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Base stats and growth rates for the classes in the database.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_stats_full`
--
-- ORDER BY:  `Unit_ID`

LOCK TABLES `class_stats_full` WRITE;
/*!40000 ALTER TABLE `class_stats_full` DISABLE KEYS */;
INSERT INTO `class_stats_full` VALUES ('1ICC','Cleric',22,8,9,6,7,4,7,8,5,90,50,60,35,40,20,35,45),('1IIR','Impaler',17,9,8,10,9,5,7,5,5,60,60,50,65,50,20,35,25),('1ISD','Spellsword',19,8,8,9,10,5,4,5,5,70,50,50,55,60,30,15,25),('1PAM','Mage',17,5,11,9,9,7,5,7,5,60,30,70,55,50,40,25,35),('1PDM','Dark Mage',20,5,11,9,7,5,7,5,5,80,30,70,55,40,30,35,25),('1PLM','Light Mage',17,5,9,7,7,7,5,10,5,60,30,60,45,40,40,25,55),('1RCR','Cavalier',19,9,5,10,7,5,7,5,7,70,60,30,65,40,30,45,25),('1RFR','Fighter',20,11,5,7,7,7,7,5,5,80,70,30,45,40,40,35,25),('1RKT','Knight',20,9,5,7,5,7,10,7,4,80,60,30,45,30,40,55,35),('1RMY','Mercenary',19,9,5,10,9,5,7,5,5,70,60,30,65,50,30,35,25),('1RPK','Pegasus Knight',17,6,5,9,10,7,7,8,7,60,40,30,55,60,40,35,45),('1RSR','Soldier',19,9,5,10,9,5,7,5,5,70,60,30,65,50,30,35,25),('1RWR','Wyvern Rider',20,11,5,7,5,7,8,5,7,80,70,30,45,30,40,45,25),('1TAR','Archer',17,8,5,10,9,7,7,7,5,60,50,30,65,50,40,35,35),('1TMN','Myrmidon',17,8,5,9,10,8,7,5,5,60,50,30,55,60,50,35,25),('1TPE','Pirate',20,11,5,6,7,5,8,7,5,80,70,30,35,40,30,45,35),('1TTF','Thief',19,8,5,9,10,8,5,5,6,70,50,30,55,60,50,25,25),('2IDK','Dread Knight',26,19,16,22,18,8,13,10,6,70,70,60,75,60,30,45,35),('2IHC','Heretic',26,18,17,22,18,8,12,11,6,70,70,60,75,60,30,45,35),('2INB','Night Blade',30,15,16,19,20,9,7,9,6,80,60,60,65,70,40,25,35),('2IPN','Paladin',37,18,18,11,13,7,14,15,6,100,60,70,45,50,30,45,55),('2ISR','Stalker',29,16,15,19,20,9,7,9,6,80,60,60,65,70,40,25,35),('2IVR','Vindicator',38,16,19,11,13,7,12,16,6,100,60,70,45,50,30,45,55),('2PAE','Archmage',28,10,24,20,18,14,10,13,6,70,40,80,65,60,50,35,45),('2PHR','Harbinger',34,9,24,18,13,9,11,10,6,90,40,80,65,50,40,45,35),('2PPT','Prophet',28,9,19,14,13,14,9,20,6,70,40,70,55,50,50,35,65),('2PSE','Sage',27,1,20,15,13,13,9,21,6,70,40,70,55,50,50,35,65),('2PSR','Sorcerer',33,9,24,19,14,9,10,9,6,90,40,80,65,50,40,45,35),('2PWD','Wizard',28,10,24,19,19,14,10,14,6,70,40,80,65,60,50,35,45),('2RCN','Captain',29,17,9,20,17,9,12,9,6,80,70,40,75,60,40,45,35),('2RCR','Commander',33,19,9,16,9,13,19,12,5,90,70,40,55,40,50,65,45),('2RFT','Falcoknight',26,13,9,19,20,13,12,17,8,70,50,40,65,70,50,45,55),('2RGL','General',33,19,9,15,9,13,20,12,5,90,70,40,55,40,50,65,45),('2RGR','Griffon Rider',26,13,9,19,21,13,12,15,8,70,50,40,65,70,50,45,55),('2RHM','Horsemaster',31,20,9,22,14,9,15,9,8,80,70,40,75,50,40,55,35),('2RHO','Hero',33,21,9,20,17,10,13,9,6,85,75,40,65,55,45,45,35),('2RHR','Halberdier',29,17,9,20,16,10,12,9,6,80,70,40,75,60,40,45,35),('2RRR','Ranger',30,20,9,22,15,10,14,9,8,80,70,40,75,50,40,55,35),('2RWK','Wyvern Knight',33,23,9,15,11,13,14,9,8,90,80,40,55,40,50,55,35),('2RWL','Wyvern Lord',33,23,9,15,10,13,15,9,8,90,80,40,55,40,50,55,35),('2RWR','Warrior',33,24,9,16,13,13,13,9,6,90,80,40,55,50,50,45,35),('2TAN','Assassin',29,18,9,20,20,15,9,9,6,80,60,40,65,70,60,35,35),('2TBD','Bladedancer',25,16,9,20,21,15,12,10,6,70,60,40,65,70,60,45,35),('2TBR','Beserker',32,24,9,14,14,9,16,11,6,90,80,40,45,50,40,55,45),('2THN','Huntsman',26,18,9,22,18,13,12,12,6,70,60,40,75,60,50,45,45),('2TNA','Ninja',29,17,9,20,21,15,9,9,6,80,60,40,65,70,60,35,35),('2TRR','Reaver',33,24,9,12,14,9,15,11,6,90,80,40,45,50,40,55,45),('2TSM','Swordsmaster',25,17,9,20,21,15,12,9,6,70,60,40,65,70,60,45,35),('2TSR','Sniper',25,17,9,22,19,13,12,12,6,70,60,40,75,60,50,45,45);
/*!40000 ALTER TABLE `class_stats_full` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-28  9:45:53
