-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 25.13.131.6    Database: fe-development
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `armies`
--

DROP TABLE IF EXISTS `armies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `armies` (
  `Army_Name` varchar(45) NOT NULL DEFAULT 'N/A',
  `Army_Leader` varchar(45) NOT NULL DEFAULT 'N/A',
  `Army_Password` varchar(45) NOT NULL DEFAULT 'N/A',
  `Army_Gold` mediumint(6) unsigned NOT NULL DEFAULT '5000',
  `Army_Land` tinyint(2) unsigned NOT NULL DEFAULT '5',
  PRIMARY KEY (`Army_Name`),
  UNIQUE KEY `Army_Name_UNIQUE` (`Army_Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='General information on the armies created.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `armies`
--
-- ORDER BY:  `Army_Name`

LOCK TABLES `armies` WRITE;
/*!40000 ALTER TABLE `armies` DISABLE KEYS */;
INSERT INTO `armies` VALUES ('Arcane Warriors','Javante\'','@rmy$1X',5000,5),('Dark Riders','Javante\'','@rmyTHR33',5000,5),('Divine Killers','Javante\'','@rmy$3V3N',5000,5),('Draconic Mages','Javante\'','@rmyTW0',5000,5),('Gliding Professionals','Javante\'','@rmyF0UR',5000,5),('Grima\'s Hope','Javante\'','@rmy31GHT',5000,5),('Marching Bulls','Javante\'','@rmyF1V3',5000,5),('Mystic Knights','Javante\'','@rmy0N3',5000,5),('Shadow Mounts','Javante\'','@rmyN1N3',5000,5);
/*!40000 ALTER TABLE `armies` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-28  9:45:50
