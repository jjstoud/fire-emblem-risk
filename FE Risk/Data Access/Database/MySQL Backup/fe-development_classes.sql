-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 25.13.131.6    Database: fe-development
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classes` (
  `Unit_ID` varchar(4) NOT NULL DEFAULT 'N/A',
  `Unit_Origin` varchar(45) NOT NULL DEFAULT 'N/A',
  `Unit_Title` varchar(45) NOT NULL DEFAULT 'N/A',
  `Unit_Tier` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `Unit_Promotion_1` varchar(45) NOT NULL DEFAULT 'N/A',
  `Unit_Promotion_2` varchar(45) NOT NULL DEFAULT 'N/A',
  `Weapon_Type_1` varchar(45) NOT NULL DEFAULT 'N/A',
  `Weapon_Type_2` varchar(45) NOT NULL DEFAULT 'N/A',
  `Weapon_Type_3` varchar(45) NOT NULL DEFAULT 'N/A',
  `Weapon_Type_4` varchar(45) NOT NULL DEFAULT 'N/A',
  `Weapon_Type_5` varchar(45) NOT NULL DEFAULT 'N/A',
  `Ability_1` tinytext,
  `Ability_2` tinytext,
  PRIMARY KEY (`Unit_ID`),
  UNIQUE KEY `Unit_ID_UNIQUE` (`Unit_ID`),
  UNIQUE KEY `Unit_Title_UNIQUE` (`Unit_Title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The table that holds basic information for each class.		';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classes`
--
-- ORDER BY:  `Unit_ID`

LOCK TABLES `classes` WRITE;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
INSERT INTO `classes` VALUES ('1ICC','Initiate','Cleric',1,'Vindicator','Paladin','Axe','Light Magic','Staff','N/A','N/A','N/A','N/A'),('1IIR','Initiate','Impaler',1,'Dread Knight','Heretic','Lance','Dark Magic','N/A','N/A','N/A','N/A','N/A'),('1ISD','Initiate','Spellsword',1,'Night Blade','Stalker','Sword','Arcane Magic','N/A','N/A','N/A','N/A','N/A'),('1PAM','Pupil','Mage',1,'Archmage','Wizard','Arcane Magic','N/A','N/A','N/A','N/A','N/A','N/A'),('1PDM','Pupil','Dark Mage',1,'Sorcerer','Harbinger','Dark Magic','N/A','N/A','N/A','N/A','N/A','N/A'),('1PLM','Pupil','Light Mage',1,'Prophet','Sage','Light Magic','N/A','N/A','N/A','N/A','N/A','N/A'),('1RCR','Rider','Cavalier',1,'Horsemaster','Ranger','Lance','Sword','N/A','N/A','N/A','N/A','N/A'),('1RFR','Recruit','Fighter',1,'Warrior','Hero','Axe','N/A','N/A','N/A','N/A','N/A','N/A'),('1RKT','Recruit','Knight',1,'General','Commander','Lance','N/A','N/A','N/A','N/A','N/A','N/A'),('1RMY','Recruit','Mercenary',1,'Captain','Hero','Sword','N/A','N/A','N/A','N/A','N/A','N/A'),('1RPK','Rider','Pegasus Knight',1,'Griffon Rider','Falcoknight','Lance','N/A','N/A','N/A','N/A','N/A','N/A'),('1RSR','Recruit','Soldier',1,'Halberdier','Hero','Lance','N/A','N/A','N/A','N/A','N/A','N/A'),('1RWR','Rider','Wyvern Rider',1,'Wyvern Knight','Wyvern Lord','Axe','N/A','N/A','N/A','N/A','N/A','N/A'),('1TAR','Trainee','Archer',1,'Sniper','Huntsman','Bow','N/A','N/A','N/A','N/A','+5 Crit Chance','N/A'),('1TMN','Trainee','Myrmidon',1,'Swordsmaster','Bladedancer','Sword','N/A','N/A','N/A','N/A','+5 Crit Chance','N/A'),('1TPE','Trainee','Pirate',1,'Reaver','Beserker','Axe','N/A','N/A','N/A','N/A','+5 Crit Chance','Moves freely through water'),('1TTF','Trainee','Thief',1,'Assassin','Ninja','Knife','N/A','N/A','N/A','N/A','Lockpick','N/A'),('2IDK','Initiate','Dread Knight',2,'N/A','N/A','Lance','Dark Magic','Sword','Axe','N/A','N/A','N/A'),('2IHC','Initiate','Heretic',2,'N/A','N/A','Lance','Dark Magic','Light Magic','N/A','N/A','N/A','N/A'),('2INB','Initiate','Night Blade',2,'N/A','N/A','Sword','Arcane Magic','Dark Magic','N/A','N/A','N/A','N/A'),('2IPN','Initiate','Paladin',2,'N/A','N/A','Axe','Light Magic','Staff','Sword','Axe','N/A','N/A'),('2ISR','Initiate','Stalker',2,'N/A','N/A','Sword','Arcane Magic','Bow','N/A','N/A','N/A','N/A'),('2IVR','Initiate','Vindicator',2,'N/A','N/A','Axe','Light Magic','Staff','Arcane Magic','N/A','N/A','N/A'),('2PAE','Pupil','Archmage',2,'N/A','N/A','Arcane Magic','Light Magic','Dark Magic','N/A','N/A','N/A','N/A'),('2PHR','Pupil','Harbinger',2,'N/A','N/A','Dark Magic','Staff','N/A','N/A','N/A','Doubled Crit Chance (any critical achieved due to the increased range ignores the target\'s resistance, instead of dealing critical damage)','N/A'),('2PPT','Pupil','Prophet',2,'N/A','N/A','Light Magic','Staff','N/A','N/A','N/A','Heals for the damage dealt to target on critical hit','N/A'),('2PSE','Pupil','Sage',2,'N/A','N/A','Light Magic','Arcane Magic','Staff','N/A','N/A','N/A','N/A'),('2PSR','Pupil','Sorcerer',2,'N/A','N/A','Dark Magic','Arcane Magic','Staff','N/A','N/A','N/A','N/A'),('2PWD','Pupil','Wizard',2,'N/A','N/A','Arcane Magic','N/A','N/A','N/A','N/A','Attacks once for 1.5x damage','N/A'),('2RCN','Recruit','Captain',2,'N/A','N/A','Sword','Lance','N/A','N/A','N/A','+5 Crit Chance','N/A'),('2RCR','Recruit','Commander',2,'N/A','N/A','Sword','Lance','Axe','N/A','N/A','N/A','N/A'),('2RFT','Rider','Falcoknight',2,'N/A','N/A','Lance','Sword','N/A','N/A','N/A','N/A','N/A'),('2RGL','Recruit','General',2,'N/A','N/A','Lance','N/A','N/A','N/A','N/A','[Luck]% chance to ignore damage when attacked','N/A'),('2RGR','Rider','Griffon Rider',2,'N/A','N/A','Lance','N/A','N/A','N/A','N/A','Not weak to bows','N/A'),('2RHM','Rider','Horsemaster',2,'N/A','N/A','Lance','Sword','Axe','N/A','N/A','N/A','N/A'),('2RHO','Recruit','Hero',2,'N/A','N/A','Sword','Lance','Axe','N/A','N/A','N/A','N/A'),('2RHR','Recruit','Halberdier',2,'N/A','N/A','Lance','N/A','N/A','N/A','N/A','+15 Crit Chance','N/A'),('2RRR','Rider','Ranger',2,'N/A','N/A','Lance','Sword','Bow','N/A','N/A','N/A','N/A'),('2RWK','Rider','Wyvern Knight',2,'N/A','N/A','Axe','N/A','N/A','N/A','N/A','Doubled Crit Chance (any critical achieved due to the increased range ignores the target\'s defense, instead of dealing critical damage)','N/A'),('2RWL','Rider','Wyvern Lord',2,'N/A','N/A','Axe','Lance','Sword','N/A','N/A','N/A','N/A'),('2RWR','Recruit','Warrior',2,'N/A','N/A','Axe','Bow','N/A','N/A','N/A','N/A','N/A'),('2TAN','Trainee','Assassin',2,'N/A','N/A','Knife','Sword','Bow','N/A','N/A','On a critical hit, if the value is less than half of skill, the enemy is instantly killed','N/A'),('2TBD','Trainee','Bladedancer',2,'N/A','N/A','Sword','N/A','N/A','N/A','N/A','Instead of attacking, can grant another unit one more turn','N/A'),('2TBR','Trainee','Beserker',2,'N/A','N/A','Axe','Lance','Sword','N/A','N/A','During combat, can change weapons with each attack','N/A'),('2THN','Trainee','Huntsman',2,'N/A','N/A','Bow','Sword','Axe','N/A','N/A','N/A','N/A'),('2TNA','Trainee','Ninja',2,'N/A','N/A','Knife','Sword','N/A','N/A','N/A','Can only be seen within enemy unit\'s attack range','N/A'),('2TRR','Trainee','Reaver',2,'N/A','N/A','Axe','N/A','N/A','N/A','N/A','+15 Crit Chance','N/A'),('2TSM','Trainee','Swordsmaster',2,'N/A','N/A','Sword','N/A','N/A','N/A','N/A','+15 Crit Chance','N/A'),('2TSR','Trainee','Sniper',2,'N/A','N/A','Bow','N/A','N/A','N/A','N/A','+15 Crit Chance','N/A');
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-28  9:45:52
