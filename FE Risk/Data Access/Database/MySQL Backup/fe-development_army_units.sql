-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 25.13.131.6    Database: fe-development
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `army_units`
--

DROP TABLE IF EXISTS `army_units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `army_units` (
  `ID` varchar(10) NOT NULL,
  `Army_Name` varchar(45) NOT NULL,
  `Count` tinyint(2) NOT NULL,
  `Origin` varchar(45) NOT NULL,
  `Title` varchar(45) NOT NULL,
  `Level` tinyint(2) NOT NULL,
  `Tier` tinyint(1) NOT NULL,
  `Promotion_1` varchar(45) NOT NULL,
  `Promotion_2` varchar(45) NOT NULL,
  `Weapon_Type_1` varchar(45) NOT NULL,
  `Weapon_Type_2` varchar(45) NOT NULL,
  `Weapon_Type_3` varchar(45) NOT NULL,
  `Weapon_Type_4` varchar(45) NOT NULL,
  `Weapon_Type_5` varchar(45) NOT NULL,
  `Weapon_1` varchar(45) NOT NULL,
  `Weapon_2` varchar(45) NOT NULL,
  `Weapon_3` varchar(45) NOT NULL,
  `Ability_1` varchar(45) NOT NULL,
  `Ability_2` varchar(45) NOT NULL,
  `Health` tinyint(2) unsigned NOT NULL,
  `Strength` tinyint(2) unsigned NOT NULL,
  `Magic` tinyint(2) unsigned NOT NULL,
  `Skill` tinyint(2) unsigned NOT NULL,
  `Speed` tinyint(2) unsigned NOT NULL,
  `Luck` tinyint(2) unsigned NOT NULL,
  `Defense` tinyint(2) unsigned NOT NULL,
  `Resistance` tinyint(2) unsigned NOT NULL,
  `Move` tinyint(1) unsigned NOT NULL,
  `Health_Growth` tinyint(3) unsigned NOT NULL,
  `Strength_Growth` tinyint(3) unsigned NOT NULL,
  `Magic_Growth` tinyint(3) unsigned NOT NULL,
  `Skill_Growth` tinyint(3) unsigned NOT NULL,
  `Speed_Growth` tinyint(3) unsigned NOT NULL,
  `Luck_Growth` tinyint(3) unsigned NOT NULL,
  `Defense_Growth` tinyint(3) unsigned NOT NULL,
  `Resistance_Growth` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `Army_Name_idx` (`Army_Name`),
  KEY `Title_idx` (`Title`),
  CONSTRAINT `Army_Name_Units` FOREIGN KEY (`Army_Name`) REFERENCES `armies` (`Army_Name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Title_Units` FOREIGN KEY (`Title`) REFERENCES `classes` (`Unit_Title`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `army_units`
--
-- ORDER BY:  `ID`

LOCK TABLES `army_units` WRITE;
/*!40000 ALTER TABLE `army_units` DISABLE KEYS */;
INSERT INTO `army_units` VALUES ('1ICCMYJ371','Mystic Knights',5,'Initiate','Cleric',1,1,'Vindicator','Paladin','Axe','Light Magic','Staff','N/A','N/A','Bronze Axe','Light','Heal','N/A','N/A',22,8,9,6,7,4,7,8,5,90,50,60,35,40,20,35,45),('1IIRMYJ371','Mystic Knights',8,'Initiate','Impaler',1,1,'Dread Knight','Heretic','Lance','Dark Magic','N/A','N/A','N/A','Bronze Lance','Flux','N/A','N/A','N/A',17,9,8,10,9,5,7,5,5,60,60,50,65,50,20,35,25),('1ISDMYJ371','Mystic Knights',7,'Initiate','Spellsword',1,1,'Night Blade','Stalker','Sword','Arcane Magic','N/A','N/A','N/A','Bronze Sword','Fire','N/A','N/A','N/A',19,8,8,9,10,5,4,5,5,70,50,50,55,60,30,15,25),('1PAMDRJ947','Draconic Mages',10,'Pupil','Mage',1,1,'Archmage','Wizard','Arcane Magic','N/A','N/A','N/A','N/A','Fire','N/A','N/A','N/A','N/A',17,5,11,9,9,7,5,7,5,60,30,70,55,50,40,25,35),('1PAMGRJ389','Grima\'s Hope',7,'Pupil','Mage',1,1,'Archmage','Wizard','Arcane Magic','N/A','N/A','N/A','N/A','Fire','N/A','N/A','N/A','N/A',17,5,11,9,9,7,5,7,5,60,30,70,55,50,40,25,35),('1PDMDRJ947','Draconic Mages',6,'Pupil','Dark Mage',1,1,'Sorcerer','Harbinger','Dark Magic','N/A','N/A','N/A','N/A','Flux','N/A','N/A','N/A','N/A',20,5,11,9,7,5,7,5,5,80,30,70,55,40,30,35,25),('1PDMGRJ389','Grima\'s Hope',8,'Pupil','Dark Mage',1,1,'Sorcerer','Harbinger','Dark Magic','N/A','N/A','N/A','N/A','Flux','N/A','N/A','N/A','N/A',20,5,11,9,7,5,7,5,5,80,30,70,55,40,30,35,25),('1PLMDRJ947','Draconic Mages',4,'Pupil','Light Mage',1,1,'Prophet','Sage','Light Magic','N/A','N/A','N/A','N/A','Light','N/A','N/A','N/A','N/A',17,5,9,7,7,7,5,10,5,60,30,60,45,40,40,25,55),('1PLMGRJ389','Grima\'s Hope',5,'Pupil','Light Mage',1,1,'Prophet','Sage','Light Magic','N/A','N/A','N/A','N/A','Light','N/A','N/A','N/A','N/A',17,5,9,7,7,7,5,10,5,60,30,60,45,40,40,25,55),('1RCRDAJ446','Dark Riders',12,'Rider','Cavalier',1,1,'Horsemaster','Ranger','Lance','Sword','N/A','N/A','N/A','Bronze Lance','Bronze Sword','N/A','N/A','N/A',19,9,5,10,7,5,7,5,7,70,60,30,65,40,30,45,25),('1RCRSHJ263','Shadow Mounts',4,'Rider','Cavalier',1,1,'Horsemaster','Ranger','Lance','Sword','N/A','N/A','N/A','Bronze Lance','Bronze Sword','N/A','N/A','N/A',19,9,5,10,7,5,7,5,7,70,60,30,65,40,30,45,25),('1RFRARJ879','Arcane Warriors',3,'Recruit','Fighter',1,1,'Warrior','Hero','Axe','N/A','N/A','N/A','N/A','Bronze Axe','N/A','N/A','N/A','N/A',20,11,5,7,7,7,7,5,5,80,70,30,45,40,40,35,25),('1RFRMAJ423','Marching Bulls',5,'Recruit','Fighter',1,1,'Warrior','Hero','Axe','N/A','N/A','N/A','N/A','Bronze Axe','N/A','N/A','N/A','N/A',20,11,5,7,7,7,7,5,5,80,70,30,45,40,40,35,25),('1RKTARJ879','Arcane Warriors',6,'Recruit','Knight',1,1,'General','Commander','Lance','N/A','N/A','N/A','N/A','Bronze Lance','N/A','N/A','N/A','N/A',20,9,5,7,5,7,10,7,4,80,60,30,45,30,40,55,35),('1RKTMAJ423','Marching Bulls',3,'Recruit','Knight',1,1,'General','Commander','Lance','N/A','N/A','N/A','N/A','Bronze Lance','N/A','N/A','N/A','N/A',20,9,5,7,5,7,10,7,4,80,60,30,45,30,40,55,35),('1RMYARJ879','Arcane Warriors',7,'Recruit','Mercenary',1,1,'Captain','Hero','Sword','N/A','N/A','N/A','N/A','Bronze Sword','N/A','N/A','N/A','N/A',19,9,5,10,9,5,7,5,5,70,60,30,65,50,30,35,25),('1RMYMAJ423','Marching Bulls',6,'Recruit','Mercenary',1,1,'Captain','Hero','Sword','N/A','N/A','N/A','N/A','Bronze Sword','N/A','N/A','N/A','N/A',19,9,5,10,9,5,7,5,5,70,60,30,65,50,30,35,25),('1RPKDAJ446','Dark Riders',3,'Rider','Pegasus Knight',1,1,'Griffon Rider','Falcoknight','Lance','N/A','N/A','N/A','N/A','Bronze Lance','N/A','N/A','N/A','N/A',17,6,5,9,10,7,7,8,7,60,40,30,55,60,40,35,45),('1RPKSHJ263','Shadow Mounts',7,'Rider','Pegasus Knight',1,1,'Griffon Rider','Falcoknight','Lance','N/A','N/A','N/A','N/A','Bronze Lance','N/A','N/A','N/A','N/A',17,6,5,9,10,7,7,8,7,60,40,30,55,60,40,35,45),('1RSRARJ879','Arcane Warriors',4,'Recruit','Soldier',1,1,'Halberdier','Hero','Lance','N/A','N/A','N/A','N/A','Bronze Lance','N/A','N/A','N/A','N/A',19,9,5,10,9,5,7,5,5,70,60,30,65,50,30,35,25),('1RSRMAJ423','Marching Bulls',6,'Recruit','Soldier',1,1,'Halberdier','Hero','Lance','N/A','N/A','N/A','N/A','Bronze Lance','N/A','N/A','N/A','N/A',19,9,5,10,9,5,7,5,5,70,60,30,65,50,30,35,25),('1RWRDAJ446','Dark Riders',5,'Rider','Wyvern Rider',1,1,'Wyvern Knight','Wyvern Lord','Axe','N/A','N/A','N/A','N/A','Bronze Axe','N/A','N/A','N/A','N/A',20,11,5,7,5,7,8,5,7,80,70,30,45,30,40,45,25),('1RWRSHJ263','Shadow Mounts',9,'Rider','Wyvern Rider',1,1,'Wyvern Knight','Wyvern Lord','Axe','N/A','N/A','N/A','N/A','Bronze Axe','N/A','N/A','N/A','N/A',20,11,5,7,5,7,8,5,7,80,70,30,45,30,40,45,25),('1TARDIJ914','Divine Killers',3,'Trainee','Archer',1,1,'Sniper','Huntsman','Bow','N/A','N/A','N/A','N/A','Bronze Bow','N/A','N/A','+5 Crit Chance','N/A',17,8,5,10,9,7,7,7,5,60,50,30,65,50,40,35,35),('1TARGLJ291','Gliding Professionals',5,'Trainee','Archer',1,1,'Sniper','Huntsman','Bow','N/A','N/A','N/A','N/A','Bronze Bow','N/A','N/A','+5 Crit Chance','N/A',17,8,5,10,9,7,7,7,5,60,50,30,65,50,40,35,35),('1TMNDIJ914','Divine Killers',4,'Trainee','Myrmidon',1,1,'Swordsmaster','Bladedancer','Sword','N/A','N/A','N/A','N/A','Bronze Sword','N/A','N/A','+5 Crit Chance','N/A',17,8,5,9,10,8,7,5,5,60,50,30,55,60,50,35,25),('1TMNGLJ291','Gliding Professionals',7,'Trainee','Myrmidon',1,1,'Swordsmaster','Bladedancer','Sword','N/A','N/A','N/A','N/A','Bronze Sword','N/A','N/A','+5 Crit Chance','N/A',17,8,5,9,10,8,7,5,5,60,50,30,55,60,50,35,25),('1TPEDIJ914','Divine Killers',6,'Trainee','Pirate',1,1,'Reaver','Beserker','Axe','N/A','N/A','N/A','N/A','Bronze Axe','N/A','N/A','+5 Crit Chance','Moves freely through water',20,11,5,6,7,5,8,7,5,80,70,30,35,40,30,45,35),('1TPEGLJ291','Gliding Professionals',5,'Trainee','Pirate',1,1,'Reaver','Beserker','Axe','N/A','N/A','N/A','N/A','Bronze Axe','N/A','N/A','+5 Crit Chance','Moves freely through water',20,11,5,6,7,5,8,7,5,80,70,30,35,40,30,45,35),('1TTFDIJ914','Divine Killers',7,'Trainee','Thief',1,1,'Assassin','Ninja','Knife','N/A','N/A','N/A','N/A','Bronze Dagger','N/A','N/A','Lockpick','N/A',19,8,5,9,10,8,5,5,6,70,50,30,55,60,50,25,25),('1TTFGLJ291','Gliding Professionals',3,'Trainee','Thief',1,1,'Assassin','Ninja','Knife','N/A','N/A','N/A','N/A','Bronze Dagger','N/A','N/A','Lockpick','N/A',19,8,5,9,10,8,5,5,6,70,50,30,55,60,50,25,25);
/*!40000 ALTER TABLE `army_units` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-28  9:45:52
