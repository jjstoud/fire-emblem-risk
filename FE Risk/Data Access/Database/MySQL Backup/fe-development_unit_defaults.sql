-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 25.13.131.6    Database: fe-development
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `unit_defaults`
--

DROP TABLE IF EXISTS `unit_defaults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit_defaults` (
  `Time_ID` varchar(5) NOT NULL DEFAULT 'N/A',
  `Time_Period` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `Weapon_Type` varchar(45) NOT NULL DEFAULT 'N/A',
  `Weapon_Name` varchar(45) NOT NULL DEFAULT 'N/A',
  PRIMARY KEY (`Time_ID`),
  UNIQUE KEY `Time_ID_UNIQUE` (`Time_ID`),
  KEY `Weapon_Name_idx` (`Weapon_Name`),
  CONSTRAINT `Weapon_Name` FOREIGN KEY (`Weapon_Name`) REFERENCES `weapons` (`Weapon_Name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The default equipment given to units based on the time period.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit_defaults`
--
-- ORDER BY:  `Time_ID`

LOCK TABLES `unit_defaults` WRITE;
/*!40000 ALTER TABLE `unit_defaults` DISABLE KEYS */;
INSERT INTO `unit_defaults` VALUES ('TP1MA',1,'Arcane Magic','Fire'),('TP1MD',1,'Dark Magic','Flux'),('TP1ML',1,'Light Magic','Light'),('TP1MT',1,'Staff','Heal'),('TP1PA',1,'Axe','Bronze Axe'),('TP1PB',1,'Bow','Bronze Bow'),('TP1PK',1,'Knife','Bronze Dagger'),('TP1PL',1,'Lance','Bronze Lance'),('TP1PS',1,'Sword','Bronze Sword');
/*!40000 ALTER TABLE `unit_defaults` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-28  9:45:51
