-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 25.13.131.6    Database: fe-development
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `thunder_magic`
--

DROP TABLE IF EXISTS `thunder_magic`;
/*!50001 DROP VIEW IF EXISTS `thunder_magic`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `thunder_magic` AS SELECT 
 1 AS `Weapon_ID`,
 1 AS `Weapon_Name`,
 1 AS `Weapon_Type`,
 1 AS `Weapon_Subtype`,
 1 AS `Might`,
 1 AS `Hit_Rate`,
 1 AS `Critical`,
 1 AS `Min_Range`,
 1 AS `Max_Range`,
 1 AS `Weight`,
 1 AS `Cost`,
 1 AS `Effect`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `wind_magic`
--

DROP TABLE IF EXISTS `wind_magic`;
/*!50001 DROP VIEW IF EXISTS `wind_magic`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `wind_magic` AS SELECT 
 1 AS `Weapon_ID`,
 1 AS `Weapon_Name`,
 1 AS `Weapon_Type`,
 1 AS `Weapon_Subtype`,
 1 AS `Might`,
 1 AS `Hit_Rate`,
 1 AS `Critical`,
 1 AS `Min_Range`,
 1 AS `Max_Range`,
 1 AS `Weight`,
 1 AS `Cost`,
 1 AS `Effect`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `special_weapons`
--

DROP TABLE IF EXISTS `special_weapons`;
/*!50001 DROP VIEW IF EXISTS `special_weapons`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `special_weapons` AS SELECT 
 1 AS `Weapon_ID`,
 1 AS `Weapon_Name`,
 1 AS `Weapon_Type`,
 1 AS `Weapon_Subtype`,
 1 AS `Might`,
 1 AS `Hit_Rate`,
 1 AS `Critical`,
 1 AS `Min_Range`,
 1 AS `Max_Range`,
 1 AS `Weight`,
 1 AS `Cost`,
 1 AS `Effect`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `light_magic`
--

DROP TABLE IF EXISTS `light_magic`;
/*!50001 DROP VIEW IF EXISTS `light_magic`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `light_magic` AS SELECT 
 1 AS `Weapon_ID`,
 1 AS `Weapon_Name`,
 1 AS `Weapon_Type`,
 1 AS `Weapon_Subtype`,
 1 AS `Might`,
 1 AS `Hit_Rate`,
 1 AS `Critical`,
 1 AS `Min_Range`,
 1 AS `Max_Range`,
 1 AS `Weight`,
 1 AS `Cost`,
 1 AS `Effect`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `physical_weapons`
--

DROP TABLE IF EXISTS `physical_weapons`;
/*!50001 DROP VIEW IF EXISTS `physical_weapons`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `physical_weapons` AS SELECT 
 1 AS `Weapon_ID`,
 1 AS `Weapon_Name`,
 1 AS `Weapon_Type`,
 1 AS `Weapon_Subtype`,
 1 AS `Might`,
 1 AS `Hit_Rate`,
 1 AS `Critical`,
 1 AS `Min_Range`,
 1 AS `Max_Range`,
 1 AS `Weight`,
 1 AS `Cost`,
 1 AS `Effect`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `staves`
--

DROP TABLE IF EXISTS `staves`;
/*!50001 DROP VIEW IF EXISTS `staves`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `staves` AS SELECT 
 1 AS `Weapon_ID`,
 1 AS `Weapon_Name`,
 1 AS `Weapon_Type`,
 1 AS `Weapon_Subtype`,
 1 AS `Might`,
 1 AS `Hit_Rate`,
 1 AS `Critical`,
 1 AS `Min_Range`,
 1 AS `Max_Range`,
 1 AS `Weight`,
 1 AS `Cost`,
 1 AS `Effect`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `magic_weapons`
--

DROP TABLE IF EXISTS `magic_weapons`;
/*!50001 DROP VIEW IF EXISTS `magic_weapons`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `magic_weapons` AS SELECT 
 1 AS `Weapon_ID`,
 1 AS `Weapon_Name`,
 1 AS `Weapon_Type`,
 1 AS `Weapon_Subtype`,
 1 AS `Might`,
 1 AS `Hit_Rate`,
 1 AS `Critical`,
 1 AS `Min_Range`,
 1 AS `Max_Range`,
 1 AS `Weight`,
 1 AS `Cost`,
 1 AS `Effect`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `top_generals`
--

DROP TABLE IF EXISTS `top_generals`;
/*!50001 DROP VIEW IF EXISTS `top_generals`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `top_generals` AS SELECT 
 1 AS `Army_Name`,
 1 AS `Name`,
 1 AS `Title`,
 1 AS `Level`,
 1 AS `Tier`,
 1 AS `Rating`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `default_weapons`
--

DROP TABLE IF EXISTS `default_weapons`;
/*!50001 DROP VIEW IF EXISTS `default_weapons`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `default_weapons` AS SELECT 
 1 AS `Weapon_ID`,
 1 AS `Weapon_Name`,
 1 AS `Weapon_Type`,
 1 AS `Weapon_Subtype`,
 1 AS `Might`,
 1 AS `Hit_Rate`,
 1 AS `Critical`,
 1 AS `Min_Range`,
 1 AS `Max_Range`,
 1 AS `Weight`,
 1 AS `Cost`,
 1 AS `Effect`,
 1 AS `Time_Period`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `axes`
--

DROP TABLE IF EXISTS `axes`;
/*!50001 DROP VIEW IF EXISTS `axes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `axes` AS SELECT 
 1 AS `Weapon_ID`,
 1 AS `Weapon_Name`,
 1 AS `Weapon_Type`,
 1 AS `Weapon_Subtype`,
 1 AS `Might`,
 1 AS `Hit_Rate`,
 1 AS `Critical`,
 1 AS `Min_Range`,
 1 AS `Max_Range`,
 1 AS `Weight`,
 1 AS `Cost`,
 1 AS `Effect`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `swords`
--

DROP TABLE IF EXISTS `swords`;
/*!50001 DROP VIEW IF EXISTS `swords`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `swords` AS SELECT 
 1 AS `Weapon_ID`,
 1 AS `Weapon_Name`,
 1 AS `Weapon_Type`,
 1 AS `Weapon_Subtype`,
 1 AS `Might`,
 1 AS `Hit_Rate`,
 1 AS `Critical`,
 1 AS `Min_Range`,
 1 AS `Max_Range`,
 1 AS `Weight`,
 1 AS `Cost`,
 1 AS `Effect`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `dark_magic`
--

DROP TABLE IF EXISTS `dark_magic`;
/*!50001 DROP VIEW IF EXISTS `dark_magic`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `dark_magic` AS SELECT 
 1 AS `Weapon_ID`,
 1 AS `Weapon_Name`,
 1 AS `Weapon_Type`,
 1 AS `Weapon_Subtype`,
 1 AS `Might`,
 1 AS `Hit_Rate`,
 1 AS `Critical`,
 1 AS `Min_Range`,
 1 AS `Max_Range`,
 1 AS `Weight`,
 1 AS `Cost`,
 1 AS `Effect`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `bows`
--

DROP TABLE IF EXISTS `bows`;
/*!50001 DROP VIEW IF EXISTS `bows`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `bows` AS SELECT 
 1 AS `Weapon_ID`,
 1 AS `Weapon_Name`,
 1 AS `Weapon_Type`,
 1 AS `Weapon_Subtype`,
 1 AS `Might`,
 1 AS `Hit_Rate`,
 1 AS `Critical`,
 1 AS `Min_Range`,
 1 AS `Max_Range`,
 1 AS `Weight`,
 1 AS `Cost`,
 1 AS `Effect`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `knives`
--

DROP TABLE IF EXISTS `knives`;
/*!50001 DROP VIEW IF EXISTS `knives`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `knives` AS SELECT 
 1 AS `Weapon_ID`,
 1 AS `Weapon_Name`,
 1 AS `Weapon_Type`,
 1 AS `Weapon_Subtype`,
 1 AS `Might`,
 1 AS `Hit_Rate`,
 1 AS `Critical`,
 1 AS `Min_Range`,
 1 AS `Max_Range`,
 1 AS `Weight`,
 1 AS `Cost`,
 1 AS `Effect`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `lances`
--

DROP TABLE IF EXISTS `lances`;
/*!50001 DROP VIEW IF EXISTS `lances`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `lances` AS SELECT 
 1 AS `Weapon_ID`,
 1 AS `Weapon_Name`,
 1 AS `Weapon_Type`,
 1 AS `Weapon_Subtype`,
 1 AS `Might`,
 1 AS `Hit_Rate`,
 1 AS `Critical`,
 1 AS `Min_Range`,
 1 AS `Max_Range`,
 1 AS `Weight`,
 1 AS `Cost`,
 1 AS `Effect`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `class_stats_base`
--

DROP TABLE IF EXISTS `class_stats_base`;
/*!50001 DROP VIEW IF EXISTS `class_stats_base`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `class_stats_base` AS SELECT 
 1 AS `Unit_ID`,
 1 AS `Unit_Title`,
 1 AS `Move`,
 1 AS `Health`,
 1 AS `Strength`,
 1 AS `Magic`,
 1 AS `Skill`,
 1 AS `Speed`,
 1 AS `Luck`,
 1 AS `Defense`,
 1 AS `Resistance`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `fire_magic`
--

DROP TABLE IF EXISTS `fire_magic`;
/*!50001 DROP VIEW IF EXISTS `fire_magic`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `fire_magic` AS SELECT 
 1 AS `Weapon_ID`,
 1 AS `Weapon_Name`,
 1 AS `Weapon_Type`,
 1 AS `Weapon_Subtype`,
 1 AS `Might`,
 1 AS `Hit_Rate`,
 1 AS `Critical`,
 1 AS `Min_Range`,
 1 AS `Max_Range`,
 1 AS `Weight`,
 1 AS `Cost`,
 1 AS `Effect`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `class_growth`
--

DROP TABLE IF EXISTS `class_growth`;
/*!50001 DROP VIEW IF EXISTS `class_growth`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `class_growth` AS SELECT 
 1 AS `Unit_ID`,
 1 AS `Unit_Title`,
 1 AS `Health_Growth`,
 1 AS `Strength_Growth`,
 1 AS `Magic_Growth`,
 1 AS `Skill_Growth`,
 1 AS `Speed_Growth`,
 1 AS `Luck_Growth`,
 1 AS `Defense_Growth`,
 1 AS `Resistance_Growth`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `thunder_magic`
--

/*!50001 DROP VIEW IF EXISTS `thunder_magic`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `thunder_magic` AS select `weapons`.`Weapon_ID` AS `Weapon_ID`,`weapons`.`Weapon_Name` AS `Weapon_Name`,`weapons`.`Weapon_Type` AS `Weapon_Type`,`weapons`.`Weapon_Subtype` AS `Weapon_Subtype`,`weapons`.`Might` AS `Might`,`weapons`.`Hit_Rate` AS `Hit_Rate`,`weapons`.`Critical` AS `Critical`,`weapons`.`Min_Range` AS `Min_Range`,`weapons`.`Max_Range` AS `Max_Range`,`weapons`.`Weight` AS `Weight`,`weapons`.`Cost` AS `Cost`,`weapons`.`Effect` AS `Effect` from `weapons` where (`weapons`.`Weapon_Subtype` = 'Thunder Magic') order by `weapons`.`Cost` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `wind_magic`
--

/*!50001 DROP VIEW IF EXISTS `wind_magic`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `wind_magic` AS select `weapons`.`Weapon_ID` AS `Weapon_ID`,`weapons`.`Weapon_Name` AS `Weapon_Name`,`weapons`.`Weapon_Type` AS `Weapon_Type`,`weapons`.`Weapon_Subtype` AS `Weapon_Subtype`,`weapons`.`Might` AS `Might`,`weapons`.`Hit_Rate` AS `Hit_Rate`,`weapons`.`Critical` AS `Critical`,`weapons`.`Min_Range` AS `Min_Range`,`weapons`.`Max_Range` AS `Max_Range`,`weapons`.`Weight` AS `Weight`,`weapons`.`Cost` AS `Cost`,`weapons`.`Effect` AS `Effect` from `weapons` where (`weapons`.`Weapon_Subtype` = 'Wind Magic') order by `weapons`.`Cost` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `special_weapons`
--

/*!50001 DROP VIEW IF EXISTS `special_weapons`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `special_weapons` AS select `weapons`.`Weapon_ID` AS `Weapon_ID`,`weapons`.`Weapon_Name` AS `Weapon_Name`,`weapons`.`Weapon_Type` AS `Weapon_Type`,`weapons`.`Weapon_Subtype` AS `Weapon_Subtype`,`weapons`.`Might` AS `Might`,`weapons`.`Hit_Rate` AS `Hit_Rate`,`weapons`.`Critical` AS `Critical`,`weapons`.`Min_Range` AS `Min_Range`,`weapons`.`Max_Range` AS `Max_Range`,`weapons`.`Weight` AS `Weight`,`weapons`.`Cost` AS `Cost`,`weapons`.`Effect` AS `Effect` from `weapons` where (`weapons`.`Effect` is not null) order by `weapons`.`Cost` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `light_magic`
--

/*!50001 DROP VIEW IF EXISTS `light_magic`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `light_magic` AS select `weapons`.`Weapon_ID` AS `Weapon_ID`,`weapons`.`Weapon_Name` AS `Weapon_Name`,`weapons`.`Weapon_Type` AS `Weapon_Type`,`weapons`.`Weapon_Subtype` AS `Weapon_Subtype`,`weapons`.`Might` AS `Might`,`weapons`.`Hit_Rate` AS `Hit_Rate`,`weapons`.`Critical` AS `Critical`,`weapons`.`Min_Range` AS `Min_Range`,`weapons`.`Max_Range` AS `Max_Range`,`weapons`.`Weight` AS `Weight`,`weapons`.`Cost` AS `Cost`,`weapons`.`Effect` AS `Effect` from `weapons` where (`weapons`.`Weapon_Type` = 'Light Magic') order by `weapons`.`Cost` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `physical_weapons`
--

/*!50001 DROP VIEW IF EXISTS `physical_weapons`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `physical_weapons` AS select `weapons`.`Weapon_ID` AS `Weapon_ID`,`weapons`.`Weapon_Name` AS `Weapon_Name`,`weapons`.`Weapon_Type` AS `Weapon_Type`,`weapons`.`Weapon_Subtype` AS `Weapon_Subtype`,`weapons`.`Might` AS `Might`,`weapons`.`Hit_Rate` AS `Hit_Rate`,`weapons`.`Critical` AS `Critical`,`weapons`.`Min_Range` AS `Min_Range`,`weapons`.`Max_Range` AS `Max_Range`,`weapons`.`Weight` AS `Weight`,`weapons`.`Cost` AS `Cost`,`weapons`.`Effect` AS `Effect` from `weapons` where ((`weapons`.`Weapon_Type` = 'Sword') or (`weapons`.`Weapon_Type` = 'Axe') or (`weapons`.`Weapon_Type` = 'Lance') or (`weapons`.`Weapon_Type` = 'Knife') or (`weapons`.`Weapon_Type` = 'Bow')) order by `weapons`.`Cost` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `staves`
--

/*!50001 DROP VIEW IF EXISTS `staves`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `staves` AS select `weapons`.`Weapon_ID` AS `Weapon_ID`,`weapons`.`Weapon_Name` AS `Weapon_Name`,`weapons`.`Weapon_Type` AS `Weapon_Type`,`weapons`.`Weapon_Subtype` AS `Weapon_Subtype`,`weapons`.`Might` AS `Might`,`weapons`.`Hit_Rate` AS `Hit_Rate`,`weapons`.`Critical` AS `Critical`,`weapons`.`Min_Range` AS `Min_Range`,`weapons`.`Max_Range` AS `Max_Range`,`weapons`.`Weight` AS `Weight`,`weapons`.`Cost` AS `Cost`,`weapons`.`Effect` AS `Effect` from `weapons` where (`weapons`.`Weapon_Type` = 'Staff') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `magic_weapons`
--

/*!50001 DROP VIEW IF EXISTS `magic_weapons`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `magic_weapons` AS select `weapons`.`Weapon_ID` AS `Weapon_ID`,`weapons`.`Weapon_Name` AS `Weapon_Name`,`weapons`.`Weapon_Type` AS `Weapon_Type`,`weapons`.`Weapon_Subtype` AS `Weapon_Subtype`,`weapons`.`Might` AS `Might`,`weapons`.`Hit_Rate` AS `Hit_Rate`,`weapons`.`Critical` AS `Critical`,`weapons`.`Min_Range` AS `Min_Range`,`weapons`.`Max_Range` AS `Max_Range`,`weapons`.`Weight` AS `Weight`,`weapons`.`Cost` AS `Cost`,`weapons`.`Effect` AS `Effect` from `weapons` where ((`weapons`.`Weapon_Type` = 'Fire Magic') or (`weapons`.`Weapon_Type` = 'Dark Magic') or (`weapons`.`Weapon_Type` = 'Light Magic') or (`weapons`.`Weapon_Type` = 'Wind Magic') or (`weapons`.`Weapon_Type` = 'Thunder Magic')) order by `weapons`.`Cost` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `top_generals`
--

/*!50001 DROP VIEW IF EXISTS `top_generals`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `top_generals` AS select `army_generals`.`Army_Name` AS `Army_Name`,`army_generals`.`Name` AS `Name`,`army_generals`.`Title` AS `Title`,`army_generals`.`Level` AS `Level`,`army_generals`.`Tier` AS `Tier`,(((((((`army_generals`.`Health` + `army_generals`.`Strength`) + `army_generals`.`Magic`) + `army_generals`.`Skill`) + `army_generals`.`Speed`) + `army_generals`.`Luck`) + `army_generals`.`Defense`) + `army_generals`.`Resistance`) AS `Rating` from `army_generals` order by (((((((`army_generals`.`Health` + `army_generals`.`Strength`) + `army_generals`.`Magic`) + `army_generals`.`Skill`) + `army_generals`.`Speed`) + `army_generals`.`Luck`) + `army_generals`.`Defense`) + `army_generals`.`Resistance`) desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `default_weapons`
--

/*!50001 DROP VIEW IF EXISTS `default_weapons`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `default_weapons` AS select `weapons`.`Weapon_ID` AS `Weapon_ID`,`weapons`.`Weapon_Name` AS `Weapon_Name`,`weapons`.`Weapon_Type` AS `Weapon_Type`,`weapons`.`Weapon_Subtype` AS `Weapon_Subtype`,`weapons`.`Might` AS `Might`,`weapons`.`Hit_Rate` AS `Hit_Rate`,`weapons`.`Critical` AS `Critical`,`weapons`.`Min_Range` AS `Min_Range`,`weapons`.`Max_Range` AS `Max_Range`,`weapons`.`Weight` AS `Weight`,`weapons`.`Cost` AS `Cost`,`weapons`.`Effect` AS `Effect`,`unit_defaults`.`Time_Period` AS `Time_Period` from (`weapons` join `unit_defaults`) where (`weapons`.`Weapon_Name` = `unit_defaults`.`Weapon_Name`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `axes`
--

/*!50001 DROP VIEW IF EXISTS `axes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `axes` AS select `weapons`.`Weapon_ID` AS `Weapon_ID`,`weapons`.`Weapon_Name` AS `Weapon_Name`,`weapons`.`Weapon_Type` AS `Weapon_Type`,`weapons`.`Weapon_Subtype` AS `Weapon_Subtype`,`weapons`.`Might` AS `Might`,`weapons`.`Hit_Rate` AS `Hit_Rate`,`weapons`.`Critical` AS `Critical`,`weapons`.`Min_Range` AS `Min_Range`,`weapons`.`Max_Range` AS `Max_Range`,`weapons`.`Weight` AS `Weight`,`weapons`.`Cost` AS `Cost`,`weapons`.`Effect` AS `Effect` from `weapons` where (`weapons`.`Weapon_Type` = 'Axe') order by `weapons`.`Cost` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `swords`
--

/*!50001 DROP VIEW IF EXISTS `swords`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `swords` AS select `weapons`.`Weapon_ID` AS `Weapon_ID`,`weapons`.`Weapon_Name` AS `Weapon_Name`,`weapons`.`Weapon_Type` AS `Weapon_Type`,`weapons`.`Weapon_Subtype` AS `Weapon_Subtype`,`weapons`.`Might` AS `Might`,`weapons`.`Hit_Rate` AS `Hit_Rate`,`weapons`.`Critical` AS `Critical`,`weapons`.`Min_Range` AS `Min_Range`,`weapons`.`Max_Range` AS `Max_Range`,`weapons`.`Weight` AS `Weight`,`weapons`.`Cost` AS `Cost`,`weapons`.`Effect` AS `Effect` from `weapons` where (`weapons`.`Weapon_Type` = 'Sword') order by `weapons`.`Cost` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `dark_magic`
--

/*!50001 DROP VIEW IF EXISTS `dark_magic`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `dark_magic` AS select `weapons`.`Weapon_ID` AS `Weapon_ID`,`weapons`.`Weapon_Name` AS `Weapon_Name`,`weapons`.`Weapon_Type` AS `Weapon_Type`,`weapons`.`Weapon_Subtype` AS `Weapon_Subtype`,`weapons`.`Might` AS `Might`,`weapons`.`Hit_Rate` AS `Hit_Rate`,`weapons`.`Critical` AS `Critical`,`weapons`.`Min_Range` AS `Min_Range`,`weapons`.`Max_Range` AS `Max_Range`,`weapons`.`Weight` AS `Weight`,`weapons`.`Cost` AS `Cost`,`weapons`.`Effect` AS `Effect` from `weapons` where (`weapons`.`Weapon_Type` = 'Dark Magic') order by `weapons`.`Cost` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `bows`
--

/*!50001 DROP VIEW IF EXISTS `bows`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `bows` AS select `weapons`.`Weapon_ID` AS `Weapon_ID`,`weapons`.`Weapon_Name` AS `Weapon_Name`,`weapons`.`Weapon_Type` AS `Weapon_Type`,`weapons`.`Weapon_Subtype` AS `Weapon_Subtype`,`weapons`.`Might` AS `Might`,`weapons`.`Hit_Rate` AS `Hit_Rate`,`weapons`.`Critical` AS `Critical`,`weapons`.`Min_Range` AS `Min_Range`,`weapons`.`Max_Range` AS `Max_Range`,`weapons`.`Weight` AS `Weight`,`weapons`.`Cost` AS `Cost`,`weapons`.`Effect` AS `Effect` from `weapons` where (`weapons`.`Weapon_Type` = 'Bow') order by `weapons`.`Cost` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `knives`
--

/*!50001 DROP VIEW IF EXISTS `knives`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `knives` AS select `weapons`.`Weapon_ID` AS `Weapon_ID`,`weapons`.`Weapon_Name` AS `Weapon_Name`,`weapons`.`Weapon_Type` AS `Weapon_Type`,`weapons`.`Weapon_Subtype` AS `Weapon_Subtype`,`weapons`.`Might` AS `Might`,`weapons`.`Hit_Rate` AS `Hit_Rate`,`weapons`.`Critical` AS `Critical`,`weapons`.`Min_Range` AS `Min_Range`,`weapons`.`Max_Range` AS `Max_Range`,`weapons`.`Weight` AS `Weight`,`weapons`.`Cost` AS `Cost`,`weapons`.`Effect` AS `Effect` from `weapons` where (`weapons`.`Weapon_Type` = 'Knife') order by `weapons`.`Cost` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `lances`
--

/*!50001 DROP VIEW IF EXISTS `lances`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `lances` AS select `weapons`.`Weapon_ID` AS `Weapon_ID`,`weapons`.`Weapon_Name` AS `Weapon_Name`,`weapons`.`Weapon_Type` AS `Weapon_Type`,`weapons`.`Weapon_Subtype` AS `Weapon_Subtype`,`weapons`.`Might` AS `Might`,`weapons`.`Hit_Rate` AS `Hit_Rate`,`weapons`.`Critical` AS `Critical`,`weapons`.`Min_Range` AS `Min_Range`,`weapons`.`Max_Range` AS `Max_Range`,`weapons`.`Weight` AS `Weight`,`weapons`.`Cost` AS `Cost`,`weapons`.`Effect` AS `Effect` from `weapons` where (`weapons`.`Weapon_Type` = 'Lance') order by `weapons`.`Cost` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `class_stats_base`
--

/*!50001 DROP VIEW IF EXISTS `class_stats_base`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `class_stats_base` AS select `class_stats_full`.`Unit_ID` AS `Unit_ID`,`class_stats_full`.`Unit_Title` AS `Unit_Title`,`class_stats_full`.`Move` AS `Move`,`class_stats_full`.`Health` AS `Health`,`class_stats_full`.`Strength` AS `Strength`,`class_stats_full`.`Magic` AS `Magic`,`class_stats_full`.`Skill` AS `Skill`,`class_stats_full`.`Speed` AS `Speed`,`class_stats_full`.`Luck` AS `Luck`,`class_stats_full`.`Defense` AS `Defense`,`class_stats_full`.`Resistance` AS `Resistance` from `class_stats_full` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `fire_magic`
--

/*!50001 DROP VIEW IF EXISTS `fire_magic`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `fire_magic` AS select `weapons`.`Weapon_ID` AS `Weapon_ID`,`weapons`.`Weapon_Name` AS `Weapon_Name`,`weapons`.`Weapon_Type` AS `Weapon_Type`,`weapons`.`Weapon_Subtype` AS `Weapon_Subtype`,`weapons`.`Might` AS `Might`,`weapons`.`Hit_Rate` AS `Hit_Rate`,`weapons`.`Critical` AS `Critical`,`weapons`.`Min_Range` AS `Min_Range`,`weapons`.`Max_Range` AS `Max_Range`,`weapons`.`Weight` AS `Weight`,`weapons`.`Cost` AS `Cost`,`weapons`.`Effect` AS `Effect` from `weapons` where (`weapons`.`Weapon_Subtype` = 'Fire Magic') order by `weapons`.`Cost` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `class_growth`
--

/*!50001 DROP VIEW IF EXISTS `class_growth`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`FEAdmin`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `class_growth` AS select `class_stats_full`.`Unit_ID` AS `Unit_ID`,`class_stats_full`.`Unit_Title` AS `Unit_Title`,`class_stats_full`.`Health_Growth` AS `Health_Growth`,`class_stats_full`.`Strength_Growth` AS `Strength_Growth`,`class_stats_full`.`Magic_Growth` AS `Magic_Growth`,`class_stats_full`.`Skill_Growth` AS `Skill_Growth`,`class_stats_full`.`Speed_Growth` AS `Speed_Growth`,`class_stats_full`.`Luck_Growth` AS `Luck_Growth`,`class_stats_full`.`Defense_Growth` AS `Defense_Growth`,`class_stats_full`.`Resistance_Growth` AS `Resistance_Growth` from `class_stats_full` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-28  9:45:55
