﻿using System;
using System.Data.SQLite;
using Core_Classes;
using MySql.Data.MySqlClient;

namespace Data_Access
{
    public class WeaponProvider
    {
        private readonly MySqlConnection _onlineDbConnection;
        private readonly SQLiteConnection _localDbConnection;

        private readonly bool _online;

        public WeaponProvider(string dbConnection, bool online)
        {
            _online = online;

            if (online)
            {
                _onlineDbConnection = new MySqlConnection(dbConnection);
            }

            else
            {
                _localDbConnection = new SQLiteConnection(dbConnection);
            }
        }

        public Weapon GetWeapon(string weaponName)
        {
            Weapon weapon = null;

            var sql = "SELECT Weapon_ID, Weapon_Type, Weapon_Subtype, Might, Hit_Rate, Critical, Min_Range, Max_Range, Weight, Cost, Effect FROM Weapons WHERE Weapon_Name='" + weaponName + "'";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        weapon = new Weapon((string)reader["Weapon_ID"], weaponName, (string)reader["Weapon_Type"], (string)reader["Weapon_Subtype"], (byte)reader["Might"], (byte)reader["Hit_Rate"], (byte)reader["Critical"], (byte)reader["Min_Range"], (byte)reader["Max_Range"], (byte)reader["Weight"], (ushort)reader["Cost"], (string)reader["Effect"]);
                    }

                    _onlineDbConnection.Close();
                }

                return weapon;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    weapon = new Weapon((string)reader["Weapon_ID"], weaponName, (string)reader["Weapon_Type"], (string)reader["Weapon_Subtype"], (int)reader["Might"], (int)reader["Hit_Rate"], (int)reader["Critical"], (int)reader["Min_Range"], (int)reader["Max_Range"], (int)reader["Weight"], (int)reader["Cost"], (string)reader["Effect"]);
                }

                _localDbConnection.Close();
            }

            return weapon;
        }

        public Weapon GetWeapon(int time, string weaponType)
        {
            Weapon weapon = null;

            var sql = "SELECT Weapon_ID, Weapon_Name, Weapon_Subtype, Might, Hit_Rate, Critical, Min_Range, Max_Range, Weight, Cost, Effect FROM Default_Weapons WHERE Weapon_Type='" + weaponType + "' AND Time_Period=" + time;

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        weapon = new Weapon((string)reader["Weapon_ID"], (string)reader["Weapon_Name"], weaponType, (string)reader["Weapon_Subtype"], (byte)reader["Might"], (byte)reader["Hit_Rate"], (byte)reader["Critical"], (byte)reader["Min_Range"], (byte)reader["Max_Range"], (byte)reader["Weight"], (ushort)reader["Cost"], (string)reader["Effect"]);
                    }

                    _onlineDbConnection.Close();
                }

                return weapon;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    weapon = new Weapon((string)reader["Weapon_ID"], (string)reader["Weapon_Name"], weaponType, (string)reader["Weapon_Subtype"], (int)reader["Might"], (int)reader["Hit_Rate"], (int)reader["Critical"], (int)reader["Min_Range"], (int)reader["Max_Range"], (int)reader["Weight"], (int)reader["Cost"], (string)reader["Effect"]);
                }

                _localDbConnection.Close();
            }

            return weapon;
        }
    }
}
