﻿using System;
using System.Data.SQLite;
using Core_Classes;
using MySql.Data.MySqlClient;

namespace Data_Access
{
    internal class StatsProvider
    {
        private readonly MySqlConnection _onlineDbConnection;
        private readonly SQLiteConnection _localDbConnection;

        private readonly bool _online;

        private readonly string _position;

        public StatsProvider(string dbConnection, bool online, string position)
        {
            _online = online;
            _position = position;

            if (online)
            {
                _onlineDbConnection = new MySqlConnection(dbConnection);
            }

            else
            {
                _localDbConnection = new SQLiteConnection(dbConnection);
            }
        }

        public Stats GetStats(string title)
        {
            Stats statsObj = null;

            var stats = new int[8];
            var statGrowths = new int[8];
            string sql;

            if (_position == "Unit")
            {
                sql = "SELECT Health, Strength, Magic, Skill, Speed, Luck, Defense, Resistance, Health_Growth, Strength_Growth, Magic_Growth, Skill_Growth, Speed_Growth, Luck_Growth, Defense_Growth, Resistance_Growth, Move FROM Class_Stats_Full WHERE Unit_Title='" + title + "'";
            }

            else
            {
                sql = "SELECT Health_Growth, Strength_Growth, Magic_Growth, Skill_Growth, Speed_Growth, Luck_Growth, Defense_Growth, Resistance_Growth, Move FROM Class_Stats_Full WHERE Unit_Title='" + title + "'";
            }

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        if (_position == "Unit")
                        {
                            stats[0] = (byte)reader["Health"];
                            stats[1] = (byte)reader["Strength"];
                            stats[2] = (byte)reader["Magic"];
                            stats[3] = (byte)reader["Skill"];
                            stats[4] = (byte)reader["Speed"];
                            stats[5] = (byte)reader["Luck"];
                            stats[6] = (byte)reader["Defense"];
                            stats[7] = (byte)reader["Resistance"];
                        }

                        else
                        {
                            stats[0] = 15;
                            stats[1] = 5;
                            stats[2] = 5;
                            stats[3] = 5;
                            stats[4] = 5;
                            stats[5] = 5;
                            stats[6] = 5;
                            stats[7] = 5;
                        }

                        statGrowths[0] = (byte)reader["Health_Growth"];
                        statGrowths[1] = (byte)reader["Strength_Growth"];
                        statGrowths[2] = (byte)reader["Magic_Growth"];
                        statGrowths[3] = (byte)reader["Skill_Growth"];
                        statGrowths[4] = (byte)reader["Speed_Growth"];
                        statGrowths[5] = (byte)reader["Luck_Growth"];
                        statGrowths[6] = (byte)reader["Defense_Growth"];
                        statGrowths[7] = (byte)reader["Resistance_Growth"];

                        statsObj = new Stats(stats, statGrowths, (byte)reader["Move"]);
                    }

                    _onlineDbConnection.Close();
                }

                return statsObj;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (_position == "Unit")
                    {
                        stats[0] = (int)reader["Health"];
                        stats[1] = (int)reader["Strength"];
                        stats[2] = (int)reader["Magic"];
                        stats[3] = (int)reader["Skill"];
                        stats[4] = (int)reader["Speed"];
                        stats[5] = (int)reader["Luck"];
                        stats[6] = (int)reader["Defense"];
                        stats[7] = (int)reader["Resistance"];
                    }

                    else
                    {
                        stats[0] = 15;
                        stats[1] = 5;
                        stats[2] = 5;
                        stats[3] = 5;
                        stats[4] = 5;
                        stats[5] = 5;
                        stats[6] = 5;
                        stats[7] = 5;
                    }

                    statGrowths[0] = (int)reader["Health_Growth"];
                    statGrowths[1] = (int)reader["Strength_Growth"];
                    statGrowths[2] = (int)reader["Magic_Growth"];
                    statGrowths[3] = (int)reader["Skill_Growth"];
                    statGrowths[4] = (int)reader["Speed_Growth"];
                    statGrowths[5] = (int)reader["Luck_Growth"];
                    statGrowths[6] = (int)reader["Defense_Growth"];
                    statGrowths[7] = (int)reader["Resistance_Growth"];

                    statsObj = new Stats(stats, statGrowths, (int)reader["Move"]);
                }

                _localDbConnection.Close();
            }

            return statsObj;
        }

        public Stats GetUnitStats(string id)
        {
            Stats statsObj = null;

            var stats = new int[8];
            var statGrowths = new int[8];

            var sql = "SELECT Health, Strength, Magic, Skill, Speed, Luck, Defense, Resistance, Health_Growth, Strength_Growth, Magic_Growth, Skill_Growth, Speed_Growth, Luck_Growth, Defense_Growth, Resistance_Growth, Move FROM Army_Units WHERE ID='" + id + "'";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        stats[0] = (byte)reader["Health"];
                        stats[1] = (byte)reader["Strength"];
                        stats[2] = (byte)reader["Magic"];
                        stats[3] = (byte)reader["Skill"];
                        stats[4] = (byte)reader["Speed"];
                        stats[5] = (byte)reader["Luck"];
                        stats[6] = (byte)reader["Defense"];
                        stats[7] = (byte)reader["Resistance"];

                        statGrowths[0] = (byte)reader["Health_Growth"];
                        statGrowths[1] = (byte)reader["Strength_Growth"];
                        statGrowths[2] = (byte)reader["Magic_Growth"];
                        statGrowths[3] = (byte)reader["Skill_Growth"];
                        statGrowths[4] = (byte)reader["Speed_Growth"];
                        statGrowths[5] = (byte)reader["Luck_Growth"];
                        statGrowths[6] = (byte)reader["Defense_Growth"];
                        statGrowths[7] = (byte)reader["Resistance_Growth"];

                        statsObj = new Stats(stats, statGrowths, (byte)reader["Move"]);
                    }

                    _onlineDbConnection.Close();
                }

                return statsObj;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    stats[0] = (int)reader["Health"];
                    stats[1] = (int)reader["Strength"];
                    stats[2] = (int)reader["Magic"];
                    stats[3] = (int)reader["Skill"];
                    stats[4] = (int)reader["Speed"];
                    stats[5] = (int)reader["Luck"];
                    stats[6] = (int)reader["Defense"];
                    stats[7] = (int)reader["Resistance"];

                    statGrowths[0] = (int)reader["Health_Growth"];
                    statGrowths[1] = (int)reader["Strength_Growth"];
                    statGrowths[2] = (int)reader["Magic_Growth"];
                    statGrowths[3] = (int)reader["Skill_Growth"];
                    statGrowths[4] = (int)reader["Speed_Growth"];
                    statGrowths[5] = (int)reader["Luck_Growth"];
                    statGrowths[6] = (int)reader["Defense_Growth"];
                    statGrowths[7] = (int)reader["Resistance_Growth"];

                    statsObj = new Stats(stats, statGrowths, (int)reader["Move"]);
                }

                _localDbConnection.Close();
            }

            return statsObj;
        }

        public Stats GetGeneralStats(string id)
        {
            Stats statsObj = null;

            var stats = new int[8];
            var statGrowths = new int[8];

            var sql = "SELECT Health, Strength, Magic, Skill, Speed, Luck, Defense, Resistance, Health_Growth, Strength_Growth, Magic_Growth, Skill_Growth, Speed_Growth, Luck_Growth, Defense_Growth, Resistance_Growth, Move FROM Army_Generals WHERE ID='" + id + "'";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        stats[0] = (byte)reader["Health"];
                        stats[1] = (byte)reader["Strength"];
                        stats[2] = (byte)reader["Magic"];
                        stats[3] = (byte)reader["Skill"];
                        stats[4] = (byte)reader["Speed"];
                        stats[5] = (byte)reader["Luck"];
                        stats[6] = (byte)reader["Defense"];
                        stats[7] = (byte)reader["Resistance"];

                        statGrowths[0] = (byte)reader["Health_Growth"];
                        statGrowths[1] = (byte)reader["Strength_Growth"];
                        statGrowths[2] = (byte)reader["Magic_Growth"];
                        statGrowths[3] = (byte)reader["Skill_Growth"];
                        statGrowths[4] = (byte)reader["Speed_Growth"];
                        statGrowths[5] = (byte)reader["Luck_Growth"];
                        statGrowths[6] = (byte)reader["Defense_Growth"];
                        statGrowths[7] = (byte)reader["Resistance_Growth"];

                        statsObj = new Stats(stats, statGrowths, (byte)reader["Move"]);
                    }

                    _onlineDbConnection.Close();
                }

                return statsObj;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    stats[0] = (int)reader["Health"];
                    stats[1] = (int)reader["Strength"];
                    stats[2] = (int)reader["Magic"];
                    stats[3] = (int)reader["Skill"];
                    stats[4] = (int)reader["Speed"];
                    stats[5] = (int)reader["Luck"];
                    stats[6] = (int)reader["Defense"];
                    stats[7] = (int)reader["Resistance"];

                    statGrowths[0] = (int)reader["Health_Growth"];
                    statGrowths[1] = (int)reader["Strength_Growth"];
                    statGrowths[2] = (int)reader["Magic_Growth"];
                    statGrowths[3] = (int)reader["Skill_Growth"];
                    statGrowths[4] = (int)reader["Speed_Growth"];
                    statGrowths[5] = (int)reader["Luck_Growth"];
                    statGrowths[6] = (int)reader["Defense_Growth"];
                    statGrowths[7] = (int)reader["Resistance_Growth"];

                    statsObj = new Stats(stats, statGrowths, (int)reader["Move"]);
                }

                _localDbConnection.Close();
            }

            return statsObj;
        }
    }
}
