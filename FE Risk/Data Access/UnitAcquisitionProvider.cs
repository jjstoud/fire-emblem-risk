﻿using System;
using System.Data.SQLite;
using MySql.Data.MySqlClient;

namespace Data_Access
{
    public class UnitAcquisitionProvider
    {
        private readonly MySqlConnection _onlineDbConnection;
        private readonly SQLiteConnection _localDbConnection;

        private readonly bool _online;

        private readonly string _origin;

        public UnitAcquisitionProvider(string dbConnection, bool online, string origin)
        {
            _online = online;
            _origin = origin;

            if (online)
            {
                _onlineDbConnection = new MySqlConnection(dbConnection);
            }

            else
            {
                _localDbConnection = new SQLiteConnection(dbConnection);
            }
        }

        public int GetFirstTierUniqueUnitCount()
        {
            int uniqueCount;

            var sql = "SELECT COUNT(DISTINCT Unit_Title) FROM Classes WHERE Unit_Origin='" + _origin + "' AND Unit_Tier='1'";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    uniqueCount = Convert.ToInt32(command.ExecuteScalar());

                    _onlineDbConnection.Close();
                }

                return uniqueCount;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                uniqueCount = Convert.ToInt32(command.ExecuteScalar());

                _localDbConnection.Close();
            }

            return uniqueCount;
        }

        public string[] GetFirstTierUniqueTitleNames(string[] unitTitles)
        {
            var sql = "SELECT DISTINCT Unit_Title FROM Classes WHERE Unit_Origin='" + _origin + "' AND Unit_Tier='1'";

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    var command = new MySqlCommand(sql, _onlineDbConnection);
                    var reader = command.ExecuteReader();

                    var count = 0;

                    while (reader.Read())
                    {
                        unitTitles[count] = (string)reader["Unit_Title"];
                        count++;
                    }

                    _onlineDbConnection.Close();
                }

                return unitTitles;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                var command = new SQLiteCommand(sql, _localDbConnection);
                var reader = command.ExecuteReader();

                var count = 0;

                while (reader.Read())
                {
                    unitTitles[count] = (string)reader["Unit_Title"];
                    count++;
                }

                _localDbConnection.Close();
            }

            return unitTitles;
        }
    }
}
