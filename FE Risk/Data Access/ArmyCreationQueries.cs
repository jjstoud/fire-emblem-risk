﻿using System;
using System.Data.SQLite;
using Core_Classes;
using MySql.Data.MySqlClient;

namespace Data_Access
{
    public class ArmyCreationQueries
    {
        private readonly MySqlConnection _onlineDbConnection;
        private readonly SQLiteConnection _localDbConnection;

        private readonly bool _online;

        public ArmyCreationQueries(string dbConnection, bool online)
        {
            _online = online;

            if (online)
            {
                _onlineDbConnection = new MySqlConnection(dbConnection);
            }

            else
            {
                _localDbConnection = new SQLiteConnection(dbConnection);
            }
        }

        public bool WriteArmyInfoToDatabase(string name, string leader, string password, int gold, int land, string status)
        {
            bool success;

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    try
                    {
                        //Prepare command for insert into Armies table
                        var command = new MySqlCommand("INSERT INTO Armies (Army_Name, Army_Leader, Army_Password, Army_Gold, Army_Land, Army_Status) VALUES (@ArmyName,@ArmyLeader,@ArmyPassword,@ArmyGold,@ArmyLand,@ArmyStatus)", _onlineDbConnection);

                        //Set parameters for insert into Armies table
                        command.Parameters.AddWithValue("@ArmyName", name);
                        command.Parameters.AddWithValue("@ArmyLeader", leader);
                        command.Parameters.AddWithValue("@ArmyPassword", password);
                        command.Parameters.AddWithValue("@ArmyGold", gold);
                        command.Parameters.AddWithValue("@ArmyLand", land);
                        command.Parameters.AddWithValue("@ArmyStatus", status);

                        command.ExecuteNonQuery();

                        success = true;
                    }

                    catch (MySqlException ex)
                    {
                        Console.WriteLine(ex.Message);
                        success = false;
                    }

                    _onlineDbConnection.Close();
                }

                return success;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                try
                {
                    //Prepare command for insert into Armies table
                    var command = new SQLiteCommand("INSERT INTO Armies (Army_Name, Army_Leader, Army_Password, Army_Gold, Army_Land, Army_Status) VALUES (@ArmyName,@ArmyLeader,@ArmyPassword,@ArmyGold,@ArmyLand,@ArmyStatus)", _localDbConnection);

                    //Set parameters for insert into Armies table
                    command.Parameters.AddWithValue("@ArmyName", name);
                    command.Parameters.AddWithValue("@ArmyLeader", leader);
                    command.Parameters.AddWithValue("@ArmyPassword", password);
                    command.Parameters.AddWithValue("@ArmyGold", gold);
                    command.Parameters.AddWithValue("@ArmyLand", land);
                    command.Parameters.AddWithValue("@ArmyStatus", status);

                    command.ExecuteNonQuery();

                    success = true;
                }

                catch (SQLiteException ex)
                {
                    Console.WriteLine(ex.Message);
                    success = false;
                }

                _localDbConnection.Close();
            }

            return success;
        }

        public bool WriteUnitInfoToDatabase(Unit[] units, string name, int[] numTitles, int uniqueUnits)
        {
            bool success;

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    try
                    {
                        for (var i = 0; i < uniqueUnits; i++)
                        {
                            var command = new MySqlCommand("INSERT INTO Army_Units (ID, Army_Name, Count, Origin, Title, Level, Tier, Promotion_1, Promotion_2, Weapon_Type_1, Weapon_Type_2, Weapon_Type_3, Weapon_Type_4, Weapon_Type_5, Weapon_1, Weapon_2, Weapon_3, Ability_1, Ability_2, Health, Strength, Magic, Skill, Speed, Luck, Defense, Resistance, Move, Health_Growth, Strength_Growth, Magic_Growth, Skill_Growth, Speed_Growth, Luck_Growth, Defense_Growth, Resistance_Growth) VALUES (@ID, @ArmyName, @Count, @Origin, @Title, @Level, @Tier, @Promotion1, @Promotion2, @WeaponType1, @WeaponType2, @WeaponType3, @WeaponType4, @WeaponType5, @Weapon1, @Weapon2, @Weapon3, @Ability1, @Ability2, @Health, @Strength, @Magic, @Skill, @Speed, @Luck, @Defense, @Resistance, @Move, @HealthGrowth, @StrengthGrowth, @MagicGrowth, @SkillGrowth, @SpeedGrowth, @LuckGrowth, @DefenseGrowth, @ResistanceGrowth)", _onlineDbConnection);

                            command.Parameters.AddWithValue("@ID", units[i].Id);
                            command.Parameters.AddWithValue("@ArmyName", name);
                            command.Parameters.AddWithValue("@Count", numTitles[i]);
                            command.Parameters.AddWithValue("@Origin", units[i].Origin);
                            command.Parameters.AddWithValue("@Title", units[i].Title);
                            command.Parameters.AddWithValue("@Level", units[i].Level);
                            command.Parameters.AddWithValue("@Tier", units[i].Tier);

                            try
                            {
                                command.Parameters.AddWithValue("@Promotion1", units[i].Promotions[0]);
                            }

                            catch (IndexOutOfRangeException)
                            {
                                command.Parameters.AddWithValue("@Promotion1", "N/A");
                            }

                            try
                            {
                                command.Parameters.AddWithValue("@Promotion2", units[i].Promotions[1]);
                            }

                            catch (IndexOutOfRangeException)
                            {
                                command.Parameters.AddWithValue("@Promotion2", "N/A");
                            }

                            command.Parameters.AddWithValue("@WeaponType1", units[i].WeaponTypes[0]);

                            try
                            {
                                command.Parameters.AddWithValue("@WeaponType2", units[i].WeaponTypes[1]);
                            }

                            catch (IndexOutOfRangeException)
                            {
                                command.Parameters.AddWithValue("@WeaponType2", "N/A");
                            }

                            try
                            {
                                command.Parameters.AddWithValue("@WeaponType3", units[i].WeaponTypes[2]);
                            }

                            catch (IndexOutOfRangeException)
                            {
                                command.Parameters.AddWithValue("@WeaponType3", "N/A");
                            }

                            try
                            {
                                command.Parameters.AddWithValue("@WeaponType4", units[i].WeaponTypes[3]);
                            }

                            catch (IndexOutOfRangeException)
                            {
                                command.Parameters.AddWithValue("@WeaponType4", "N/A");
                            }

                            try
                            {
                                command.Parameters.AddWithValue("@WeaponType5", units[i].WeaponTypes[4]);
                            }

                            catch (IndexOutOfRangeException)
                            {
                                command.Parameters.AddWithValue("@WeaponType5", "N/A");
                            }

                            command.Parameters.AddWithValue("@Weapon1", units[i].Weapons[0].Name);
                            command.Parameters.AddWithValue("@Weapon2", units[i].Weapons[1].Name);
                            command.Parameters.AddWithValue("@Weapon3", units[i].Weapons[2].Name);

                            try
                            {
                                command.Parameters.AddWithValue("@Ability1", units[i].Abilities[0]);
                            }

                            catch (IndexOutOfRangeException)
                            {
                                command.Parameters.AddWithValue("@Ability1", "N/A");
                            }

                            try
                            {
                                command.Parameters.AddWithValue("@Ability2", units[i].Abilities[1]);
                            }

                            catch (IndexOutOfRangeException)
                            {
                                command.Parameters.AddWithValue("@Ability2", "N/A");
                            }

                            command.Parameters.AddWithValue("@Health", units[i].Stats.stats[0]);
                            command.Parameters.AddWithValue("@Strength", units[i].Stats.stats[1]);
                            command.Parameters.AddWithValue("@Magic", units[i].Stats.stats[2]);
                            command.Parameters.AddWithValue("@Skill", units[i].Stats.stats[3]);
                            command.Parameters.AddWithValue("@Speed", units[i].Stats.stats[4]);
                            command.Parameters.AddWithValue("@Luck", units[i].Stats.stats[5]);
                            command.Parameters.AddWithValue("@Defense", units[i].Stats.stats[6]);
                            command.Parameters.AddWithValue("@Resistance", units[i].Stats.stats[7]);
                            command.Parameters.AddWithValue("@Move", units[i].Stats.Move);
                            command.Parameters.AddWithValue("@HealthGrowth", units[i].Stats.StatGrowths[0]);
                            command.Parameters.AddWithValue("@StrengthGrowth", units[i].Stats.StatGrowths[1]);
                            command.Parameters.AddWithValue("@MagicGrowth", units[i].Stats.StatGrowths[2]);
                            command.Parameters.AddWithValue("@SkillGrowth", units[i].Stats.StatGrowths[3]);
                            command.Parameters.AddWithValue("@SpeedGrowth", units[i].Stats.StatGrowths[4]);
                            command.Parameters.AddWithValue("@LuckGrowth", units[i].Stats.StatGrowths[5]);
                            command.Parameters.AddWithValue("@DefenseGrowth", units[i].Stats.StatGrowths[6]);
                            command.Parameters.AddWithValue("@ResistanceGrowth", units[i].Stats.StatGrowths[7]);

                            command.ExecuteNonQuery();
                        }

                        success = true;
                    }

                    catch (MySqlException ex)
                    {
                        Console.WriteLine(ex.Message);
                        success = false;
                    }

                    _onlineDbConnection.Close();
                }

                return success;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                try
                {
                    for (var i = 0; i < uniqueUnits; i++)
                    {
                        var command = new SQLiteCommand("INSERT INTO Army_Units (ID, Army_Name, Count, Origin, Title, Level, Tier, Promotion_1, Promotion_2, Weapon_Type_1, Weapon_Type_2, Weapon_Type_3, Weapon_Type_4, Weapon_Type_5, Weapon_1, Weapon_2, Weapon_3, Ability_1, Ability_2, Health, Strength, Magic, Skill, Speed, Luck, Defense, Resistance, Move, Health_Growth, Strength_Growth, Magic_Growth, Skill_Growth, Speed_Growth, Luck_Growth, Defense_Growth, Resistance_Growth) VALUES (@ID, @ArmyName, @Count, @Origin, @Title, @Level, @Tier, @Promotion1, @Promotion2, @WeaponType1, @WeaponType2, @WeaponType3, @WeaponType4, @WeaponType5, @Weapon1, @Weapon2, @Weapon3, @Ability1, @Ability2, @Health, @Strength, @Magic, @Skill, @Speed, @Luck, @Defense, @Resistance, @Move, @HealthGrowth, @StrengthGrowth, @MagicGrowth, @SkillGrowth, @SpeedGrowth, @LuckGrowth, @DefenseGrowth, @ResistanceGrowth)", _localDbConnection);

                        command.Parameters.AddWithValue("@ID", units[i].Id);
                        command.Parameters.AddWithValue("@ArmyName", name);
                        command.Parameters.AddWithValue("@Count", numTitles[i]);
                        command.Parameters.AddWithValue("@Origin", units[i].Origin);
                        command.Parameters.AddWithValue("@Title", units[i].Title);
                        command.Parameters.AddWithValue("@Level", units[i].Level);
                        command.Parameters.AddWithValue("@Tier", units[i].Tier);

                        try
                        {
                            command.Parameters.AddWithValue("@Promotion1", units[i].Promotions[0]);
                        }

                        catch (IndexOutOfRangeException)
                        {
                            command.Parameters.AddWithValue("@Promotion1", "N/A");
                        }

                        try
                        {
                            command.Parameters.AddWithValue("@Promotion2", units[i].Promotions[1]);
                        }

                        catch (IndexOutOfRangeException)
                        {
                            command.Parameters.AddWithValue("@Promotion2", "N/A");
                        }

                        command.Parameters.AddWithValue("@WeaponType1", units[i].WeaponTypes[0]);

                        try
                        {
                            command.Parameters.AddWithValue("@WeaponType2", units[i].WeaponTypes[1]);
                        }

                        catch (IndexOutOfRangeException)
                        {
                            command.Parameters.AddWithValue("@WeaponType2", "N/A");
                        }

                        try
                        {
                            command.Parameters.AddWithValue("@WeaponType3", units[i].WeaponTypes[2]);
                        }

                        catch (IndexOutOfRangeException)
                        {
                            command.Parameters.AddWithValue("@WeaponType3", "N/A");
                        }

                        try
                        {
                            command.Parameters.AddWithValue("@WeaponType4", units[i].WeaponTypes[3]);
                        }

                        catch (IndexOutOfRangeException)
                        {
                            command.Parameters.AddWithValue("@WeaponType4", "N/A");
                        }

                        try
                        {
                            command.Parameters.AddWithValue("@WeaponType5", units[i].WeaponTypes[4]);
                        }

                        catch (IndexOutOfRangeException)
                        {
                            command.Parameters.AddWithValue("@WeaponType5", "N/A");
                        }

                        command.Parameters.AddWithValue("@Weapon1", units[i].Weapons[0].Name);
                        command.Parameters.AddWithValue("@Weapon2", units[i].Weapons[1].Name);
                        command.Parameters.AddWithValue("@Weapon3", units[i].Weapons[2].Name);

                        try
                        {
                            command.Parameters.AddWithValue("@Ability1", units[i].Abilities[0]);
                        }

                        catch (IndexOutOfRangeException)
                        {
                            command.Parameters.AddWithValue("@Ability1", "N/A");
                        }

                        try
                        {
                            command.Parameters.AddWithValue("@Ability2", units[i].Abilities[1]);
                        }

                        catch (IndexOutOfRangeException)
                        {
                            command.Parameters.AddWithValue("@Ability2", "N/A");
                        }

                        command.Parameters.AddWithValue("@Health", units[i].Stats.stats[0]);
                        command.Parameters.AddWithValue("@Strength", units[i].Stats.stats[1]);
                        command.Parameters.AddWithValue("@Magic", units[i].Stats.stats[2]);
                        command.Parameters.AddWithValue("@Skill", units[i].Stats.stats[3]);
                        command.Parameters.AddWithValue("@Speed", units[i].Stats.stats[4]);
                        command.Parameters.AddWithValue("@Luck", units[i].Stats.stats[5]);
                        command.Parameters.AddWithValue("@Defense", units[i].Stats.stats[6]);
                        command.Parameters.AddWithValue("@Resistance", units[i].Stats.stats[7]);
                        command.Parameters.AddWithValue("@Move", units[i].Stats.Move);
                        command.Parameters.AddWithValue("@HealthGrowth", units[i].Stats.StatGrowths[0]);
                        command.Parameters.AddWithValue("@StrengthGrowth", units[i].Stats.StatGrowths[1]);
                        command.Parameters.AddWithValue("@MagicGrowth", units[i].Stats.StatGrowths[2]);
                        command.Parameters.AddWithValue("@SkillGrowth", units[i].Stats.StatGrowths[3]);
                        command.Parameters.AddWithValue("@SpeedGrowth", units[i].Stats.StatGrowths[4]);
                        command.Parameters.AddWithValue("@LuckGrowth", units[i].Stats.StatGrowths[5]);
                        command.Parameters.AddWithValue("@DefenseGrowth", units[i].Stats.StatGrowths[6]);
                        command.Parameters.AddWithValue("@ResistanceGrowth", units[i].Stats.StatGrowths[7]);

                        command.ExecuteNonQuery();
                    }

                    success = true;
                }

                catch (SQLiteException ex)
                {
                    Console.WriteLine(ex.Message);
                    success = false;
                }

                _localDbConnection.Close();
            }

            return success;
        }

        public bool WriteGeneralInfoToDatabase(General general, string name)
        {
            bool success;

            if (_online)
            {
                try
                {
                    _onlineDbConnection.Open();
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                finally
                {
                    try
                    {
                        var command = new MySqlCommand("INSERT INTO Army_Generals (ID, Army_Name, Name, Origin, Title, Level, Tier, Promotion_1, Promotion_2, Weapon_Type_1, Weapon_Type_2, Weapon_Type_3, Weapon_Type_4, Weapon_Type_5, Weapon_1, Weapon_2, Weapon_3, Ability_1, Ability_2, Health, Strength, Magic, Skill, Speed, Luck, Defense, Resistance, Move, Health_Growth, Strength_Growth, Magic_Growth, Skill_Growth, Speed_Growth, Luck_Growth, Defense_Growth, Resistance_Growth) VALUES (@ID, @ArmyName, @Name, @Origin, @Title, @Level, @Tier, @Promotion1, @Promotion2, @WeaponType1, @WeaponType2, @WeaponType3, @WeaponType4, @WeaponType5, @Weapon1, @Weapon2, @Weapon3, @Ability1, @Ability2, @Health, @Strength, @Magic, @Skill, @Speed, @Luck, @Defense, @Resistance, @Move, @HealthGrowth, @StrengthGrowth, @MagicGrowth, @SkillGrowth, @SpeedGrowth, @LuckGrowth, @DefenseGrowth, @ResistanceGrowth)", _onlineDbConnection);

                        //Set parameters for insert into Army_Generals table
                        command.Parameters.AddWithValue("@ID", general.Id);
                        command.Parameters.AddWithValue("@ArmyName", name);
                        command.Parameters.AddWithValue("@Name", general.Name);
                        command.Parameters.AddWithValue("@Origin", general.Origin);
                        command.Parameters.AddWithValue("@Title", general.Title);
                        command.Parameters.AddWithValue("@Level", general.Level);
                        command.Parameters.AddWithValue("@Tier", general.Tier);

                        try
                        {
                            command.Parameters.AddWithValue("@Promotion1", general.Promotions[0]);
                        }

                        catch (IndexOutOfRangeException)
                        {
                            command.Parameters.AddWithValue("@Promotion1", "N/A");
                        }

                        try
                        {
                            command.Parameters.AddWithValue("@Promotion2", general.Promotions[1]);
                        }

                        catch (IndexOutOfRangeException)
                        {
                            command.Parameters.AddWithValue("@Promotion2", "N/A");
                        }

                        command.Parameters.AddWithValue("@WeaponType1", general.WeaponTypes[0]);

                        try
                        {
                            command.Parameters.AddWithValue("@WeaponType2", general.WeaponTypes[1]);
                        }

                        catch (IndexOutOfRangeException)
                        {
                            command.Parameters.AddWithValue("@WeaponType2", "N/A");
                        }

                        try
                        {
                            command.Parameters.AddWithValue("@WeaponType3", general.WeaponTypes[2]);
                        }

                        catch (IndexOutOfRangeException)
                        {
                            command.Parameters.AddWithValue("@WeaponType3", "N/A");
                        }

                        try
                        {
                            command.Parameters.AddWithValue("@WeaponType4", general.WeaponTypes[3]);
                        }

                        catch (IndexOutOfRangeException)
                        {
                            command.Parameters.AddWithValue("@WeaponType4", "N/A");
                        }

                        try
                        {
                            command.Parameters.AddWithValue("@WeaponType5", general.WeaponTypes[4]);
                        }

                        catch (IndexOutOfRangeException)
                        {
                            command.Parameters.AddWithValue("@WeaponType5", "N/A");
                        }

                        command.Parameters.AddWithValue("@Weapon1", general.Weapons[0].Name);
                        command.Parameters.AddWithValue("@Weapon2", general.Weapons[1].Name);
                        command.Parameters.AddWithValue("@Weapon3", general.Weapons[2].Name);

                        try
                        {
                            command.Parameters.AddWithValue("@Ability1", general.Abilities[0]);
                        }

                        catch (IndexOutOfRangeException)
                        {
                            command.Parameters.AddWithValue("@Ability1", "N/A");
                        }

                        try
                        {
                            command.Parameters.AddWithValue("@Ability2", general.Abilities[1]);
                        }

                        catch (IndexOutOfRangeException)
                        {
                            command.Parameters.AddWithValue("@Ability2", "N/A");
                        }

                        command.Parameters.AddWithValue("@Health", general.Stats.stats[0]);
                        command.Parameters.AddWithValue("@Strength", general.Stats.stats[1]);
                        command.Parameters.AddWithValue("@Magic", general.Stats.stats[2]);
                        command.Parameters.AddWithValue("@Skill", general.Stats.stats[3]);
                        command.Parameters.AddWithValue("@Speed", general.Stats.stats[4]);
                        command.Parameters.AddWithValue("@Luck", general.Stats.stats[5]);
                        command.Parameters.AddWithValue("@Defense", general.Stats.stats[6]);
                        command.Parameters.AddWithValue("@Resistance", general.Stats.stats[7]);
                        command.Parameters.AddWithValue("@Move", general.Stats.Move);
                        command.Parameters.AddWithValue("@HealthGrowth", general.Stats.StatGrowths[0]);
                        command.Parameters.AddWithValue("@StrengthGrowth", general.Stats.StatGrowths[1]);
                        command.Parameters.AddWithValue("@MagicGrowth", general.Stats.StatGrowths[2]);
                        command.Parameters.AddWithValue("@SkillGrowth", general.Stats.StatGrowths[3]);
                        command.Parameters.AddWithValue("@SpeedGrowth", general.Stats.StatGrowths[4]);
                        command.Parameters.AddWithValue("@LuckGrowth", general.Stats.StatGrowths[5]);
                        command.Parameters.AddWithValue("@DefenseGrowth", general.Stats.StatGrowths[6]);
                        command.Parameters.AddWithValue("@ResistanceGrowth", general.Stats.StatGrowths[7]);

                        command.ExecuteNonQuery();

                        success = true;
                    }

                    catch (MySqlException ex)
                    {
                        Console.WriteLine(ex.Message);
                        success = false;
                    }

                    _onlineDbConnection.Close();
                }

                return success;
            }

            try
            {
                _localDbConnection.Open();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
                try
                {
                    var command = new SQLiteCommand("INSERT INTO Army_Generals (ID, Army_Name, Name, Origin, Title, Level, Tier, Promotion_1, Promotion_2, Weapon_Type_1, Weapon_Type_2, Weapon_Type_3, Weapon_Type_4, Weapon_Type_5, Weapon_1, Weapon_2, Weapon_3, Ability_1, Ability_2, Health, Strength, Magic, Skill, Speed, Luck, Defense, Resistance, Move, Health_Growth, Strength_Growth, Magic_Growth, Skill_Growth, Speed_Growth, Luck_Growth, Defense_Growth, Resistance_Growth) VALUES (@ID, @ArmyName, @Name, @Origin, @Title, @Level, @Tier, @Promotion1, @Promotion2, @WeaponType1, @WeaponType2, @WeaponType3, @WeaponType4, @WeaponType5, @Weapon1, @Weapon2, @Weapon3, @Ability1, @Ability2, @Health, @Strength, @Magic, @Skill, @Speed, @Luck, @Defense, @Resistance, @Move, @HealthGrowth, @StrengthGrowth, @MagicGrowth, @SkillGrowth, @SpeedGrowth, @LuckGrowth, @DefenseGrowth, @ResistanceGrowth)", _localDbConnection);

                    //Set parameters for insert into Army_Generals table
                    command.Parameters.AddWithValue("@ID", general.Id);
                    command.Parameters.AddWithValue("@ArmyName", name);
                    command.Parameters.AddWithValue("@Name", general.Name);
                    command.Parameters.AddWithValue("@Origin", general.Origin);
                    command.Parameters.AddWithValue("@Title", general.Title);
                    command.Parameters.AddWithValue("@Level", general.Level);
                    command.Parameters.AddWithValue("@Tier", general.Tier);

                    try
                    {
                        command.Parameters.AddWithValue("@Promotion1", general.Promotions[0]);
                    }

                    catch (IndexOutOfRangeException)
                    {
                        command.Parameters.AddWithValue("@Promotion1", "N/A");
                    }

                    try
                    {
                        command.Parameters.AddWithValue("@Promotion2", general.Promotions[1]);
                    }

                    catch (IndexOutOfRangeException)
                    {
                        command.Parameters.AddWithValue("@Promotion2", "N/A");
                    }

                    command.Parameters.AddWithValue("@WeaponType1", general.WeaponTypes[0]);

                    try
                    {
                        command.Parameters.AddWithValue("@WeaponType2", general.WeaponTypes[1]);
                    }

                    catch (IndexOutOfRangeException)
                    {
                        command.Parameters.AddWithValue("@WeaponType2", "N/A");
                    }

                    try
                    {
                        command.Parameters.AddWithValue("@WeaponType3", general.WeaponTypes[2]);
                    }

                    catch (IndexOutOfRangeException)
                    {
                        command.Parameters.AddWithValue("@WeaponType3", "N/A");
                    }

                    try
                    {
                        command.Parameters.AddWithValue("@WeaponType4", general.WeaponTypes[3]);
                    }

                    catch (IndexOutOfRangeException)
                    {
                        command.Parameters.AddWithValue("@WeaponType4", "N/A");
                    }

                    try
                    {
                        command.Parameters.AddWithValue("@WeaponType5", general.WeaponTypes[4]);
                    }

                    catch (IndexOutOfRangeException)
                    {
                        command.Parameters.AddWithValue("@WeaponType5", "N/A");
                    }

                    command.Parameters.AddWithValue("@Weapon1", general.Weapons[0].Name);
                    command.Parameters.AddWithValue("@Weapon2", general.Weapons[1].Name);
                    command.Parameters.AddWithValue("@Weapon3", general.Weapons[2].Name);

                    try
                    {
                        command.Parameters.AddWithValue("@Ability1", general.Abilities[0]);
                    }

                    catch (IndexOutOfRangeException)
                    {
                        command.Parameters.AddWithValue("@Ability1", "N/A");
                    }

                    try
                    {
                        command.Parameters.AddWithValue("@Ability2", general.Abilities[1]);
                    }

                    catch (IndexOutOfRangeException)
                    {
                        command.Parameters.AddWithValue("@Ability2", "N/A");
                    }

                    command.Parameters.AddWithValue("@Health", general.Stats.stats[0]);
                    command.Parameters.AddWithValue("@Strength", general.Stats.stats[1]);
                    command.Parameters.AddWithValue("@Magic", general.Stats.stats[2]);
                    command.Parameters.AddWithValue("@Skill", general.Stats.stats[3]);
                    command.Parameters.AddWithValue("@Speed", general.Stats.stats[4]);
                    command.Parameters.AddWithValue("@Luck", general.Stats.stats[5]);
                    command.Parameters.AddWithValue("@Defense", general.Stats.stats[6]);
                    command.Parameters.AddWithValue("@Resistance", general.Stats.stats[7]);
                    command.Parameters.AddWithValue("@Move", general.Stats.Move);
                    command.Parameters.AddWithValue("@HealthGrowth", general.Stats.StatGrowths[0]);
                    command.Parameters.AddWithValue("@StrengthGrowth", general.Stats.StatGrowths[1]);
                    command.Parameters.AddWithValue("@MagicGrowth", general.Stats.StatGrowths[2]);
                    command.Parameters.AddWithValue("@SkillGrowth", general.Stats.StatGrowths[3]);
                    command.Parameters.AddWithValue("@SpeedGrowth", general.Stats.StatGrowths[4]);
                    command.Parameters.AddWithValue("@LuckGrowth", general.Stats.StatGrowths[5]);
                    command.Parameters.AddWithValue("@DefenseGrowth", general.Stats.StatGrowths[6]);
                    command.Parameters.AddWithValue("@ResistanceGrowth", general.Stats.StatGrowths[7]);

                    command.ExecuteNonQuery();

                    success = true;
                }

                catch (SQLiteException ex)
                {
                    Console.WriteLine(ex.Message);
                    success = false;
                }

                _localDbConnection.Close();
            }

            return success;
        }
    }
}
