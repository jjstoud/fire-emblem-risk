﻿using System.Collections;

namespace Core_Classes
{
    public class Army
    {
        public ArrayList Generals { get; set; }
        public ArrayList Units { get; set; }
        public string Name { get; set; }
        public string Leader { get; set; }
        public int Gold { get; set; }
        public int Land { get; set; }
        public string Status { get; set; }


        public Army()
        {
            Generals = new ArrayList();
            Units = new ArrayList();
        }

        public void AddGeneral(General general)
        {
            Generals.Add(general);
        }

        public void AddUnit(Unit unit)
        {
            Units.Add(unit);
        }

        public void ResetArmy()
        {
            Generals.Clear();
            Units.Clear();
        }

        public string GetArmyInfo()
        {
            return "Army Information \r\n\r\nName: " + Name + "\r\nLeader: " + Leader + "\r\nGold: " + Gold + "\r\nLand: " + Land + "\r\nStatus: " + Status;
        }
    }
}