﻿using System;
using System.Collections;

namespace Core_Classes
{
    public class General
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string Origin { get; set; }
        public string Title { get; set; }
        public int Level { get; set; }
        public int Tier { get; set; }
        public string[] Promotions { get; set; }
        public string[] WeaponTypes { get; set; }
        public Weapon[] Weapons { get; set; }
        public ArrayList WeaponsAl { get; set; }
        public string[] Abilities { get; set; }
        public Stats Stats { get; set; }

        public Weapon EquippedWeapon { get; set; }

        public General(string id, string origin, string title, int tier, ArrayList promotions, ArrayList weaponTypes, ArrayList abilities, Stats stats)
        {
            Name = "";
            Id = id;
            Origin = origin;
            Title = title;
            Level = 1;
            Tier = tier;
            Promotions = ConvertArrayList(promotions);
            WeaponTypes = ConvertArrayList(weaponTypes);
            Weapons = new[] { new Weapon(), new Weapon(), new Weapon() };
            WeaponsAl = new ArrayList();
            Abilities = ConvertArrayList(abilities);
            Stats = stats;
            EquippedWeapon = Weapons[0];
        }

        public General(string name, string id, string origin, string title, int level, int tier, ArrayList promotions, ArrayList weaponTypes, IList weapons, ArrayList abilities, Stats stats)
        {
            Name = name;
            Id = id;
            Origin = origin;
            Title = title;
            Level = level;
            Tier = tier;
            Promotions = ConvertArrayList(promotions);
            WeaponTypes = ConvertArrayList(weaponTypes);
            WeaponsAl = new ArrayList();
            Weapons = ConvertWeaponArrayList(weapons);
            Abilities = ConvertArrayList(abilities);
            Stats = stats;
            EquippedWeapon = Weapons[0];
        }

        public void AddWeapon(Weapon weapon)
        {
            WeaponsAl.Add(weapon);

            for (var i=0; i < WeaponsAl.Count; i++)
            {
                if (i > 2)
                {
                    break;
                }

                Weapons[i] = (Weapon)WeaponsAl[i];
            }
        }

        public void LevelUp(bool trueLevelUp)
        {
            var rand = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);

            if (trueLevelUp)
            {
                Level += 1;
            }

            for (var i = 0; i < 8; i++)
            {
                var randNum = rand.Next(0, 101);

                if (randNum <= Stats.StatGrowths[i])
                {
                    Console.WriteLine(randNum + " is less than or equal to " + Stats.StatGrowths[i] + ", so leveled up stat at location " + i + ".");
                    Stats.stats[i] += 1;
                }

                else
                {
                    Console.WriteLine(randNum + " is greater than " + Stats.StatGrowths[i] + ", so didn't level up stat at location " + i + ".");
                }
            }
        }

        public string GetGeneralInfo()
        {
            var unitInfo = "General Information \r\n\r\nName: " + Name + "\r\nID: " + Id + "\r\nOrigin: " + Origin + "\r\nTitle: " + Title + "\r\nLevel: " + Level + "\r\nTier: " + Tier + "\r\nPossible Promotions: " + string.Join(", ", Promotions) + "\r\nWeapon Types: " + string.Join(", ", WeaponTypes) + "\r\nEquipped Weapon: " + EquippedWeapon.Name + "\r\nAbilities: " + string.Join(", ", Abilities) + "\r\n\r\n\r\nUnit Stats \r\n\r\nHealth: " + Stats.stats[0] + "\r\nStrength: " + Stats.stats[1] + "\r\nMagic: " + Stats.stats[2] + "\r\nSkill: " + Stats.stats[3] + "\r\nSpeed: " + Stats.stats[4] + "\r\nLuck: " + Stats.stats[5] + "\r\nDefense: " + Stats.stats[6] + "\r\nResistance: " + Stats.stats[7] + "\r\nMove: " + Stats.Move + "\r\n\r\n\r\nUnit Stat Growths \r\n\r\nHealth: " + Stats.StatGrowths[0] + "%\r\nStrength: " + Stats.StatGrowths[1] + "%\r\nMagic: " + Stats.StatGrowths[2] + "%\r\nSkill: " + Stats.StatGrowths[3] + "%\r\nSpeed: " + Stats.StatGrowths[4] + "%\r\nLuck: " + Stats.StatGrowths[5] + "%\r\nDefense: " + Stats.StatGrowths[6] + "%\r\nResistance: " + Stats.StatGrowths[7] + "%";

            return unitInfo;
        }

        public string GetBasicGeneralInfo()
        {
            var unitInfo = "Name: " + Name + "\r\nTitle: " + Title + "\r\nLevel: " + Level + "\r\nTier: " + Tier + "\r\nWeapon Types: " + string.Join(", ", WeaponTypes) + "\r\nEquipped Weapon: " + EquippedWeapon.Name + "\r\nAbilities: " + string.Join(", ", Abilities) + "\r\n\r\nHealth: " + Stats.stats[0] + "\r\nStrength: " + Stats.stats[1] + "\r\nMagic: " + Stats.stats[2] + "\r\nSkill: " + Stats.stats[3] + "\r\nSpeed: " + Stats.stats[4] + "\r\nLuck: " + Stats.stats[5] + "\r\nDefense: " + Stats.stats[6] + "\r\nResistance: " + Stats.stats[7] + "\r\nMove: " + Stats.Move;

            return unitInfo;
        }

        private static string[] ConvertArrayList(ArrayList al)
        {
            var array = new string[al.Count];

            var tempArray = al.ToArray();
            var count = 0;

            foreach (var obj in tempArray)
            {
                array[count] = obj.ToString();
                count++;
            }

            return array;
        }

        private Weapon[] ConvertWeaponArrayList(IList al)
        {
            var array = new Weapon[3];

            for (var i = 0; i < al.Count; i++)
            {
                array[i] = (Weapon)al[i];
                WeaponsAl.Add((Weapon)al[i]);
            }

            for (var i = 0; i < 3; i++)
            {
                if (array[i] == null)
                {
                    array[i] = new Weapon();
                }
            }

            return array;
        }
    }
}