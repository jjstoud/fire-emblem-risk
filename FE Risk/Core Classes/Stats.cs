﻿namespace Core_Classes
{
    public class Stats
    {
        // ReSharper disable once InconsistentNaming
        public int[] stats { get; set; }
        public int[] StatGrowths { get; set; }
        public int Move { get; set; }

        public Stats (int[] stats, int[] statGrowths, int move)
        {
            this.stats = stats;
            StatGrowths = statGrowths;
            Move = move;
        }
    }
}
