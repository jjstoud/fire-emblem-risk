﻿namespace Core_Classes
{
    public class Weapon
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Subtype { get; set; }
        public int Might { get; set; }
        public int HitRate { get; set; }
        public int Critical { get; set; }
        public int MinRange { get; set; }
        public int MaxRange { get; set; }
        public int Weight { get; set; }
        public int Cost { get; set; }
        public string Effect { get; set; }

        public Weapon()
        {
            Id = "N/A";
            Name = "N/A";
            Type = "N/A";
            Subtype = "N/A";
            Might = 0;
            HitRate = 0;
            Critical = 0;
            MinRange = 0;
            MaxRange = 0;
            Weight = 0;
            Cost = 0;
            Effect = "N/A";
        }

        public Weapon(string id, string name, string type, string subtype, int might, int hitRate, int critical, int minRange, int maxRange, int weight, int cost, string effect)
        {
            Id = id;
            Name = name;
            Type = type;
            Subtype = subtype;
            Might = might;
            HitRate = hitRate;
            Critical = critical;
            MinRange = minRange;
            MaxRange = maxRange;
            Weight = weight;
            Cost = cost;
            Effect = effect;
        }
    }
}