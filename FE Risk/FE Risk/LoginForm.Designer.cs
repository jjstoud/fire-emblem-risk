﻿namespace Army_Manager
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelArmyName = new System.Windows.Forms.Label();
            this.labelArmyPassword = new System.Windows.Forms.Label();
            this.textBoxArmyName = new System.Windows.Forms.TextBox();
            this.textBoxArmyPassword = new System.Windows.Forms.TextBox();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelArmyName
            // 
            this.labelArmyName.AutoSize = true;
            this.labelArmyName.Font = new System.Drawing.Font("Modern No. 20", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelArmyName.Location = new System.Drawing.Point(45, 15);
            this.labelArmyName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelArmyName.Name = "labelArmyName";
            this.labelArmyName.Size = new System.Drawing.Size(222, 41);
            this.labelArmyName.TabIndex = 0;
            this.labelArmyName.Text = "Army Name:";
            // 
            // labelArmyPassword
            // 
            this.labelArmyPassword.AutoSize = true;
            this.labelArmyPassword.Font = new System.Drawing.Font("Modern No. 20", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelArmyPassword.Location = new System.Drawing.Point(8, 101);
            this.labelArmyPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelArmyPassword.Name = "labelArmyPassword";
            this.labelArmyPassword.Size = new System.Drawing.Size(282, 41);
            this.labelArmyPassword.TabIndex = 1;
            this.labelArmyPassword.Text = "Army Password:";
            // 
            // textBoxArmyName
            // 
            this.textBoxArmyName.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArmyName.Location = new System.Drawing.Point(16, 60);
            this.textBoxArmyName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxArmyName.Name = "textBoxArmyName";
            this.textBoxArmyName.Size = new System.Drawing.Size(285, 36);
            this.textBoxArmyName.TabIndex = 2;
            // 
            // textBoxArmyPassword
            // 
            this.textBoxArmyPassword.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArmyPassword.Location = new System.Drawing.Point(16, 146);
            this.textBoxArmyPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxArmyPassword.Name = "textBoxArmyPassword";
            this.textBoxArmyPassword.Size = new System.Drawing.Size(285, 36);
            this.textBoxArmyPassword.TabIndex = 3;
            // 
            // buttonLogin
            // 
            this.buttonLogin.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonLogin.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLogin.Location = new System.Drawing.Point(16, 202);
            this.buttonLogin.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(287, 43);
            this.buttonLogin.TabIndex = 4;
            this.buttonLogin.Text = "Login";
            this.buttonLogin.UseVisualStyleBackColor = false;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(319, 260);
            this.Controls.Add(this.buttonLogin);
            this.Controls.Add(this.textBoxArmyPassword);
            this.Controls.Add(this.textBoxArmyName);
            this.Controls.Add(this.labelArmyPassword);
            this.Controls.Add(this.labelArmyName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LoginForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelArmyName;
        private System.Windows.Forms.Label labelArmyPassword;
        private System.Windows.Forms.TextBox textBoxArmyName;
        private System.Windows.Forms.TextBox textBoxArmyPassword;
        private System.Windows.Forms.Button buttonLogin;
    }
}