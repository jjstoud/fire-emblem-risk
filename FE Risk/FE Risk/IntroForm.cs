﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Data.SQLite;
using System.Threading;
using System.Windows.Forms;
using Army_Manager.Properties;
using Microsoft.VisualBasic;
using MySql.Data.MySqlClient;

namespace Army_Manager
{
    public partial class IntroForm : Form
    {
        private bool _databaseOnline;
        private bool _retryConnection;
        private int _connectionAttempt;
        private MySqlConnection _onlineDbConnection;
        private SQLiteConnection _localDbConnection;
        private string _localConnectionString;
        private string _onlineConnectionString;

        private bool _authenticated;
        private string _armyName;

        private readonly BackgroundWorker _bw = new BackgroundWorker();

        public IntroForm()
        {
            InitializeComponent();

            //Set window name
            Text = ConfigurationManager.AppSettings["programName"];

            _bw.WorkerSupportsCancellation = false;
            _bw.WorkerReportsProgress = true;
            _bw.DoWork += bw_DoWork;
            _bw.ProgressChanged += bw_ProgressChanged;
            _bw.RunWorkerCompleted += bw_RunWorkerCompleted;

            _retryConnection = true;

            //Online play
            if (MessageBox.Show(@"Access online database?", @"FE Risk: Database Mode", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                _databaseOnline = true;

                while (ConfigurationManager.AppSettings["databaseOnline"] == "")
                {
                    ChangeOnlineDbAddress();
                }

                while (_retryConnection)
                {
                    _connectionAttempt++;
                    ConnectToDatabase();
                }
            }

            //Local play
            else
            {
                if (MessageBox.Show(@"Play locally, then?", @"FE Risk: Database Mode", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    _databaseOnline = false;

                    while (ConfigurationManager.AppSettings["databaseLoc"] == "")
                    {
                        ChangeLocalDbAddress();

                        if (ConfigurationManager.AppSettings["databaseLoc"] == "")
                        {
                            MessageBox.Show(@"Select location of the local database.");
                        }
                    }

                    while (_retryConnection)
                    {
                        _connectionAttempt++;
                        ConnectToDatabase();
                    }
                }

                else
                {
                    MessageBox.Show(@"Quitting Application.");
                    Close();
                }
            }
        }

        public sealed override string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        private void ChangeOnlineDbAddress()
        {
            var ipAddress = Interaction.InputBox("Enter the IP address of the database server", "Database IP");

            var builtConn = new MySqlConnectionStringBuilder
            {
                Server = ipAddress,
                Port = 9090, //Windows Server
                //Port = 33982, //Linux Server
                UserID = "FEAdmin",
                Password = "F3@dm1n022716",
                Database = "fe-development"
            };

            _onlineConnectionString = builtConn.ToString();

            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings["databaseOnline"].Value = _onlineConnectionString;
            configuration.Save();

            ConfigurationManager.RefreshSection("appSettings");
        }

        private void ChangeLocalDbAddress()
        {
            var ofd = new OpenFileDialog
            {
                Title = @"Select location of local database.",
                Filter = @"SQLite Database Files (.db)|*.db"
            };

            ofd.ShowDialog();

            var builtConn = new SQLiteConnectionStringBuilder {DataSource = ofd.FileName};
            _localConnectionString = builtConn.ToString();

            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings["databaseLoc"].Value = _localConnectionString;
            configuration.Save();

            ConfigurationManager.RefreshSection("appSettings");
        }

        private void ConnectToDatabase()
        {
            if (_databaseOnline)
            {
                _onlineDbConnection = new MySqlConnection(ConfigurationManager.AppSettings["databaseOnline"]);

                try
                {
                    _onlineDbConnection.Open();
                    _onlineDbConnection.Close();
                    _retryConnection = false;
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                    if (MessageBox.Show(@"Retry connecting to the database?", @"Failed Connection", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        if (MessageBox.Show(@"Switch to local connection?", @"Failed Connection", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            _databaseOnline = false;

                            while (ConfigurationManager.AppSettings["databaseLoc"] == "")
                            {
                                ChangeLocalDbAddress();

                                if (ConfigurationManager.AppSettings["databaseLoc"] == "")
                                {
                                    MessageBox.Show(@"Select location of the local database.");
                                }
                            }

                            _connectionAttempt = 0;

                            while (_retryConnection)
                            {
                                _connectionAttempt++;
                                ConnectToDatabase();
                            }
                        }

                        else
                        {
                            _retryConnection = false;
                            MessageBox.Show(@"Can't continue without a database connection. Shutting down the program.");
                            Close();
                        }
                    }

                    else
                    {
                        if (_connectionAttempt > 2)
                        {
                            if (MessageBox.Show(@"Wanna try changing the IP address of the database server?", @"Failed Connection", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            {
                                ChangeOnlineDbAddress();
                            }
                        }
                    }
                }
            }

            else
            {
                _localConnectionString = ConfigurationManager.AppSettings["databaseLoc"];
                _localDbConnection = new SQLiteConnection(_localConnectionString);

                try
                {
                    _localDbConnection.Open();
                    _localDbConnection.Close();
                    _retryConnection = false;
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                    if (MessageBox.Show(@"Retry connecting to the database?", @"Failed Connection", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        _retryConnection = false;
                        MessageBox.Show(@"Can't continue without a database connection. Shutting down the program.");
                        Close();
                    }

                    else
                    {
                        if (_connectionAttempt > 2)
                        {
                            if (MessageBox.Show(@"Wanna try changing the database file?", @"Failed Connection", MessageBoxButtons.YesNo) == DialogResult.Yes)
                            {
                                ChangeLocalDbAddress();
                            }
                        }
                    }
                }
            }
        }

        public void AuthenticateUser(bool authenticated)
        {
            _authenticated = authenticated;
        }

        public void SetArmyName(string armyName)
        {
            _armyName = armyName;
        }

        private void pictureBoxNew_MouseEnter(object sender, EventArgs e)
        {
            pictureBoxNew.Image = Resources.AwakeningNew2;
        }

        private void pictureBoxNew_MouseLeave(object sender, EventArgs e)
        {
            pictureBoxNew.Image = Resources.AwakeningNew;
        }

        private void pictureBoxNew_Click(object sender, EventArgs e)
        {
            if (_databaseOnline)
            {
                if (string.IsNullOrEmpty(_onlineConnectionString))
                {
                    var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    _onlineConnectionString = configuration.AppSettings.Settings["databaseOnline"].Value;
                }

                var originForm = new OriginOverviewForm(_onlineConnectionString, true);
                Hide();
                originForm.ShowDialog();
                Close();
            }

            else
            {
                if (string.IsNullOrEmpty(_localConnectionString))
                {
                    var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    _localConnectionString = configuration.AppSettings.Settings["databaseLoc"].Value;
                }

                var originForm = new OriginOverviewForm(_localConnectionString, false);
                Hide();
                originForm.ShowDialog();
                Close();
            }
        }

        private void pictureBoxLoad_MouseEnter(object sender, EventArgs e)
        {
            pictureBoxLoad.Image = Resources.AwakeningLoad2;
        }

        private void pictureBoxLoad_MouseLeave(object sender, EventArgs e)
        {
            pictureBoxLoad.Image = Resources.AwakeningLoad;
        }

        private void pictureBoxLoad_Click(object sender, EventArgs e)
        {
            if (_databaseOnline)
            {
                if (string.IsNullOrEmpty(_onlineConnectionString))
                {
                    var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    _onlineConnectionString = configuration.AppSettings.Settings["databaseOnline"].Value;
                }

                using (var loginForm = new LoginForm(_onlineConnectionString, true, "IntroForm"))
                {
                    loginForm.Owner = this;
                    loginForm.ShowDialog();
                }

                if (_authenticated)
                {
                    var armyOverviewForm = new ArmyOverviewForm(_onlineConnectionString, true, _armyName);
                    Hide();
                    armyOverviewForm.ShowDialog();
                    Close();
                }

                else
                {
                    MessageBox.Show(@"Failed to login! Ensure that you input the proper information and try again.", @"Login Failure!");
                }
            }

            else
            {
                if (string.IsNullOrEmpty(_localConnectionString))
                {
                    var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    _localConnectionString = configuration.AppSettings.Settings["databaseLoc"].Value;
                }

                using (var loginForm = new LoginForm(_localConnectionString, false, "IntroForm"))
                {
                    loginForm.Owner = this;
                    loginForm.ShowDialog();
                }

                if (_authenticated)
                {
                    var armyOverviewForm = new ArmyOverviewForm(_localConnectionString, false, _armyName);
                    Hide();
                    armyOverviewForm.ShowDialog();
                    Close();
                }

                else
                {
                    MessageBox.Show(@"Failed to login! Ensure that you input the proper information and try again.", @"Login Failure!");
                }
            }
        }

        private void pictureBoxInfo_MouseEnter(object sender, EventArgs e)
        {
            pictureBoxInfo.Image = Resources.AwakeningInfo2;
        }

        private void pictureBoxInfo_MouseLeave(object sender, EventArgs e)
        {
            pictureBoxInfo.Image = Resources.AwakeningInfo;
        }

        private void pictureBoxInfo_Click(object sender, EventArgs e)
        {
            Text = ConfigurationManager.AppSettings["programName"] + @" " + (0 + @"%");
            Enabled = false;

            if (_databaseOnline)
            {
                if (string.IsNullOrEmpty(_onlineConnectionString))
                {
                    var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    _onlineConnectionString = configuration.AppSettings.Settings["databaseOnline"].Value;
                }

                if (_bw.IsBusy != true)
                {
                    _bw.RunWorkerAsync();
                }
            }

            else
            {
                if (string.IsNullOrEmpty(_localConnectionString))
                {
                    var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    _localConnectionString = configuration.AppSettings.Settings["databaseLoc"].Value;
                }

                if (_bw.IsBusy != true)
                {
                    _bw.RunWorkerAsync();
                }
            }
        }

        private void pictureBoxQuit_MouseEnter(object sender, EventArgs e)
        {
            pictureBoxQuit.Image = Resources.AwakeningQuit2;
        }

        private void pictureBoxQuit_MouseLeave(object sender, EventArgs e)
        {
            pictureBoxQuit.Image = Resources.AwakeningQuit;
        }

        private void pictureBoxQuit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;

            if (_databaseOnline)
            {
                for (var i = 1; (i <= 50); i++)
                {
                    var armyGen = new ArmyGenerator(_onlineConnectionString, true);
                    armyGen.GenerateArmy();
                    armyGen.WriteArmyToDatabase();

                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                    worker?.ReportProgress((i * 2));
                }
            }

            else
            {
                for (var i = 1; (i <= 5); i++)
                {
                    var armyGen = new ArmyGenerator(_localConnectionString, false);
                    armyGen.GenerateArmy();
                    armyGen.WriteArmyToDatabase();

                    // Perform a time consuming operation and report progress.
                    Thread.Sleep(500);
                    worker?.ReportProgress((i * 20));
                }
            }
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Text = ConfigurationManager.AppSettings["programName"] + @" " + (e.ProgressPercentage + @"%");
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Text = ConfigurationManager.AppSettings["programName"] + @" " + (@"Error: " + e.Error.Message);
            }

            else
            {
                Text = ConfigurationManager.AppSettings["programName"];
            }

            Enabled = true;
            MessageBox.Show(@"Armies Generated!");
        }
    }
}