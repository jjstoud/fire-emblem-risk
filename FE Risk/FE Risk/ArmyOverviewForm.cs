﻿using System;
using System.Configuration;
using System.Windows.Forms;
using Core_Classes;
using Data_Access;

namespace Army_Manager
{
    public partial class ArmyOverviewForm : Form
    {
        private readonly ArmyOverviewQueries _armyOq;
        private readonly GeneralProvider _generalP;
        private readonly UnitProvider _unitP;

        private readonly string _connectionString;
        private readonly bool _databaseOnline;

        private readonly string _armyName;
        private int _tabCountGenerals;
        private int _tabCountUnits;
        private string[] _generalNames;
        private string[] _unitDisplayTitles;
        private string[] _unitTitles;

        private Army _army;

        public ArmyOverviewForm(string connectionString, bool databaseOnline, string armyName)
        {
            InitializeComponent();

            Text = ConfigurationManager.AppSettings["programName"] + @" (" + armyName + @")";

            _connectionString = connectionString;
            _databaseOnline = databaseOnline;

            _armyOq = new ArmyOverviewQueries(connectionString, databaseOnline, armyName);
            _generalP = new GeneralProvider(connectionString, databaseOnline);
            _unitP = new UnitProvider(connectionString, databaseOnline);

            _armyName = armyName;

            GetArmy();
            GetArmyStatus();
            AddGeneralTabs();
            SetGeneralTabs();
            AddUnitTabs();
            SetUnitTabs();
        }

        public sealed override string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        private void GetArmy()
        {
            _army = _armyOq.GetArmy();
        }

        private void GetArmyStatus()
        {
            richTextBoxArmyStatus.Text = _army.GetArmyInfo();
        }

        private void AddGeneralTabs()
        {
            //Get number of army generals
            _tabCountGenerals = _armyOq.GetNumArmyGenerals();

            //Make an array to hold each unique title and add a tab for each title
            _generalNames = new string[_tabCountGenerals];

            //Get each unique title
            _generalNames = _armyOq.GetGeneralNames(_generalNames);

            //Set each tab to a unique unit
            foreach (var name in _generalNames)
            {
                tabControlGenerals.TabPages.Add(name);
            }
        }

        private void AddUnitTabs()
        {
            //Get number of army generals
            _tabCountUnits = _armyOq.GetUniqueArmyUnits();

            //Make an array to hold each unique title and add a tab for each title
            _unitDisplayTitles = new string[_tabCountUnits];
            _unitTitles = new string[_tabCountUnits];

            //Get each unique title
            _unitDisplayTitles = _armyOq.GetUnitDisplayTitles(_unitDisplayTitles);
            _unitTitles = _armyOq.GetUnitTitles(_unitTitles);

            //Set each tab to a unique unit
            foreach (var title in _unitDisplayTitles)
            {
                tabControlUnits.TabPages.Add(title);
            }
        }

        private void SetGeneralTabs()
        {
            var textBoxGenerals = new RichTextBox[_tabCountGenerals];

            for (var i = 0; i < _tabCountGenerals; i++)
            {
                textBoxGenerals[i] = new RichTextBox();

                tabControlGenerals.TabPages[i].Controls.Add(textBoxGenerals[i]);
                textBoxGenerals[i].ReadOnly = true;
                textBoxGenerals[i].Multiline = true;
                textBoxGenerals[i].WordWrap = true;
                textBoxGenerals[i].Dock = DockStyle.Fill;

                var general = _generalP.GetArmyGeneral(_armyName, _generalNames[i]);
                _army.AddGeneral(general);

                textBoxGenerals[i].Text = general.GetGeneralInfo();
            }
        }

        private void SetUnitTabs()
        {
            var textBoxUnits = new RichTextBox[_tabCountUnits];

            for (var i = 0; i < _tabCountUnits; i++)
            {
                textBoxUnits[i] = new RichTextBox();

                tabControlUnits.TabPages[i].Controls.Add(textBoxUnits[i]);
                textBoxUnits[i].ReadOnly = true;
                textBoxUnits[i].Multiline = true;
                textBoxUnits[i].WordWrap = true;
                textBoxUnits[i].Dock = DockStyle.Fill;

                var unit = _unitP.GetArmyUnit(_armyName, _unitTitles[i]);
                _army.AddUnit(unit);

                textBoxUnits[i].Text = unit.GetUnitInfo();
            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            var introForm = new IntroForm();
            Hide();
            introForm.ShowDialog();
            Close();
        }
    }
}
