﻿using System;
using System.Configuration;
using System.Windows.Forms;
using Data_Access;

namespace Army_Manager
{
    public partial class LoginForm : Form
    {
        private readonly MiscProvider _miscP;
        private readonly string _parentForm;

        public LoginForm(string dbConnection, bool online, string form)
        {
            InitializeComponent();

            _parentForm = form;

            Text = ConfigurationManager.AppSettings["programName"] + @" Login";

            _miscP = new MiscProvider(dbConnection, online);

            textBoxArmyPassword.PasswordChar = '*';
        }

        public sealed override string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            var armyName = textBoxArmyName.Text;
            var armyPassword = textBoxArmyPassword.Text;

            if (_parentForm == "IntroForm")
            {
                var parentForm = (IntroForm)Owner;
                parentForm.AuthenticateUser(_miscP.AuthenticateUser(armyName, armyPassword));

                if (_miscP.AuthenticateUser(armyName, armyPassword))
                {
                    parentForm.SetArmyName(armyName);
                }
            }

            Close();
        }
    }
}
