﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Army_Manager
{
    partial class StatModifierForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(StatModifierForm));
            this.buttonDone = new System.Windows.Forms.Button();
            this.comboBoxHealth = new System.Windows.Forms.ComboBox();
            this.textBoxHealth = new System.Windows.Forms.TextBox();
            this.textBoxResistance = new System.Windows.Forms.TextBox();
            this.textBoxDefense = new System.Windows.Forms.TextBox();
            this.textBoxLuck = new System.Windows.Forms.TextBox();
            this.textBoxSpeed = new System.Windows.Forms.TextBox();
            this.textBoxSkill = new System.Windows.Forms.TextBox();
            this.textBoxMagic = new System.Windows.Forms.TextBox();
            this.textBoxStrength = new System.Windows.Forms.TextBox();
            this.comboBoxResistance = new System.Windows.Forms.ComboBox();
            this.comboBoxDefense = new System.Windows.Forms.ComboBox();
            this.comboBoxLuck = new System.Windows.Forms.ComboBox();
            this.comboBoxSpeed = new System.Windows.Forms.ComboBox();
            this.comboBoxSkill = new System.Windows.Forms.ComboBox();
            this.comboBoxMagic = new System.Windows.Forms.ComboBox();
            this.comboBoxStrength = new System.Windows.Forms.ComboBox();
            this.labelName = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonDone
            // 
            this.buttonDone.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonDone.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonDone.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDone.Location = new System.Drawing.Point(0, 257);
            this.buttonDone.Name = "buttonDone";
            this.buttonDone.Size = new System.Drawing.Size(296, 25);
            this.buttonDone.TabIndex = 0;
            this.buttonDone.Text = "Done";
            this.buttonDone.UseVisualStyleBackColor = false;
            this.buttonDone.Click += new System.EventHandler(this.buttonDone_Click);
            // 
            // comboBoxHealth
            // 
            this.comboBoxHealth.FormattingEnabled = true;
            this.comboBoxHealth.Location = new System.Drawing.Point(167, 52);
            this.comboBoxHealth.Name = "comboBoxHealth";
            this.comboBoxHealth.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHealth.TabIndex = 1;
            this.comboBoxHealth.SelectedValueChanged += new System.EventHandler(this.comboBoxHealth_SelectedValueChanged);
            // 
            // textBoxHealth
            // 
            this.textBoxHealth.Location = new System.Drawing.Point(12, 53);
            this.textBoxHealth.Name = "textBoxHealth";
            this.textBoxHealth.ReadOnly = true;
            this.textBoxHealth.Size = new System.Drawing.Size(135, 20);
            this.textBoxHealth.TabIndex = 2;
            // 
            // textBoxResistance
            // 
            this.textBoxResistance.Location = new System.Drawing.Point(12, 235);
            this.textBoxResistance.Name = "textBoxResistance";
            this.textBoxResistance.ReadOnly = true;
            this.textBoxResistance.Size = new System.Drawing.Size(135, 20);
            this.textBoxResistance.TabIndex = 3;
            // 
            // textBoxDefense
            // 
            this.textBoxDefense.Location = new System.Drawing.Point(12, 209);
            this.textBoxDefense.Name = "textBoxDefense";
            this.textBoxDefense.ReadOnly = true;
            this.textBoxDefense.Size = new System.Drawing.Size(135, 20);
            this.textBoxDefense.TabIndex = 4;
            // 
            // textBoxLuck
            // 
            this.textBoxLuck.Location = new System.Drawing.Point(12, 183);
            this.textBoxLuck.Name = "textBoxLuck";
            this.textBoxLuck.ReadOnly = true;
            this.textBoxLuck.Size = new System.Drawing.Size(135, 20);
            this.textBoxLuck.TabIndex = 5;
            // 
            // textBoxSpeed
            // 
            this.textBoxSpeed.Location = new System.Drawing.Point(12, 157);
            this.textBoxSpeed.Name = "textBoxSpeed";
            this.textBoxSpeed.ReadOnly = true;
            this.textBoxSpeed.Size = new System.Drawing.Size(135, 20);
            this.textBoxSpeed.TabIndex = 6;
            // 
            // textBoxSkill
            // 
            this.textBoxSkill.Location = new System.Drawing.Point(12, 131);
            this.textBoxSkill.Name = "textBoxSkill";
            this.textBoxSkill.ReadOnly = true;
            this.textBoxSkill.Size = new System.Drawing.Size(135, 20);
            this.textBoxSkill.TabIndex = 7;
            // 
            // textBoxMagic
            // 
            this.textBoxMagic.Location = new System.Drawing.Point(12, 105);
            this.textBoxMagic.Name = "textBoxMagic";
            this.textBoxMagic.ReadOnly = true;
            this.textBoxMagic.Size = new System.Drawing.Size(135, 20);
            this.textBoxMagic.TabIndex = 8;
            // 
            // textBoxStrength
            // 
            this.textBoxStrength.Location = new System.Drawing.Point(12, 79);
            this.textBoxStrength.Name = "textBoxStrength";
            this.textBoxStrength.ReadOnly = true;
            this.textBoxStrength.Size = new System.Drawing.Size(135, 20);
            this.textBoxStrength.TabIndex = 9;
            // 
            // comboBoxResistance
            // 
            this.comboBoxResistance.FormattingEnabled = true;
            this.comboBoxResistance.Location = new System.Drawing.Point(167, 234);
            this.comboBoxResistance.Name = "comboBoxResistance";
            this.comboBoxResistance.Size = new System.Drawing.Size(121, 21);
            this.comboBoxResistance.TabIndex = 10;
            this.comboBoxResistance.SelectedValueChanged += new System.EventHandler(this.comboBoxResistance_SelectedValueChanged);
            // 
            // comboBoxDefense
            // 
            this.comboBoxDefense.FormattingEnabled = true;
            this.comboBoxDefense.Location = new System.Drawing.Point(167, 208);
            this.comboBoxDefense.Name = "comboBoxDefense";
            this.comboBoxDefense.Size = new System.Drawing.Size(121, 21);
            this.comboBoxDefense.TabIndex = 11;
            this.comboBoxDefense.SelectedValueChanged += new System.EventHandler(this.comboBoxDefense_SelectedValueChanged);
            // 
            // comboBoxLuck
            // 
            this.comboBoxLuck.FormattingEnabled = true;
            this.comboBoxLuck.Location = new System.Drawing.Point(167, 182);
            this.comboBoxLuck.Name = "comboBoxLuck";
            this.comboBoxLuck.Size = new System.Drawing.Size(121, 21);
            this.comboBoxLuck.TabIndex = 12;
            this.comboBoxLuck.SelectedValueChanged += new System.EventHandler(this.comboBoxLuck_SelectedValueChanged);
            // 
            // comboBoxSpeed
            // 
            this.comboBoxSpeed.FormattingEnabled = true;
            this.comboBoxSpeed.Location = new System.Drawing.Point(167, 156);
            this.comboBoxSpeed.Name = "comboBoxSpeed";
            this.comboBoxSpeed.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSpeed.TabIndex = 13;
            this.comboBoxSpeed.SelectedValueChanged += new System.EventHandler(this.comboBoxSpeed_SelectedValueChanged);
            // 
            // comboBoxSkill
            // 
            this.comboBoxSkill.FormattingEnabled = true;
            this.comboBoxSkill.Location = new System.Drawing.Point(167, 130);
            this.comboBoxSkill.Name = "comboBoxSkill";
            this.comboBoxSkill.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSkill.TabIndex = 14;
            this.comboBoxSkill.SelectedValueChanged += new System.EventHandler(this.comboBoxSkill_SelectedValueChanged);
            // 
            // comboBoxMagic
            // 
            this.comboBoxMagic.FormattingEnabled = true;
            this.comboBoxMagic.Location = new System.Drawing.Point(167, 104);
            this.comboBoxMagic.Name = "comboBoxMagic";
            this.comboBoxMagic.Size = new System.Drawing.Size(121, 21);
            this.comboBoxMagic.TabIndex = 15;
            this.comboBoxMagic.SelectedValueChanged += new System.EventHandler(this.comboBoxMagic_SelectedValueChanged);
            // 
            // comboBoxStrength
            // 
            this.comboBoxStrength.FormattingEnabled = true;
            this.comboBoxStrength.Location = new System.Drawing.Point(167, 78);
            this.comboBoxStrength.Name = "comboBoxStrength";
            this.comboBoxStrength.Size = new System.Drawing.Size(121, 21);
            this.comboBoxStrength.TabIndex = 16;
            this.comboBoxStrength.SelectedValueChanged += new System.EventHandler(this.comboBoxStrength_SelectedValueChanged);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Modern No. 20", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.Location = new System.Drawing.Point(12, 9);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(96, 34);
            this.labelName.TabIndex = 17;
            this.labelName.Text = "Name:";
            // 
            // textBoxName
            // 
            this.textBoxName.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxName.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxName.Location = new System.Drawing.Point(114, 14);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(174, 30);
            this.textBoxName.TabIndex = 18;
            this.textBoxName.Text = "Default";
            // 
            // StatModifierForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(296, 282);
            this.ControlBox = false;
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.comboBoxStrength);
            this.Controls.Add(this.comboBoxMagic);
            this.Controls.Add(this.comboBoxSkill);
            this.Controls.Add(this.comboBoxSpeed);
            this.Controls.Add(this.comboBoxLuck);
            this.Controls.Add(this.comboBoxDefense);
            this.Controls.Add(this.comboBoxResistance);
            this.Controls.Add(this.textBoxStrength);
            this.Controls.Add(this.textBoxMagic);
            this.Controls.Add(this.textBoxSkill);
            this.Controls.Add(this.textBoxSpeed);
            this.Controls.Add(this.textBoxLuck);
            this.Controls.Add(this.textBoxDefense);
            this.Controls.Add(this.textBoxResistance);
            this.Controls.Add(this.textBoxHealth);
            this.Controls.Add(this.comboBoxHealth);
            this.Controls.Add(this.buttonDone);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StatModifierForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Stat Modifier";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button buttonDone;
        private ComboBox comboBoxHealth;
        private TextBox textBoxHealth;
        private TextBox textBoxResistance;
        private TextBox textBoxDefense;
        private TextBox textBoxLuck;
        private TextBox textBoxSpeed;
        private TextBox textBoxSkill;
        private TextBox textBoxMagic;
        private TextBox textBoxStrength;
        private ComboBox comboBoxResistance;
        private ComboBox comboBoxDefense;
        private ComboBox comboBoxLuck;
        private ComboBox comboBoxSpeed;
        private ComboBox comboBoxSkill;
        private ComboBox comboBoxMagic;
        private ComboBox comboBoxStrength;
        private Label labelName;
        private TextBox textBoxName;
    }
}