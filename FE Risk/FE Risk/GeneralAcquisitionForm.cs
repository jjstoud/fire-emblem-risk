﻿using System;
using System.Windows.Forms;
using Core_Classes;
using Data_Access;

namespace Army_Manager
{
    public partial class GeneralAcquisitionForm : Form
    {
        private readonly GeneralAcquisitionProvider _generalAp;
        private readonly GeneralProvider _generalP;

        private int _tabCount;
        private General[] _generals;
        private General _general;
        private string[] _generalTitles;
        private readonly string _parentForm;

        public GeneralAcquisitionForm(string connectionString, bool databaseOnline, string form)
        {
            InitializeComponent();

            _parentForm = form;

            _generalAp = new GeneralAcquisitionProvider(connectionString, databaseOnline);
            _generalP = new GeneralProvider(connectionString, databaseOnline);

            AddGenerals();

            //Instructions to select a general
            MessageBox.Show(@"Choose a general.", @"General Selection", MessageBoxButtons.OK);
        }

        private void AddGenerals()
        {
            //Get number of unique titles
            _tabCount = _generalAp.GetFirstTierUniqueGeneralCount();

            //Make an array to hold each unique title
            _generalTitles = new string[_tabCount];
            _generals = new General[_tabCount];

            //Get each unique title
            _generalTitles = _generalAp.GetFirstTierUniqueTitleNames(_generalTitles);

            //Add a unique general to list
            for (var i = 0; i < _tabCount; i++)
            {
                _generals[i] = _generalP.GetGeneral(_generalTitles[i]);
            }

            listBoxUnits.DataSource = _generalTitles;
        }

        public void ConfirmGeneralAcquisition(General general)
        {
            _general = general;
        }

        private void listBoxUnits_SelectedValueChanged(object sender, EventArgs e)
        {
            if (_parentForm == "OriginSpecifiedForm")
            {
                richTextBoxInfo.Text = _generals[listBoxUnits.SelectedIndex].GetBasicGeneralInfo();
            }
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            using (var statModForm = new StatModifierForm(_generals[listBoxUnits.SelectedIndex]))
            {
                statModForm.Owner = this;
                statModForm.ShowDialog();
            }

            if (_parentForm == "OriginSpecifiedForm")
            {
                var parentForm = (OriginSpecifiedForm)Owner;
                parentForm.ConfirmGeneralAcquisition(_general);
            }

            Close();
        }
    }
}
