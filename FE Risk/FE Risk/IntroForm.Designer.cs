﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Army_Manager
{
    partial class IntroForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IntroForm));
            this.pictureBoxInfo = new System.Windows.Forms.PictureBox();
            this.pictureBoxQuit = new System.Windows.Forms.PictureBox();
            this.pictureBoxLoad = new System.Windows.Forms.PictureBox();
            this.pictureBoxNew = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxQuit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNew)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxInfo
            // 
            this.pictureBoxInfo.Image = global::Army_Manager.Properties.Resources.AwakeningInfo;
            this.pictureBoxInfo.Location = new System.Drawing.Point(0, 369);
            this.pictureBoxInfo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBoxInfo.Name = "pictureBoxInfo";
            this.pictureBoxInfo.Size = new System.Drawing.Size(533, 369);
            this.pictureBoxInfo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxInfo.TabIndex = 3;
            this.pictureBoxInfo.TabStop = false;
            this.pictureBoxInfo.Click += new System.EventHandler(this.pictureBoxInfo_Click);
            this.pictureBoxInfo.MouseEnter += new System.EventHandler(this.pictureBoxInfo_MouseEnter);
            this.pictureBoxInfo.MouseLeave += new System.EventHandler(this.pictureBoxInfo_MouseLeave);
            // 
            // pictureBoxQuit
            // 
            this.pictureBoxQuit.Image = global::Army_Manager.Properties.Resources.AwakeningQuit;
            this.pictureBoxQuit.Location = new System.Drawing.Point(533, 369);
            this.pictureBoxQuit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBoxQuit.Name = "pictureBoxQuit";
            this.pictureBoxQuit.Size = new System.Drawing.Size(533, 369);
            this.pictureBoxQuit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxQuit.TabIndex = 2;
            this.pictureBoxQuit.TabStop = false;
            this.pictureBoxQuit.Click += new System.EventHandler(this.pictureBoxQuit_Click);
            this.pictureBoxQuit.MouseEnter += new System.EventHandler(this.pictureBoxQuit_MouseEnter);
            this.pictureBoxQuit.MouseLeave += new System.EventHandler(this.pictureBoxQuit_MouseLeave);
            // 
            // pictureBoxLoad
            // 
            this.pictureBoxLoad.Image = global::Army_Manager.Properties.Resources.AwakeningLoad;
            this.pictureBoxLoad.Location = new System.Drawing.Point(533, 0);
            this.pictureBoxLoad.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBoxLoad.Name = "pictureBoxLoad";
            this.pictureBoxLoad.Size = new System.Drawing.Size(533, 369);
            this.pictureBoxLoad.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLoad.TabIndex = 1;
            this.pictureBoxLoad.TabStop = false;
            this.pictureBoxLoad.Click += new System.EventHandler(this.pictureBoxLoad_Click);
            this.pictureBoxLoad.MouseEnter += new System.EventHandler(this.pictureBoxLoad_MouseEnter);
            this.pictureBoxLoad.MouseLeave += new System.EventHandler(this.pictureBoxLoad_MouseLeave);
            // 
            // pictureBoxNew
            // 
            this.pictureBoxNew.Image = global::Army_Manager.Properties.Resources.AwakeningNew;
            this.pictureBoxNew.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxNew.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBoxNew.Name = "pictureBoxNew";
            this.pictureBoxNew.Size = new System.Drawing.Size(533, 369);
            this.pictureBoxNew.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxNew.TabIndex = 0;
            this.pictureBoxNew.TabStop = false;
            this.pictureBoxNew.Click += new System.EventHandler(this.pictureBoxNew_Click);
            this.pictureBoxNew.MouseEnter += new System.EventHandler(this.pictureBoxNew_MouseEnter);
            this.pictureBoxNew.MouseLeave += new System.EventHandler(this.pictureBoxNew_MouseLeave);
            // 
            // IntroForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 738);
            this.Controls.Add(this.pictureBoxInfo);
            this.Controls.Add(this.pictureBoxQuit);
            this.Controls.Add(this.pictureBoxLoad);
            this.Controls.Add(this.pictureBoxNew);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "IntroForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FE Risk";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxQuit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNew)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PictureBox pictureBoxNew;
        private PictureBox pictureBoxLoad;
        private PictureBox pictureBoxQuit;
        private PictureBox pictureBoxInfo;
    }
}