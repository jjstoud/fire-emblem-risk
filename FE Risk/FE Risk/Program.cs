﻿using System;
using System.Windows.Forms;

namespace Army_Manager
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            try
            {
                Application.Run(new IntroForm());
            }

            catch (Exception)
            {
                Application.Exit();
            }
        }
    }
}
