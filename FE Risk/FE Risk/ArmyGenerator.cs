﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web.Security;
using System.Windows.Forms;
using Army_Manager.Properties;
using Core_Classes;
using Data_Access;

namespace Army_Manager
{
    class ArmyGenerator
    {
        private readonly ArmyCreationQueries _armyCq;
        private readonly OriginOverviewProvider _originOp;
        private UnitAcquisitionProvider _unitAp;
        private GeneralAcquisitionProvider _generalAp;
        private readonly UnitProvider _unitP;
        private readonly GeneralProvider _generalP;
        private readonly WeaponProvider _weaponP;
        private readonly MiscProvider _miscP;

        private readonly string _connectionString;
        private readonly bool _databaseOnline;

        private string _origin;
        private readonly Army _army;
        private int _armyNum;
        private string _password; 

        public ArmyGenerator(string connectionString, bool databaseOnline)
        {
            _connectionString = connectionString;
            _databaseOnline = databaseOnline;
            _army = new Army();

            _armyCq = new ArmyCreationQueries(connectionString, databaseOnline);

            _originOp = new OriginOverviewProvider(connectionString, databaseOnline);

            _unitP = new UnitProvider(connectionString, databaseOnline);
            _generalP = new GeneralProvider(connectionString, databaseOnline);

            _weaponP = new WeaponProvider(connectionString, databaseOnline);

            _miscP = new MiscProvider(connectionString, databaseOnline);
        }

        public void GenerateArmy()
        {
            var rand = new Random();
            _armyNum = rand.Next(100, 1000);
            _army.Leader = @"Computer";

            SetArmyName();
            var nameFound = _miscP.CheckArmyName(_army.Name);

            while (nameFound)
            {
                SetArmyName();
                nameFound = _miscP.CheckArmyName(_army.Name);
            }

            SetPassword();
            SelectOrigin();
            SelectUnits();
            SelectGeneral();
        }

        public void WriteArmyToDatabase()
        {
            _armyCq.WriteArmyInfoToDatabase(_army.Name, _army.Leader, _password, 5000, 5, @"Peace");

            var units = new Unit[20];
            var titles = new string[20];

            var i = 0;

            foreach (Unit unit in _army.Units)
            {
                units[i] = unit;
                titles[i] = unit.Title;
                i++;
            }

            var distinctTitles = titles.Distinct().ToArray();
            var numTitles = new int[distinctTitles.Length];
            var distinctUnits = new Unit[distinctTitles.Length];

            for (i = 0; i < distinctTitles.Length; i++)
            {
                var title = distinctTitles[i];
                var unitAdded = false;

                foreach (var tempTitle in titles)
                {
                    if (tempTitle.Equals(title))
                    {
                        numTitles[i]++;
                    }
                }

                foreach (var unit in units)
                {
                    if (unit.Title.Equals(title) && !unitAdded)
                    {
                        distinctUnits[i] = unit;
                        unitAdded = true;
                    }
                }
            }

            _armyCq.WriteUnitInfoToDatabase(distinctUnits, _army.Name, numTitles, distinctTitles.Length);
            _armyCq.WriteGeneralInfoToDatabase((General) _army.Generals[0], _army.Name);
        }

        private void SelectOrigin()
        {
            var rand = new Random((int) DateTime.Now.Ticks & 0x0000FFFF);

            var originsCount = _originOp.GetUniqueOriginCount();
            var origins = new string[originsCount];
            origins = _originOp.GetUniqueOriginNames(origins);

            var randNum = rand.Next(0, originsCount);

            _origin = origins[randNum];

            Console.WriteLine(_origin);
        }

        private void SelectUnits()
        {
            var rand = new Random((int) DateTime.Now.Ticks & 0x0000FFFF);

            _unitAp = new UnitAcquisitionProvider(_connectionString, _databaseOnline, _origin);

            var titlesCount = _unitAp.GetFirstTierUniqueUnitCount();
            var titles = new string[titlesCount];
            titles = _unitAp.GetFirstTierUniqueTitleNames(titles);

            var unitCount = 0;

            var tempTitles = new string[20];
            var j = 0;

            while (unitCount < 20)
            {


                foreach (var title in titles)
                {
                    var tempUnitCount = unitCount;
                    var units = rand.Next(0, 4);
                    tempUnitCount += units;



                    if (tempUnitCount <= 20)
                    {
                        unitCount = tempUnitCount;

                        for (var i = 0; i < units; i++)
                        {
                            tempTitles[j] = title;
                            j++;
                        }
                    }

                    Thread.Sleep(1000);
                }
            }

            Array.Sort(tempTitles);

            foreach (var t in tempTitles)
            {
                _army.AddUnit(_unitP.GetUnit(t));
                Console.WriteLine(t + @"-" + unitCount);
            }

            foreach (Unit unit in _army.Units)
            {
                unit.Id = unit.Id + char.ToUpper(_army.Name[0]) + char.ToUpper(_army.Name[1]) + "A" + _armyNum;

                foreach (var weaponType in unit.WeaponTypes)
                {
                    unit.AddWeapon(_weaponP.GetWeapon(1, weaponType));
                }

                unit.EquippedWeapon = unit.Weapons[0];
            }
        }

        private void SelectGeneral()
        {
            var rand = new Random((int) DateTime.Now.Ticks & 0x0000FFFF);

            _generalAp = new GeneralAcquisitionProvider(_connectionString, _databaseOnline);

            var titlesCount = _generalAp.GetFirstTierUniqueGeneralCount();
            var titles = new string[titlesCount];
            titles = _generalAp.GetFirstTierUniqueTitleNames(titles);

            var randNum = rand.Next(0, titlesCount);
            var title = titles[randNum];

            var general = _generalP.GetGeneral(title);

            var statMods = SetGeneralStats();
            var generalName = GetGeneralName();

            for (var i = 0; i < general.Stats.StatGrowths.Length; i++)
            {
                general.Stats.StatGrowths[i] += statMods[i];

                if (general.Stats.StatGrowths[i] > 100)
                {
                    general.Stats.StatGrowths[i] = 100;
                }

                else if (general.Stats.StatGrowths[i] < 0)
                {
                    general.Stats.StatGrowths[i] = 0;
                }
            }

            general.Name = generalName;
            general.Id = "G" + char.ToUpper(generalName[0]) + general.Origin[0] + general.Title[0] + char.ToUpper(generalName[1]) + char.ToUpper(_army.Name[0]) + char.ToUpper(_army.Name[1]) + "A" + _armyNum;

            for (var i = 0; i < 5; i++)
            {
                general.LevelUp(false);
                Thread.Sleep(100);
            }

            foreach (var weaponType in general.WeaponTypes)
            {
                general.AddWeapon(_weaponP.GetWeapon(1, weaponType));
            }

            general.EquippedWeapon = general.Weapons[0];

            _army.AddGeneral(general);
        }

        private static int[] SetGeneralStats()
        {
            var rand = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);

            var stats20Pos = 0;
            var stats10Pos = 0;
            var statsZero = 0;
            var stats10Neg = 0;
            var stats20Neg = 0;

            var statsArrayMods = new[] {-20, -10, 0, 10, 20};
            var statsArray = new int[8];

            for (var i = 0; i < 8; i++)
            {
                var validMod = false;

                while (!validMod)
                {
                    Thread.Sleep(1000);
                    var randNum = rand.Next(0, 5);

                    if (statsArrayMods[randNum] == 20 && stats20Pos < 1)
                    {
                        statsArray[i] = statsArrayMods[randNum];
                        stats20Pos++;
                        validMod = true;
                    }

                    else if (statsArrayMods[randNum] == 10 && stats10Pos < 2)
                    {
                        statsArray[i] = statsArrayMods[randNum];
                        stats10Pos++;
                        validMod = true;
                    }

                    else if (statsArrayMods[randNum] == 0 && statsZero < 2)
                    {
                        statsArray[i] = statsArrayMods[randNum];
                        statsZero++;
                        validMod = true;
                    }

                    else if (statsArrayMods[randNum] == -10 && stats10Neg < 2)
                    {
                        statsArray[i] = statsArrayMods[randNum];
                        stats10Neg++;
                        validMod = true;
                    }

                    else if (statsArrayMods[randNum] == -20 && stats20Neg < 1)
                    {
                        statsArray[i] = statsArrayMods[randNum];
                        stats20Neg++;
                        validMod = true;
                    }
                }

                Console.WriteLine(statsArray[i]);
            }

            return statsArray;
        }

        private static string GetGeneralName()
        {
            var rand = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);

            string name;
            var generalNames = new ArrayList();
            var sr = new StringReader(Resources.GeneralNames);

            while ((name = sr.ReadLine()) != null)
            {
                generalNames.Add(name);
            }

            sr.Close();

            var randNum = rand.Next(0, generalNames.Count);

            return (string)generalNames[randNum];
        }

        private void SetArmyName()
        {
            var rand = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);

            string name;
            var armyNames = new ArrayList();
            var sr = new StringReader(Resources.ArmyNames);

            while ((name = sr.ReadLine()) != null)
            {
                armyNames.Add(name);
            }

            sr.Close();

            var randNum = rand.Next(0, armyNames.Count);

            _army.Name = (string)armyNames[randNum];
        }

        private void SetPassword()
        {
            _password = Membership.GeneratePassword(16, 4);
        }
    }
}
