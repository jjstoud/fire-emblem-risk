﻿namespace Army_Manager
{
    partial class ArmyOverviewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ArmyOverviewForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControlGenerals = new System.Windows.Forms.TabControl();
            this.tabControlUnits = new System.Windows.Forms.TabControl();
            this.richTextBoxArmyStatus = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanelButtons = new System.Windows.Forms.TableLayoutPanel();
            this.buttonGenerals = new System.Windows.Forms.Button();
            this.buttonUnits = new System.Windows.Forms.Button();
            this.buttonShop = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanelButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanelButtons);
            this.splitContainer1.Panel1.Controls.Add(this.richTextBoxArmyStatus);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControlUnits);
            this.splitContainer1.Panel2.Controls.Add(this.tabControlGenerals);
            this.splitContainer1.Size = new System.Drawing.Size(800, 600);
            this.splitContainer1.SplitterDistance = 266;
            this.splitContainer1.TabIndex = 0;
            // 
            // tabControlGenerals
            // 
            this.tabControlGenerals.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControlGenerals.Location = new System.Drawing.Point(0, 0);
            this.tabControlGenerals.Name = "tabControlGenerals";
            this.tabControlGenerals.SelectedIndex = 0;
            this.tabControlGenerals.Size = new System.Drawing.Size(530, 300);
            this.tabControlGenerals.TabIndex = 0;
            // 
            // tabControlUnits
            // 
            this.tabControlUnits.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControlUnits.Location = new System.Drawing.Point(0, 300);
            this.tabControlUnits.Name = "tabControlUnits";
            this.tabControlUnits.SelectedIndex = 0;
            this.tabControlUnits.Size = new System.Drawing.Size(530, 300);
            this.tabControlUnits.TabIndex = 1;
            // 
            // richTextBoxArmyStatus
            // 
            this.richTextBoxArmyStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.richTextBoxArmyStatus.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxArmyStatus.Name = "richTextBoxArmyStatus";
            this.richTextBoxArmyStatus.ReadOnly = true;
            this.richTextBoxArmyStatus.Size = new System.Drawing.Size(266, 300);
            this.richTextBoxArmyStatus.TabIndex = 1;
            this.richTextBoxArmyStatus.Text = "";
            // 
            // tableLayoutPanelButtons
            // 
            this.tableLayoutPanelButtons.ColumnCount = 1;
            this.tableLayoutPanelButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelButtons.Controls.Add(this.buttonGenerals, 0, 0);
            this.tableLayoutPanelButtons.Controls.Add(this.buttonUnits, 0, 1);
            this.tableLayoutPanelButtons.Controls.Add(this.buttonShop, 0, 2);
            this.tableLayoutPanelButtons.Controls.Add(this.buttonExit, 0, 3);
            this.tableLayoutPanelButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanelButtons.Location = new System.Drawing.Point(0, 300);
            this.tableLayoutPanelButtons.Name = "tableLayoutPanelButtons";
            this.tableLayoutPanelButtons.RowCount = 4;
            this.tableLayoutPanelButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelButtons.Size = new System.Drawing.Size(266, 300);
            this.tableLayoutPanelButtons.TabIndex = 2;
            // 
            // buttonGenerals
            // 
            this.buttonGenerals.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonGenerals.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonGenerals.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGenerals.Location = new System.Drawing.Point(3, 3);
            this.buttonGenerals.Name = "buttonGenerals";
            this.buttonGenerals.Size = new System.Drawing.Size(260, 69);
            this.buttonGenerals.TabIndex = 0;
            this.buttonGenerals.Text = "Manage Generals";
            this.buttonGenerals.UseVisualStyleBackColor = false;
            // 
            // buttonUnits
            // 
            this.buttonUnits.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonUnits.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonUnits.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUnits.Location = new System.Drawing.Point(3, 78);
            this.buttonUnits.Name = "buttonUnits";
            this.buttonUnits.Size = new System.Drawing.Size(260, 69);
            this.buttonUnits.TabIndex = 1;
            this.buttonUnits.Text = "Manage Units";
            this.buttonUnits.UseVisualStyleBackColor = false;
            // 
            // buttonShop
            // 
            this.buttonShop.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonShop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonShop.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonShop.Location = new System.Drawing.Point(3, 153);
            this.buttonShop.Name = "buttonShop";
            this.buttonShop.Size = new System.Drawing.Size(260, 69);
            this.buttonShop.TabIndex = 2;
            this.buttonShop.Text = "Shop Equipment";
            this.buttonShop.UseVisualStyleBackColor = false;
            // 
            // buttonExit
            // 
            this.buttonExit.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonExit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonExit.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExit.Location = new System.Drawing.Point(3, 228);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(260, 69);
            this.buttonExit.TabIndex = 3;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // ArmyOverviewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ArmyOverviewForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ArmyOverviewForm";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanelButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RichTextBox richTextBoxArmyStatus;
        private System.Windows.Forms.TabControl tabControlUnits;
        private System.Windows.Forms.TabControl tabControlGenerals;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelButtons;
        private System.Windows.Forms.Button buttonGenerals;
        private System.Windows.Forms.Button buttonUnits;
        private System.Windows.Forms.Button buttonShop;
        private System.Windows.Forms.Button buttonExit;
    }
}