﻿using System;
using System.Threading;
using System.Windows.Forms;
using Core_Classes;

namespace Army_Manager
{
    public partial class StatModifierForm : Form
    {
        private readonly General _general;

        public StatModifierForm(General general)
        {
            InitializeComponent();

            MessageBox.Show(@"Select one 20, two 10s, two 0s, two -10s, and one -20.");

            Text = @"Stat Modifier: " + general.Title;

            _general = general;

            SetFormInfo();
        }

        public sealed override string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        public void SetFormInfo()
        {
            comboBoxHealth.DataSource = new[] { 20, 10, 0, -10, -20 };
            comboBoxStrength.DataSource = new[] { 20, 10, 0, -10, -20 };
            comboBoxMagic.DataSource = new[] { 20, 10, 0, -10, -20 };
            comboBoxSkill.DataSource = new[] { 20, 10, 0, -10, -20 };
            comboBoxSpeed.DataSource = new[] { 20, 10, 0, -10, -20 };
            comboBoxLuck.DataSource = new[] { 20, 10, 0, -10, -20 };
            comboBoxDefense.DataSource = new[] { 20, 10, 0, -10, -20 };
            comboBoxResistance.DataSource = new[] { 20, 10, 0, -10, -20 };

            comboBoxHealth.SelectedItem = 0;
            comboBoxStrength.SelectedItem = 0;
            comboBoxMagic.SelectedItem = 0;
            comboBoxSkill.SelectedItem = 0;
            comboBoxSpeed.SelectedItem = 0;
            comboBoxLuck.SelectedItem = 0;
            comboBoxDefense.SelectedItem = 0;
            comboBoxResistance.SelectedItem = 0;

            textBoxHealth.Text = @"Health Growth: " + _general.Stats.StatGrowths[0] + @"%";
            textBoxStrength.Text = @"Strength Growth: " + _general.Stats.StatGrowths[1] + @"%";
            textBoxMagic.Text = @"Magic Growth: " + _general.Stats.StatGrowths[2] + @"%";
            textBoxSkill.Text = @"Skill Growth: " + _general.Stats.StatGrowths[3] + @"%";
            textBoxSpeed.Text = @"Speed Growth: " + _general.Stats.StatGrowths[4] + @"%";
            textBoxLuck.Text = @"Luck Growth: " + _general.Stats.StatGrowths[5] + @"%";
            textBoxDefense.Text = @"Defense Growth: " + _general.Stats.StatGrowths[6] + @"%";
            textBoxResistance.Text = @"Resistance Growth: " + _general.Stats.StatGrowths[7] + @"%";
        }

        private void comboBoxHealth_SelectedValueChanged(object sender, EventArgs e)
        {
            textBoxHealth.Text = @"Health Growth: " + (_general.Stats.StatGrowths[0] + (int)comboBoxHealth.SelectedValue) + @"%";
        }

        private void comboBoxStrength_SelectedValueChanged(object sender, EventArgs e)
        {
            textBoxStrength.Text = @"Strength Growth: " + (_general.Stats.StatGrowths[1] + (int)comboBoxStrength.SelectedValue) + @"%";
        }

        private void comboBoxMagic_SelectedValueChanged(object sender, EventArgs e)
        {
            textBoxMagic.Text = @"Magic Growth: " + (_general.Stats.StatGrowths[2] + (int)comboBoxMagic.SelectedValue) + @"%";
        }

        private void comboBoxSkill_SelectedValueChanged(object sender, EventArgs e)
        {
            textBoxSkill.Text = @"Skill Growth: " + (_general.Stats.StatGrowths[3] + (int)comboBoxSkill.SelectedValue) + @"%";
        }

        private void comboBoxSpeed_SelectedValueChanged(object sender, EventArgs e)
        {
            textBoxSpeed.Text = @"Speed Growth: " + (_general.Stats.StatGrowths[4] + (int)comboBoxSpeed.SelectedValue) + @"%";
        }

        private void comboBoxLuck_SelectedValueChanged(object sender, EventArgs e)
        {
            textBoxLuck.Text = @"Luck Growth: " + (_general.Stats.StatGrowths[5] + (int)comboBoxLuck.SelectedValue) + @"%";
        }

        private void comboBoxDefense_SelectedValueChanged(object sender, EventArgs e)
        {
            textBoxDefense.Text = @"Defense Growth: " + (_general.Stats.StatGrowths[6] + (int)comboBoxDefense.SelectedValue) + @"%";
        }

        private void comboBoxResistance_SelectedValueChanged(object sender, EventArgs e)
        {
            textBoxResistance.Text = @"Resistance Growth: " + (_general.Stats.StatGrowths[7] + (int)comboBoxResistance.SelectedValue) + @"%";
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            var statArray = new[] { (int)comboBoxHealth.SelectedValue, (int)comboBoxStrength.SelectedValue, (int)comboBoxMagic.SelectedValue, (int)comboBoxSkill.SelectedValue, (int)comboBoxSpeed.SelectedValue, (int)comboBoxLuck.SelectedValue, (int)comboBoxDefense.SelectedValue, (int)comboBoxResistance.SelectedValue };

            var pos20 = 0;
            var pos10 = 0;
            var zero = 0;
            var neg10 = 0;
            var neg20 = 0;

            foreach (var num in statArray)
            {
                if (num == 20)
                {
                    pos20 += 1;
                }

                else if (num == 10)
                {
                    pos10 += 1;
                }

                else if (num == 0)
                {
                    zero += 1;
                }

                else if (num == -10)
                {
                    neg10 += 1;
                }

                else
                {
                    neg20 += 1;
                }
            }

            if (pos20 != 1 || pos10 != 2 || zero != 2 || neg10 != 2 || neg20 != 1)
            {
                MessageBox.Show(@"Select one 20, two 10s, two 0s, two -10s, and one -20.");
            }

            else if (textBoxName.Text == "")
            {
                MessageBox.Show(@"Enter a name for your general.");
            }

            else if (textBoxName.Text.Length < 2)
            {
                MessageBox.Show(@"General's name must have at least two characters.");
            }

            else
            {
                _general.Id = "G" + char.ToUpper(textBoxName.Text[0]) + _general.Origin[0] + _general.Title[0] + char.ToUpper(textBoxName.Text[1]);

                _general.Name = textBoxName.Text;

                _general.Stats.StatGrowths[0] += (int)comboBoxHealth.SelectedValue;
                _general.Stats.StatGrowths[1] += (int)comboBoxStrength.SelectedValue;
                _general.Stats.StatGrowths[2] += (int)comboBoxMagic.SelectedValue;
                _general.Stats.StatGrowths[3] += (int)comboBoxSkill.SelectedValue;
                _general.Stats.StatGrowths[4] += (int)comboBoxSpeed.SelectedValue;
                _general.Stats.StatGrowths[5] += (int)comboBoxLuck.SelectedValue;
                _general.Stats.StatGrowths[6] += (int)comboBoxDefense.SelectedValue;
                _general.Stats.StatGrowths[7] += (int)comboBoxResistance.SelectedValue;

                for (var i = 0; i < _general.Stats.StatGrowths.Length; i++)
                {
                    if (_general.Stats.StatGrowths[i] > 100)
                    {
                        _general.Stats.StatGrowths[i] = 100;
                    }

                    else if (_general.Stats.StatGrowths[i] < 0)
                    {
                        _general.Stats.StatGrowths[i] = 0;
                    }
                }

                for (var i = 0; i < 5; i++)
                {
                    Console.WriteLine(@"Round " + (i+1) + @":");

                    _general.LevelUp(false);
                    Thread.Sleep(100);
                }

                var parentForm = (GeneralAcquisitionForm)Owner;
                parentForm.ConfirmGeneralAcquisition(_general);

                Close();
            }
        }
    }
}
