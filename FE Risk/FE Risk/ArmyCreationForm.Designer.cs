﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Army_Manager
{
    partial class ArmyCreationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ArmyCreationForm));
            this.richTextBoxGeneral = new System.Windows.Forms.RichTextBox();
            this.labelArmyName = new System.Windows.Forms.Label();
            this.labelArmyLeader = new System.Windows.Forms.Label();
            this.labelArmyPassword = new System.Windows.Forms.Label();
            this.textBoxArmyName = new System.Windows.Forms.TextBox();
            this.textBoxArmyLeader = new System.Windows.Forms.TextBox();
            this.textBoxArmyPassword = new System.Windows.Forms.TextBox();
            this.buttonStartOver = new System.Windows.Forms.Button();
            this.buttonSaveArmy = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.SuspendLayout();
            // 
            // richTextBoxGeneral
            // 
            this.richTextBoxGeneral.Location = new System.Drawing.Point(400, 0);
            this.richTextBoxGeneral.Name = "richTextBoxGeneral";
            this.richTextBoxGeneral.ReadOnly = true;
            this.richTextBoxGeneral.Size = new System.Drawing.Size(400, 339);
            this.richTextBoxGeneral.TabIndex = 1;
            this.richTextBoxGeneral.Text = "";
            // 
            // labelArmyName
            // 
            this.labelArmyName.AutoSize = true;
            this.labelArmyName.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelArmyName.Location = new System.Drawing.Point(406, 384);
            this.labelArmyName.Name = "labelArmyName";
            this.labelArmyName.Size = new System.Drawing.Size(117, 24);
            this.labelArmyName.TabIndex = 3;
            this.labelArmyName.Text = "Army Name:";
            // 
            // labelArmyLeader
            // 
            this.labelArmyLeader.AutoSize = true;
            this.labelArmyLeader.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelArmyLeader.Location = new System.Drawing.Point(406, 348);
            this.labelArmyLeader.Name = "labelArmyLeader";
            this.labelArmyLeader.Size = new System.Drawing.Size(126, 24);
            this.labelArmyLeader.TabIndex = 2;
            this.labelArmyLeader.Text = "Army Leader:";
            // 
            // labelArmyPassword
            // 
            this.labelArmyPassword.AutoSize = true;
            this.labelArmyPassword.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelArmyPassword.Location = new System.Drawing.Point(406, 420);
            this.labelArmyPassword.Name = "labelArmyPassword";
            this.labelArmyPassword.Size = new System.Drawing.Size(147, 24);
            this.labelArmyPassword.TabIndex = 4;
            this.labelArmyPassword.Text = "Army Password:";
            // 
            // textBoxArmyName
            // 
            this.textBoxArmyName.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxArmyName.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArmyName.Location = new System.Drawing.Point(559, 381);
            this.textBoxArmyName.Name = "textBoxArmyName";
            this.textBoxArmyName.Size = new System.Drawing.Size(229, 30);
            this.textBoxArmyName.TabIndex = 6;
            this.textBoxArmyName.Text = "Default";
            // 
            // textBoxArmyLeader
            // 
            this.textBoxArmyLeader.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxArmyLeader.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArmyLeader.Location = new System.Drawing.Point(559, 345);
            this.textBoxArmyLeader.Name = "textBoxArmyLeader";
            this.textBoxArmyLeader.Size = new System.Drawing.Size(229, 30);
            this.textBoxArmyLeader.TabIndex = 5;
            this.textBoxArmyLeader.Text = "Default";
            // 
            // textBoxArmyPassword
            // 
            this.textBoxArmyPassword.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxArmyPassword.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxArmyPassword.Location = new System.Drawing.Point(559, 417);
            this.textBoxArmyPassword.Name = "textBoxArmyPassword";
            this.textBoxArmyPassword.Size = new System.Drawing.Size(229, 30);
            this.textBoxArmyPassword.TabIndex = 7;
            this.textBoxArmyPassword.Text = "Default";
            // 
            // buttonStartOver
            // 
            this.buttonStartOver.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonStartOver.Font = new System.Drawing.Font("Modern No. 20", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStartOver.Location = new System.Drawing.Point(400, 453);
            this.buttonStartOver.Name = "buttonStartOver";
            this.buttonStartOver.Size = new System.Drawing.Size(400, 71);
            this.buttonStartOver.TabIndex = 8;
            this.buttonStartOver.Text = "Start Over";
            this.buttonStartOver.UseVisualStyleBackColor = false;
            this.buttonStartOver.Click += new System.EventHandler(this.buttonStartOver_Click);
            // 
            // buttonSaveArmy
            // 
            this.buttonSaveArmy.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonSaveArmy.Font = new System.Drawing.Font("Modern No. 20", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSaveArmy.Location = new System.Drawing.Point(400, 529);
            this.buttonSaveArmy.Name = "buttonSaveArmy";
            this.buttonSaveArmy.Size = new System.Drawing.Size(400, 71);
            this.buttonSaveArmy.TabIndex = 9;
            this.buttonSaveArmy.Text = "Save Army";
            this.buttonSaveArmy.UseVisualStyleBackColor = false;
            this.buttonSaveArmy.Click += new System.EventHandler(this.buttonSaveArmy_Click);
            // 
            // tabControl
            // 
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Left;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(400, 600);
            this.tabControl.TabIndex = 10;
            // 
            // ArmyCreationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.buttonSaveArmy);
            this.Controls.Add(this.buttonStartOver);
            this.Controls.Add(this.textBoxArmyPassword);
            this.Controls.Add(this.textBoxArmyLeader);
            this.Controls.Add(this.textBoxArmyName);
            this.Controls.Add(this.labelArmyPassword);
            this.Controls.Add(this.labelArmyLeader);
            this.Controls.Add(this.labelArmyName);
            this.Controls.Add(this.richTextBoxGeneral);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ArmyCreationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FE Risk";
            this.TransparencyKey = System.Drawing.Color.White;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private RichTextBox richTextBoxGeneral;
        private Label labelArmyName;
        private Label labelArmyLeader;
        private Label labelArmyPassword;
        private TextBox textBoxArmyName;
        private TextBox textBoxArmyLeader;
        private TextBox textBoxArmyPassword;
        private Button buttonStartOver;
        private Button buttonSaveArmy;
        private TabControl tabControl;
    }
}