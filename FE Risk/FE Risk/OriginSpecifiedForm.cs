﻿using System;
using System.Collections;
using System.Configuration;
using System.Windows.Forms;
using Core_Classes;
using Data_Access;

namespace Army_Manager
{
    public partial class OriginSpecifiedForm : Form
    {
        private readonly OriginSpecifiedProvider _originSp;
        private readonly UnitProvider _unitP;
        private readonly WeaponProvider _weaponP;

        private readonly string _connectionString;
        private readonly bool _databaseOnline;

        private int _tabCount;
        private readonly string _origin;
        private string[] _unitTitles;

        private readonly Army _army;

        public OriginSpecifiedForm()
        {
            InitializeComponent();

            //Set window name
            Text = ConfigurationManager.AppSettings["programName"];
        }

        public sealed override string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        public OriginSpecifiedForm(string connectionString, bool databaseOnline, string origin)
        {
            InitializeComponent();

            //Set window name
            Text = ConfigurationManager.AppSettings["programName"];

            _connectionString = connectionString;
            _databaseOnline = databaseOnline;
            _origin = origin;

            _originSp = new OriginSpecifiedProvider(connectionString, databaseOnline, origin);
            _unitP = new UnitProvider(connectionString, databaseOnline);
            _weaponP = new WeaponProvider(connectionString, databaseOnline);

            _origin = origin;
            _army = new Army();

            AddTabs();
            SetTabs();
        }

        private void AddTabs()
        {
            //Get number of unique titles
            _tabCount = _originSp.GetUniqueTitleCount();

            //Make an array to hold each unique title and add a tab for each title
            _unitTitles = new string[_tabCount];

            //Get each unique title
            _unitTitles = _originSp.GetUniqueTitleNames(_unitTitles);

            //Set each tab to a unique unit
            foreach (var title in _unitTitles)
            {
                tabControl.TabPages.Add(title);
            }
        }

        private void SetTabs()
        {
            var textBoxUnits = new RichTextBox[_tabCount];

            for (var i = 0; i < _tabCount; i++)
            {
                textBoxUnits[i] = new RichTextBox();

                tabControl.TabPages[i].Controls.Add(textBoxUnits[i]);
                textBoxUnits[i].ReadOnly = true;
                textBoxUnits[i].Multiline = true;
                textBoxUnits[i].WordWrap = true;
                textBoxUnits[i].Dock = DockStyle.Fill;

                var unit = _unitP.GetUnit(_unitTitles[i]);

                textBoxUnits[i].Text = unit.GetUnitInfo();
            }
        }

        public void ConfirmUnitAcquisition(Hashtable chosenUnits)
        {
            // Get a collection of the keys.
            var key = chosenUnits.Keys;

            foreach (string k in key)
            {
                for (var i=0; i < (int)chosenUnits[k]; i++)
                {
                    _army.AddUnit(_unitP.GetUnit(k));
                }
            }
        }

        public void ConfirmGeneralAcquisition(General general)
        {
            _army.AddGeneral(general);
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            var originOForm = new OriginOverviewForm(_connectionString, _databaseOnline);
            Hide();
            originOForm.ShowDialog();
            Close();
        }

        private void buttonContinue_MouseClick(object sender, MouseEventArgs e)
        {
            using (var unitAcForm = new UnitAcquisitionForm(_connectionString, _databaseOnline, _origin, "OriginSpecifiedForm"))
            {
                unitAcForm.Owner = this;
                unitAcForm.ShowDialog();
            }

            using (var genAcForm = new GeneralAcquisitionForm(_connectionString, _databaseOnline, "OriginSpecifiedForm"))
            {
                genAcForm.Owner = this;
                genAcForm.ShowDialog();
            }

            foreach (General general in _army.Generals)
            {
                foreach (var weaponType in general.WeaponTypes)
                {
                    general.AddWeapon(_weaponP.GetWeapon(1, weaponType));
                }

                general.EquippedWeapon = general.Weapons[0];
            }

            foreach (Unit unit in _army.Units)
            {
                foreach (var weaponType in unit.WeaponTypes)
                {
                    unit.AddWeapon(_weaponP.GetWeapon(1, weaponType));
                }

                unit.EquippedWeapon = unit.Weapons[0];
            }

             var armyCreationForm = new ArmyCreationForm(_connectionString, _databaseOnline, _army);
             Hide();
             armyCreationForm.ShowDialog();
             Close();
        }
    }
}