﻿using System;
using System.Collections;
using System.Configuration;
using System.Windows.Forms;
using Core_Classes;
using Data_Access;

namespace Army_Manager
{
    public partial class ArmyCreationForm : Form
    {
        private readonly ArmyCreationQueries _armyCq;
        private readonly MiscProvider _miscP;

        private readonly string _connectionString;
        private readonly bool _databaseOnline;

        private readonly Army _army;
        private int _tabCount;
        private string[] _uniqueTitles;
        private int[] _numTitles;
        private Unit[] _units;

        private bool _armyInfoWritten;
        private bool _armyUnitsWritten;
        private bool _armyGeneralWritten;

        public ArmyCreationForm(string connectionString, bool databaseOnline, Army army)
        {
            InitializeComponent();

            //Set window name
            Text = ConfigurationManager.AppSettings["programName"];

            _connectionString = connectionString;
            _databaseOnline = databaseOnline;

            _armyCq = new ArmyCreationQueries(connectionString, databaseOnline);
            _miscP = new MiscProvider(connectionString, databaseOnline);

            _army = army;
            _tabCount = 0;

            SetArmyWeapons();
            SetArmyInfo();
        }

        public sealed override string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        private void SetArmyInfo()
        {
            var tempTitles = new ArrayList();

            //Add and set tabs for each unique unit
            foreach (var t in _army.Units)
            {
                var unit = (Unit)t;
                var title = unit.Title;

                if (!tempTitles.Contains(title))
                {
                    tempTitles.Add(title);
                    tabControl.TabPages.Add(title);
                    _tabCount++;

                    var rtb = new RichTextBox();

                    tabControl.TabPages[_tabCount - 1].Controls.Add(rtb);
                    rtb.ReadOnly = true;
                    rtb.Multiline = true;
                    rtb.WordWrap = true;
                    rtb.Dock = DockStyle.Fill;

                    rtb.Text = unit.GetUnitInfo();
                }
            }

            _uniqueTitles = new string[tempTitles.Count];
            _numTitles = new int[tempTitles.Count];
            _units = new Unit[tempTitles.Count];

            for (var i = 0; i < tempTitles.Count; i++)
            {
                _numTitles[i] = 0;
                _uniqueTitles[i] = (string)tempTitles[i];
            }

            for (var i = 0; i < tempTitles.Count; i++)
            {
                var unitAdded = false;

                foreach (Unit unit in _army.Units)
                {
                    if (unit.Title == _uniqueTitles[i])
                    {
                        if (unitAdded == false)
                        {
                            _units[i] = unit;
                            unitAdded = true;
                        }

                        _numTitles[i] += 1;
                    }
                }
            }

            for (var i = 0; i < tempTitles.Count; i++)
            {
                tabControl.TabPages[i].Text = _uniqueTitles[i] + @"-" + _numTitles[i];
            }

            //Set general info
            var general = (General)_army.Generals[0];

            richTextBoxGeneral.ReadOnly = true;
            richTextBoxGeneral.Multiline = true;
            richTextBoxGeneral.WordWrap = true;

            richTextBoxGeneral.Text = general.GetGeneralInfo();
        }

        private void SetArmyWeapons()
        {
            foreach (General general in _army.Generals)
            {
                general.EquippedWeapon = general.Weapons[0];
            }

            foreach (Unit unit in _army.Units)
            {
                unit.EquippedWeapon = unit.Weapons[0];
            }
        }

        private void buttonStartOver_Click(object sender, EventArgs e)
        {
            var originForm = new OriginOverviewForm(_connectionString, _databaseOnline);
            Hide();
            originForm.ShowDialog();
            Close();
        }

        private void buttonSaveArmy_Click(object sender, EventArgs e)
        {
            if (textBoxArmyLeader.Text == "")
            {
                MessageBox.Show(@"Enter a name for the army's leader.");
            }

            else if (textBoxArmyName.Text == "")
            {
                MessageBox.Show(@"Enter a name for the army.");
            }

            else if (textBoxArmyPassword.Text == "")
            {
                MessageBox.Show(@"Enter a password for access to army's information.");
            }

            else if (textBoxArmyName.Text.Length < 2)
            {
                MessageBox.Show(@"The army's name must have at least 2 characters in it.");
            }

            else if (_miscP.CheckArmyName(textBoxArmyName.Text))
            {
                MessageBox.Show(@"Army name already in the database. Input a new one.");
            }

            else
            {
                var rand = new Random();
                var randomNumber = rand.Next(100, 1000);

                //Prepare command for insert into Armies table
                _armyInfoWritten = _armyCq.WriteArmyInfoToDatabase(textBoxArmyName.Text, textBoxArmyLeader.Text,textBoxArmyPassword.Text, 5000, 5, "Peace");

                if (!_armyInfoWritten)
                {
                    MessageBox.Show(@"Error writing army info to the database. Try again.", @"Database Error");
                    return;
                }

                foreach (var unit in _units)
                {
                    unit.Id = unit.Id + char.ToUpper(textBoxArmyName.Text[0]) + char.ToUpper(textBoxArmyName.Text[1]) + char.ToUpper(textBoxArmyLeader.Text[0]) + randomNumber;
                }

                //Prepare Unit Info to be written to database.
                _armyUnitsWritten = _armyCq.WriteUnitInfoToDatabase(_units, textBoxArmyName.Text, _numTitles,_tabCount);

                if (!_armyUnitsWritten)
                {
                    MessageBox.Show(@"Error writing army units to the database. Try again.", @"Database Error");
                    return;
                }

                var general = (General)_army.Generals[0];
                general.Id = general.Id + char.ToUpper(textBoxArmyName.Text[0]) + char.ToUpper(textBoxArmyName.Text[1]) + char.ToUpper(textBoxArmyLeader.Text[0]) + randomNumber;

                //Prepare command for insert into Army_Generals table
                _armyGeneralWritten = _armyCq.WriteGeneralInfoToDatabase(general, textBoxArmyName.Text);

                if (!_armyGeneralWritten)
                {
                    MessageBox.Show(@"Error writing army general to the database. Try again.", @"Database Error");
                    return;
                }

                var introForm = new IntroForm();
                Hide();
                introForm.ShowDialog();
                Close();
            }
        }
    }
}
