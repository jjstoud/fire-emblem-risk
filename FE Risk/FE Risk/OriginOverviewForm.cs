﻿using System;
using System.Configuration;
using System.Windows.Forms;
using Data_Access;

namespace Army_Manager
{
    public partial class OriginOverviewForm : Form
    {
        private readonly OriginOverviewProvider _originOp;

        private readonly string _connectionString;
        private readonly bool _databaseOnline;

        private int _tabCount;
        private string[] _unitOrigins;

        public OriginOverviewForm(string connectionString, bool databaseOnline)
        {
            InitializeComponent();

            //Set window name
            Text = ConfigurationManager.AppSettings["programName"];

            _connectionString = connectionString;
            _databaseOnline = databaseOnline;

            _originOp = new OriginOverviewProvider(connectionString, databaseOnline);

            AddTabs();
            SetTabs();
        }

        public sealed override string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        private void AddTabs()
        {
            //Get number of unique origins
            _tabCount = _originOp.GetUniqueOriginCount();

            //Make an array to hold each unique origin and add a tab for each origin
            _unitOrigins = new string[_tabCount];

            //Get each unique origin
            _unitOrigins = _originOp.GetUniqueOriginNames(_unitOrigins);

            //Set each tab to a unique origin
            foreach (var origin in _unitOrigins)
            {
                tabControl.TabPages.Add(origin);
            }
        }

        private void SetTabs()
        {
            var dataGridView = new DataGridView[_tabCount];

            for (var i = 0; i < _tabCount; i++)
            {
                dataGridView[i] = new DataGridView();

                tabControl.TabPages[i].Controls.Add(dataGridView[i]);

                dataGridView[i].AllowUserToResizeColumns = false;
                dataGridView[i].AllowUserToResizeRows = false;
                dataGridView[i].ReadOnly = true;
                dataGridView[i].AllowUserToAddRows = false;
                dataGridView[i].Dock = DockStyle.Fill;

                dataGridView[i].DataSource = _originOp.GetOriginDataTable(_unitOrigins[i]);

                dataGridView[i].AutoResizeColumns();
                dataGridView[i].AutoResizeRows();
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            var introForm = new IntroForm();
            Hide();
            introForm.ShowDialog();
            Close();
        }

        private void buttonContinue_Click(object sender, EventArgs e)
        {
            var originSForm = new OriginSpecifiedForm(_connectionString, _databaseOnline, tabControl.SelectedTab.Text);
            Hide();
            originSForm.ShowDialog();
            Close();
        }
    }
}