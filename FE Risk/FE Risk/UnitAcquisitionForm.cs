﻿using System;
using System.Collections;
using System.Windows.Forms;
using Data_Access;

namespace Army_Manager
{
    public partial class UnitAcquisitionForm : Form
    {
        private readonly UnitAcquisitionProvider _unitAp;
        private readonly UnitProvider _unitP;

        private int _tabCount;
        private string[] _unitTitles;
        private readonly Hashtable _chosenUnits;
        private readonly string _parentForm;

        public UnitAcquisitionForm(string connectionString, bool databaseOnline, string origin, string form)
        {
            InitializeComponent();

            _parentForm = form;

            _unitAp = new UnitAcquisitionProvider(connectionString, databaseOnline, origin);
            _unitP = new UnitProvider(connectionString, databaseOnline);

            _chosenUnits = new Hashtable();

            AddTabs();
            SetTabs();

            //Instructions to select units
            MessageBox.Show(@"Choose a combination of 20 units.", @"Unit Selection", MessageBoxButtons.OK);
        }

        private void AddTabs()
        {
            //Get number of unique titles
            _tabCount = _unitAp.GetFirstTierUniqueUnitCount();

            //Make an array to hold each unique title and add a tab for each title
            _unitTitles = new string[_tabCount];

            //Get each unique title
            _unitTitles = _unitAp.GetFirstTierUniqueTitleNames(_unitTitles);

            //Set each tab to a unique unit
            foreach (var title in _unitTitles)
            {
                tabControl.TabPages.Add(title);
            }
        }

        private void SetTabs()
        {
            var textBoxUnits = new RichTextBox[_tabCount];
            var numericUdUnitNum = new NumericUpDown[_tabCount];

            for (var i = 0; i < _tabCount; i++)
            {
                textBoxUnits[i] = new RichTextBox();
                numericUdUnitNum[i] = new NumericUpDown();

                tabControl.TabPages[i].Controls.Add(textBoxUnits[i]);
                textBoxUnits[i].ReadOnly = true;
                textBoxUnits[i].Multiline = true;
                textBoxUnits[i].WordWrap = true;
                textBoxUnits[i].ScrollBars = RichTextBoxScrollBars.Vertical;
                textBoxUnits[i].Dock = DockStyle.Fill;

                tabControl.TabPages[i].Controls.Add(numericUdUnitNum[i]);
                numericUdUnitNum[i].Minimum = 0;
                numericUdUnitNum[i].Maximum = 20;
                numericUdUnitNum[i].Dock = DockStyle.Bottom;

                var unit = _unitP.GetUnit(_unitTitles[i]);

                textBoxUnits[i].Text = unit.GetBasicUnitInfo();
            }
        }

        private void ConvertUnitsToHashtable()
        {
            for (var i=0; i < _tabCount; i++)
            {
                if (Int32.Parse(tabControl.TabPages[i].Controls[1].Text) != 0)
                {
                    var numUnits = Int32.Parse(tabControl.TabPages[i].Controls[1].Text);
                    var title = tabControl.TabPages[i].Text;

                    _chosenUnits.Add(title, numUnits);
                }
            }
        }

        private void buttonDone_Click(object sender, EventArgs e)
        {
            var unitCount = 0;

            for (var i=0; i < _tabCount; i++)
            {
                unitCount += Int32.Parse(tabControl.TabPages[i].Controls[1].Text);
            }

            if (unitCount < 20)
            {
                var leftOver = 20 - unitCount;

                MessageBox.Show(unitCount + @" out of 20 units chosen. Add " + leftOver + @" units.", @"Unit Count", MessageBoxButtons.OK);
            }

            else if (unitCount > 20)
            {
                var leftOver = unitCount - 20;

                MessageBox.Show(unitCount + @" out of 20 units chosen. Remove " + leftOver + @" units.", @"Unit Count", MessageBoxButtons.OK);
            }

            else
            {
                ConvertUnitsToHashtable();

                if (_parentForm == "OriginSpecifiedForm")
                {
                    var parentForm = (OriginSpecifiedForm)Owner;
                    parentForm.ConfirmUnitAcquisition(_chosenUnits);
                }

                Close();
            }
        }
    }
}
